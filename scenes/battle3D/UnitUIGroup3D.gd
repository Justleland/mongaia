extends MarginContainer

signal unit_selected(unit)
signal unit_hovered(unit)
signal unit_hover_exited()

var unit_ui_scene = preload("res://scenes/battle3D/UnitUI3D.tscn")
var ui_group = []
var hover_unit = null
var track_hover: bool = false

onready var child_hbox = $HBox

func _ready():
	var children = child_hbox.get_children()
	for child in children:
		child_hbox.remove_child(child)


func align(center: float, width: float) -> void:
	var screen_width = OS.window_size.x
	var segment = screen_width / 16.0
	
	print("BEFORE:\nMin Size x: " 
			+ String(rect_min_size.x)
			+ "\nSize x: " + String(rect_size.x)
			+ "\nPosition x: " + String(rect_position.x))
	
	child_hbox.rect_min_size.x = (width + 3.5) * segment
	rect_size.x = child_hbox.rect_min_size.x
	rect_position.x = (center + 8.0) * segment - (child_hbox.rect_min_size.x / 2.0)
	
	print("AFTER:\nMin Size x: " 
			+ String(rect_min_size.x)
			+ "\nSize x: " + String(rect_size.x)
			+ "\nPosition x: " + String(rect_position.x))


func format(units: Array) -> void:
	var count = 0
	for unit in units:
		if unit is Unit:
			var ui = unit_ui_scene.instance()
			ui.init_unit(unit)
			Utils.safe_connect(ui, "mouse_entered", self, "_on_hover_entered_" + String(count))
			Utils.safe_connect(ui, "mouse_exited", self, "_on_hover_exited_" + String(count))
			Utils.safe_connect(ui, "selected", self, "_on_unit_selected")
			child_hbox.add_child(ui)
			ui_group.append(ui)
			count += 1
		else:
			Utils.log_print("Cannot make UI for non Unit Obj.")


func switch(unit_a: Unit, unit_b: Unit) -> void:
	for ui in ui_group:
		if ui.unit == unit_a:
			ui.set_unit(unit_b)
			return


func _on_unit_selected(unit):
	if unit is Unit:
		emit_signal("unit_selected", unit)


func _on_hover_entered(ui):
	if ui.unit is Unit:
		emit_signal("unit_hovered", ui.unit)
		pass


func _on_hover_exited(ui):
#	var rect = ui.get_global_rect()
#	var mouse = get_global_mouse_position()
#	if mouse.x < rect.position.x:
#		print("TOO FAR LEFT")
#
#	if mouse.y < rect.position.y:
#		print("TOO FAR UP")
#
#	if mouse.x >= rect.position.x + rect.size.x:
#		print("TOO FAR RIGHT")
#
#	if mouse.y >= rect.position.y + rect.size.y:
#		print("TOO FAR DOWN")
#
	if not ui.get_global_rect().has_point(get_global_mouse_position()):
		print("Exited")
		print(ui.get_global_rect())
		print(get_global_mouse_position())
		print()
		emit_signal("unit_hover_exited")
#	else:
#		print("Not Exited")
#		print(ui.get_global_rect())
#		print(get_global_mouse_position())
#
#		print()
	pass


func _on_hover_entered_0():
	_on_hover_entered(ui_group[0])

func _on_hover_entered_1():
	_on_hover_entered(ui_group[1])

func _on_hover_entered_2():
	_on_hover_entered(ui_group[2])

func _on_hover_entered_3():
	_on_hover_entered(ui_group[3])

func _on_hover_entered_4():
	_on_hover_entered(ui_group[4])

func _on_hover_exited_0():
	_on_hover_exited(ui_group[0])

func _on_hover_exited_1():
	_on_hover_exited(ui_group[1])

func _on_hover_exited_2():
	_on_hover_exited(ui_group[2])

func _on_hover_exited_3():
	_on_hover_exited(ui_group[3])

func _on_hover_exited_4():
	_on_hover_exited(ui_group[4])
