extends KinematicBody

const ACCELERATION = 2
const MAX_SPEED = 20

var velocity = Vector3.ZERO
var temper = 1.0

#func _ready():
#	position = PlayerData.load_pos


func _physics_process(delta):
	var input_vector = Vector3.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.z = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	# print(input_vector)
	# Doesn't normalize for D-pad
	if(Input.is_action_pressed("keyboard_controls")):
		input_vector = input_vector.normalized()
	
	if input_vector != Vector3.ZERO:
		velocity += (input_vector * ACCELERATION)
		temper = 1.0
		if !Input.is_action_pressed("keyboard_controls") and 0.75 > max(abs(input_vector.x), abs(input_vector.y)):
			temper = 0.5
		velocity = velocity.limit_length(MAX_SPEED * temper)
		if $AnimationPlayer.current_animation != "run":
			$AnimationPlayer.play("run")
	else:
		velocity = velocity.linear_interpolate(Vector3.ZERO, 0.1)
		if $AnimationPlayer.current_animation != "idle":
			$AnimationPlayer.play("idle")
	
	# print(input_vector)
	move_and_slide(velocity)
	#z_index = position.y
	
#	for i in get_slide_count():
#		var collision = get_slide_collision(i)
#		print("Collided with: ", collision.collider.name)
