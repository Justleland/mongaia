extends MarginContainer

signal selected(unit)

var unit = null

onready var hp_bar = $VBox/HpBar
onready var stance = $VBox/TextSplit/Stance
onready var level = $VBox/TextSplit/Level
onready var indicator = $VBox/Indicator

func _ready():
	indicator.modulate.a = 0.0
	if unit != null:
		_update_ui()
		modulate.a = 1.0
	else:
		modulate.a = 0.0


func _gui_input(event):
	if unit is Unit and event.is_pressed():
		emit_signal("selected", unit)


func _update_ui():
	if unit != null:
		_update_hp()
		_update_stance()
		level.text = "Lvl: " + String(unit.monster.level)


func _update_hp():
	if unit != null:
		hp_bar.value = 100.0 * unit.hp / unit.max_hp


func _update_stance():
	if unit != null:
		stance.text = unit.stance


func init_unit(_unit: Unit):
	self.unit = _unit
	Utils.safe_connect(unit, "hp_def_changed", self, "_on_unit_hp_def_changed")
	Utils.safe_connect(unit, "level_changed", self, "_on_unit_level_changed")
	Utils.safe_connect(unit, "stance_changed", self, "_on_unit_stance_changed")
	Utils.safe_connect(unit, "targeted", self, "_on_unit_targeted")

func set_unit(_unit: Unit):
	if self.unit != null:
		clear()
		
	self.unit = _unit
	Utils.safe_connect(unit, "hp_def_changed", self, "_on_unit_hp_def_changed")
	Utils.safe_connect(unit, "level_changed", self, "_on_unit_level_changed")
	Utils.safe_connect(unit, "stance_changed", self, "_on_unit_stance_changed")
	Utils.safe_connect(unit, "targeted", self, "_on_unit_targeted")
	modulate.a = 1.0
	_update_ui()


func clear() -> void:
	_disconnect()
	unit = null
	modulate.a = 0.0


func _disconnect() -> void:
	if unit != null:
		Utils.safe_disconnect(unit, "hp_def_changed", self, "_on_unit_hp_def_changed")
		Utils.safe_disconnect(unit, "level_changed", self, "_on_unit_level_changed")
		Utils.safe_disconnect(unit, "stance_changed", self, "_on_unit_stance_changed")
		Utils.safe_disconnect(unit, "targeted", self, "_on_unit_targeted")


func _on_unit_stance_changed() -> void:
	_update_stance()


func _on_unit_hp_def_changed() -> void:
	_update_hp()


func _on_unit_targeted(is_targeted) -> void:
	if is_targeted is bool and is_targeted:
		indicator.modulate.a = 1.0
	else: 
		indicator.modulate.a = 0.0
