extends VBoxContainer

onready var name_level = $InforContainer/MarginGroup/MarginContainer/VBoxContainer/Name
onready var hp_bar = $InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin/VBox/HBox/Bar
onready var hp_label = $InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin/VBox/Margin/Numbers
onready var sp_bar = $InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin/VBox/HBox/Bar
onready var sp_label = $InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin/VBox/Margin/Numbers
onready var statuses = $StatusMargin/HBox

export var enemy_config = false
var ally_panel = load("res://resources/style/AllyUIPanel.tres")
var ally_highlight = load("res://resources/style/AllyUIPanelHighlight.tres")
var enemy_panel = load("res://resources/style/EnemyUIPanel.tres")
var enemy_highlight = load("res://resources/style/EnemyUIPanelHightlight.tres")
var status_scene = load("res://scenes/battleObjects/StatusIcon.tscn")
var monster: Monster setget set_monster
var unit: BattleUnit 
var active = false
var timer = 0

func _ready():
	modulate.a = 0.0
	if enemy_config:
		var left = "custom_constants/margin_left"
		var right = "custom_constants/margin_right"
		
		$InforContainer/MarginGroup/PanelMargin/Panel.set("custom_styles/panel", enemy_panel)
		_reverse_margins($InforContainer/MarginGroup/PanelMargin)
		
		_reverse_margins($InforContainer/MarginGroup/MarginContainer)
		$InforContainer/MarginGroup/MarginContainer/VBoxContainer/Name.align = Label.ALIGN_LEFT
		
		_reverse_margins($InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin)
		_add_margin_buffer($InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin, 4)
		$InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin/VBox/HBox.alignment = BoxContainer.ALIGN_BEGIN
		$InforContainer/MarginGroup/MarginContainer/VBoxContainer/HPMargin/VBox/Margin/Numbers.visible = false
		
		_reverse_margins($InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin)
		_add_margin_buffer($InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin, 2)
		$InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin/VBox/HBox.alignment = BoxContainer.ALIGN_BEGIN
		$InforContainer/MarginGroup/MarginContainer/VBoxContainer/SPMargin/VBox/Margin/Numbers.visible = false


func _process(delta):
	# 30 frame timer feel to fast, 45 frames feel to slow
	# will need to play test to get the right feel
	if active and modulate.a < 1.0:
		if timer >= 45:
			modulate.a = min(modulate.a + 0.02, 1.0)
		else:
			timer += 1
	elif !active and modulate.a > 0.0:
		if timer >= 45:
			modulate.a = max(modulate.a - 0.02, 0.0)
		else:
			timer += 1
		


func _add_margin_buffer(node, space: int) -> void:
	node.set("custom_constants/margin_bottom", space)


func _reverse_margins(node) -> void:
	var left = "custom_constants/margin_left"
	var right = "custom_constants/margin_right"
	var l_margin = node.get(left)
	var r_margin = node.get(right)
	node.set(left, r_margin)
	node.set(right, l_margin)


func start_visible_alpha() -> void:
	modulate.a = 1.0


func set_unit(value) -> void:
	if value is BattleUnit:
		unit = value
		Utils.safe_connect(value, "state_active", self, "_on_unit_state_active")
		Utils.safe_connect(value, "state_inactive", self, "_on_unit_state_inactive")
		Utils.safe_connect(value, "new_status_condition", self, "_on_unit_new_status_condition")
		Utils.safe_connect(value, "highlighted", self, "_on_unit_highlighted")
		Utils.safe_connect(value, "unhighlighted", self, "_on_unit_unhighlighted")


func set_monster(value: Monster) -> void:
	if monster != null:
		Utils.safe_disconnect(monster, "hp_changed", self, "_on_monster_hp_changed")
		Utils.safe_disconnect(monster, "max_hp_changed", self, "_on_monster_max_hp_changed")
		Utils.safe_disconnect(monster, "sp_changed", self, "_on_monster_sp_changed")
		Utils.safe_disconnect(monster, "max_sp_changed", self, "_on_monster_max_sp_changed")
		Utils.safe_disconnect(monster, "level_changed", self, "_on_monster_level_changed")
		
	monster = value
	if monster != null:
		Utils.safe_connect(monster, "hp_changed", self, "_on_monster_hp_changed")
		Utils.safe_connect(monster, "max_hp_changed", self, "_on_monster_max_hp_changed")
		Utils.safe_connect(monster, "sp_changed", self, "_on_monster_sp_changed")
		Utils.safe_connect(monster, "max_sp_changed", self, "_on_monster_max_sp_changed")
		Utils.safe_connect(monster, "level_changed", self, "_on_monster_level_changed")
	
	update()


func update():
	update_name_level()
	update_hp()
	update_sp()


func update_name_level():
	if monster != null:
		name_level.text = monster.monster_name + " Lvl: " + String(monster.level)


func update_hp():
	if monster != null:
		hp_bar.value = 100 * monster.hp / monster.max_hp
		hp_label.text = String(monster.hp) + " / " + String(monster.max_hp) 
		# print(String(monster.hp) + " / " + String(monster.max_hp))


func update_sp():
	if monster != null:
		sp_bar.value = 100 * monster.sp / monster.max_sp
		sp_label.text = String(monster.sp) + " / " + String(monster.max_sp)


func _on_monster_level_changed():
	update_name_level()


func _on_monster_hp_changed():
	update_hp()


func _on_monster_max_hp_changed():
	update_hp()


func _on_monster_sp_changed():
	update_sp()
	


func _on_monster_max_sp_changed():
	update_sp()


func _on_unit_state_active():
	active = true
	timer = 0
	set_monster(unit.monster)


func _on_unit_state_inactive():
	active = false
	timer = 0


func _on_unit_new_status_condition(condition: String, stacks: int) -> void:
	var status_icon = status_scene.instance().setup(unit, condition, stacks)
	statuses.add_child(status_icon)


func _on_unit_highlighted() -> void:
	if enemy_config:
		$InforContainer/MarginGroup/PanelMargin/Panel.set("custom_styles/panel", enemy_highlight)
	else:
		$InforContainer/MarginGroup/PanelMargin/Panel.set("custom_styles/panel", ally_highlight)


func _on_unit_unhighlighted() -> void:
	if enemy_config:
		$InforContainer/MarginGroup/PanelMargin/Panel.set("custom_styles/panel", enemy_panel)
	else:
		$InforContainer/MarginGroup/PanelMargin/Panel.set("custom_styles/panel", ally_panel)
