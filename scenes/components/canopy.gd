extends Polygon2D

func _ready():
	var collision = $StaticBody2D/CollisionPolygon2D
	if collision != null:
		collision.set_polygon(get_polygon())
