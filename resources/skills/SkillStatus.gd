extends Skill
class_name SkillStatus

export(Array, String) var target_stat_buff = []
export(int) var target_stat_buff_amount = 0
export(Array, String) var target_stat_debuff = []
export(int) var target_stat_debuff_amount = 0
export(Array, String) var self_stat_buff = []
export(int) var self_stat_buff_amount = 0
export(Array, String) var self_stat_debuff = []
export(int) var self_stat_debuff_amount = 0

export(Conditions) var target_condition_type
export(int) var target_min_condition_stack = 0
export(int) var target_max_condition_stack = 0
export(Conditions) var self_condition_type
export(int) var self_min_condition_stack = 0
export(int) var self_max_condition_stack = 0

func target_stat_change() -> bool:
	return (!target_stat_buff.empty() and target_stat_buff_amount > 0) or (!target_stat_debuff.empty() and target_stat_debuff_amount < 0)


func self_stat_change() -> bool:
	return (!self_stat_buff.empty() and self_stat_buff_amount > 0) or (!self_stat_debuff.empty() and self_stat_debuff_amount < 0)


func target_status_condition() -> bool:
	return (target_condition_type != Conditions.NONE and target_max_condition_stack > 0)


func self_status_condition() -> bool:
	return (self_condition_type != Conditions.NONE and self_max_condition_stack > 0)


func is_hit(attacker: BattleUnit, target: BattleUnit) -> bool:
	if self.target.ally and self.target.self_target:
		return true
	var e_acc = float(acc)
	return _move_hit(target.dodge, (e_acc + _percentage_mod(e_acc, attacker.intellect, 0.4)))
