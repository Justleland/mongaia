extends HBoxContainer

signal tab_action(action)

var unit: Unit = null
var switch_targets = []

onready var current_monster = $CurrentMonster
onready var switch_monster = $SwitchMonster
onready var buttons = [
	$Buttons/SwitchButton,
	$Buttons/SwitchButton2,
	$Buttons/SwitchButton3
]

func _ready():
	visible = false

func activate(_unit: Unit, _switch_targets: Array) -> void:
	visible = true
	unit = _unit
	switch_targets = _switch_targets
	
	current_monster.set_info(unit)
	
	switch_monster.visible = false
	for target in switch_targets:
		if target is Unit and target.hp > 0:
			switch_monster.visible = true
			switch_monster.set_info(target)
			break
	
	for i in range(buttons.size()):
		if switch_targets.size() > i:
			buttons[i].set_unit(switch_targets[i])
		else:
			buttons[i].set_unit(null)


func deactivate() -> void:
	unit = null
	switch_targets = []
	for button in buttons:
		button.set_unit(null)
	visible = false


func _on_button_mouse_entered(pos):
	var switch = buttons[pos].unit
	if switch != null:
		switch_monster.set_info(switch)


func _on_button_pressed(pos):
	var switch = buttons[pos].unit
	if switch != null:
		emit_signal("tab_action", {
			"type": "SWITCH",
			"side": "PLAYER",
			"acting": unit,
			"switch_to": switch
		})
		pass
