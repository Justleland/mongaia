extends MarginContainer

onready var new_skill_btn = $HBox/Margin/VBox/NewSkillBtn
onready var new_skill_desc = $HBox/VBox/NewSkillDesc
onready var old_skill_desc = $HBox/VBox/OldSkillDesc
onready var old_skill_btns = [
	$HBox/Margin/VBox/Margin/BtnMargin/VBox/OldSkill0,
	$HBox/Margin/VBox/Margin/BtnMargin/VBox/OldSkill1,
	$HBox/Margin/VBox/Margin/BtnMargin/VBox/OldSkill2,
	$HBox/Margin/VBox/Margin/BtnMargin/VBox/OldSkill3,
	$HBox/Margin/VBox/Margin/BtnMargin/VBox/OldSkill4
]
onready var old_skill_btns_ui = $HBox/Margin/VBox/Margin

var active = false
var learner: Monster = null
var new_skill = null
var new_skill_list = []
var old_skills = []
var forget = false
var in_focus = false

signal inactive

func _ready():
	new_skill_desc.visible = true
	old_skill_btns_ui.visible = false
	visible = false


func _input(event):
	if active and !forget and (event.is_action_released("ui_accept") or event.is_action_released("mouse_controls")):
		_learn_skill_from_list()
		
	if forget:
		if in_focus: 
			if event.is_action_released("mouse_controls"):
				in_focus = false
				if new_skill_btn.has_focus():
					new_skill_btn.release_focus()
				else:
					for button in old_skill_btns:
						if button.has_focus():
							button.release_focus()
							break
		else:
			if !old_skill_btns[0].disabled and (event.is_action_released("keyboard_controls") or event.is_action_released("controller_controls")):
				in_focus = true
				old_skill_btns[0].grab_focus()
	
	if event.is_action_released("ui_menu_toggle") and active:
		old_skill_desc.toggle()


func setup(monster: Monster, lvl_start: int, lvl_end: int) -> void:
	if monster is Monster:
		var skills_learned = monster.monsterData.get_skills_from_level(lvl_start + 1, lvl_end)
		if !skills_learned.empty():
			active = true
			_set_learner(monster)
			new_skill_list = skills_learned
			if monster.skills_known.size() >= 5:
				_set_old_skills(monster.skills_known)


func _learn_skill_from_list() -> void:
	var skill = new_skill_list.pop_front()
	if skill != null:
		_learn_skill(skill)
	else:
		active = false
		emit_signal("inactive")


func _learn_skill(skill) -> void:
	new_skill = skill
	new_skill_btn.text = new_skill.skill_name
	new_skill_desc.set_with_skill(new_skill)
	if learner.skills_known.size() >= 5:
		old_skill_btns_ui.visible = true
		_set_old_skills(learner.skills_known)
		forget = true
	else:
		old_skill_btns_ui.visible = false
		learner.skills_known.append(new_skill)


func _set_learner(monster: Monster) -> void:
	learner = monster
	if learner is Monster:
		$HBox/Margin/VBox/MonsterCard/VBox/HBox/Name.text = learner.monster_name
		$HBox/Margin/VBox/MonsterCard/VBox/HBox/Level.text = "Lvl " + String(learner.level)

func _set_old_skills(skills: Array) -> void:
	if skills.size() >= 5:
		old_skills = skills
		old_skill_desc.set_with_skill(old_skills[0])
		for i in range(0, 5):
			old_skill_btns[i].text = old_skills[i].skill_name


func _on_OldSkill_button_up(btn_num):
	if forget:
		old_skills.remove(btn_num)
		old_skills.append(new_skill)
		learner.skills_known = old_skills
		forget = false
		_learn_skill_from_list()


func _on_NewSkillBtn_button_up():
	if forget:
		forget = false
		_learn_skill_from_list()


func _on_button_focus_entered(btn_num):
	if forget:
		old_skill_desc.set_with_skill(old_skills[btn_num])
