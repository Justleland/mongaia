extends HBoxContainer

signal tab_action(action)
signal tab_skill(skill)
signal tab_switch()

var unit: Unit = null

onready var stat_info = $MenuUnitStatInfo
onready var name_level = $VBox/NameLevel
onready var species = $VBox/Species

# Buttons
onready var stance_btn = $VBox/StanceHBox/StanceBtn
onready var stance_btn_left = $VBox/StanceHBox/StanceLeftBtn
onready var stance_btn_right = $VBox/StanceHBox/StanceRightBtn
onready var switch_btn = $VBox/BtnHBox/VBox/SwitchBtn
onready var pass_btn = $VBox/BtnHBox/VBox/PassBtn

onready var skill_btns = [
	$VBox/BtnHBox/Grid/SkillBtn1,
	$VBox/BtnHBox/Grid/SkillBtn2,
	$VBox/BtnHBox/Grid/SkillBtn3,
	$VBox/BtnHBox/Grid/SkillBtn4
]

func _ready():
	Utils.safe_connect(stance_btn, "button_up", self, "_on_button_stance_next")
	Utils.safe_connect(stance_btn_left, "button_up", self, "_on_button_stance_previous")
	Utils.safe_connect(stance_btn_right, "button_up", self, "_on_button_stance_next")
	Utils.safe_connect(switch_btn, "button_up", self, "_on_button_switch")
	Utils.safe_connect(pass_btn, "button_up", self, "_on_button_pass")


func set_unit(_unit: Unit) -> void:
	if _unit != null:
		unit = _unit
		stat_info.set_info(_unit)
		name_level.text = unit.monster.monster_name + " Lvl: " + String(unit.level)
		stance_btn.text = unit.stance
		
		if unit.monster.monster_name != unit.monster.monsterData.species:
			species.text = unit.monster.monsterData.species
			species.modulate.a = 1.0
		else:
			species.modulate.a = 0.0
		
		for i in range(skill_btns.size()):
			if _unit.skills.size() > i:
				skill_btns[i].text = _unit.skills[i].skill_name
				skill_btns[i].disabled = false
			else:
				skill_btns[i].text = "---"
				skill_btns[i].disabled = true


func _on_button_stance_next():
	match unit.stance:
		BattleUtils.ST_NEU:
			unit.set_stance(BattleUtils.ST_STR)
		BattleUtils.ST_STR:
			unit.set_stance(BattleUtils.ST_DEX)
		BattleUtils.ST_DEX:
			unit.set_stance(BattleUtils.ST_INT)
		BattleUtils.ST_INT:
			unit.set_stance(BattleUtils.ST_NEU)
	
	stance_btn.text = unit.stance


func _on_button_stance_previous():
	match unit.stance:
		BattleUtils.ST_NEU:
			unit.set_stance(BattleUtils.ST_INT)
		BattleUtils.ST_STR:
			unit.set_stance(BattleUtils.ST_NEU)
		BattleUtils.ST_DEX:
			unit.set_stance(BattleUtils.ST_STR)
		BattleUtils.ST_INT:
			unit.set_stance(BattleUtils.ST_DEX)
	
	stance_btn.text = unit.stance


func _on_button_switch():
	emit_signal("tab_switch")


func _on_button_pass():
	emit_signal("tab_action", {
		"type": "PASS",
		"acting": unit
	}) 


func _on_SkillBtn_pressed(pos):
	var skill = unit.skills[pos]
	if skill is Skill:
		emit_signal("tab_skill", skill)
