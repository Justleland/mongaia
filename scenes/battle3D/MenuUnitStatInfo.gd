extends VBoxContainer

onready var family1 = $IconFamilyHBox/VBox/Family1
onready var family2 = $IconFamilyHBox/VBox/Family2
onready var hp = $StatItemHBox/VBox/HP
onready var def = $StatItemHBox/VBox/MarginContainer/StatsGrid/Def
onready var label_def = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelDef
onready var agi = $StatItemHBox/VBox/MarginContainer/StatsGrid/Agi
onready var label_agi = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelAgi
onready var strength = $StatItemHBox/VBox/MarginContainer/StatsGrid/Str
onready var label_str = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelStr
onready var dexterity = $StatItemHBox/VBox/MarginContainer/StatsGrid/Dex
onready var label_dex = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelDex
onready var intellect = $StatItemHBox/VBox/MarginContainer/StatsGrid/Int
onready var label_int = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelInt
onready var acc = $StatItemHBox/VBox/MarginContainer/StatsGrid/Acc
onready var label_acc = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelAcc
onready var dodge = $StatItemHBox/VBox/MarginContainer/StatsGrid/Dodge
onready var label_dodge = $StatItemHBox/VBox/MarginContainer/StatsGrid/LabelDodge

func set_info(unit: Unit) -> void:
	if unit != null:
		family1.text = unit.family1.text
		
		if unit.family2 is Family:
			family2.text = unit.family2.text
			family2.visible = true
		else:
			family2.visible = false
		
		hp.text = "HP:  " + String(unit.hp) + "/" + String(unit.max_hp)
		def.text = String(unit.def)
		agi.text = String(unit.agi)
		strength.text = String(unit.strength)
		dexterity.text = String(unit.dexterity)
		intellect.text = String(unit.intellect)
		# TODO: Need to add Accuracy to unit/monster
		acc.text = "100"
		dodge = String(unit.dodge)
