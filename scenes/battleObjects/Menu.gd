extends VBoxContainer

onready var buttons = [
	$Button0,
	$Button1,
	$Button2,
	$Button3,
	$Button4,
	$Button5
]

var in_focus = false

signal button_response(num)
signal button_focus(num)

func _ready():
	for button in buttons:
		button.visible = false
		button.disabled = true



func _process(delta):
	if in_focus:
		if Input.is_action_just_released("mouse_controls"):
			in_focus = false
			for button in buttons:
				if button.has_focus():
					button.release_focus()
					break
	else:
		if !buttons[0].disabled and (Input.is_action_just_released("keyboard_controls") or Input.is_action_just_released("controller_controls")):
			in_focus = true
			buttons[0].grab_focus()


func set_buttons(array: Array) -> void:
	_deactivate_buttons()
	for i in range(array.size()):
		_activate_button(buttons[i], array[i])
	if in_focus:
		buttons[0].grab_focus()

func _activate_button(button: Button, move_name: String) -> void:
	if move_name is String:
		button.text = move_name
		button.visible = true
		button.disabled = false
	else:
		button.visible = false
		button.disabled = true


func _deactivate_buttons() -> void:
	for button in buttons:
		button.visible = false
		button.disabled = true


func _on_button_up(num):
	emit_signal("button_response", num)


func _on_button_focus_entered(num):
	emit_signal("button_focus", num)
