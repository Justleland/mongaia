extends KinematicBody2D

const ACCELERATION = 60
const MAX_SPEED = 400
const MAX_DIST = 160

var velocity = Vector2.ZERO

func _ready():
	position = PlayerData.load_pos

func follow(leader_pos: Vector2, leader_temper):
	var distance = position.distance_to(leader_pos)
	var direction = position.direction_to(leader_pos)
	
	if distance > MAX_DIST:
		velocity += (direction * ACCELERATION)
		velocity = velocity.clamped(MAX_SPEED * leader_temper)
	else:
		velocity = velocity.linear_interpolate(Vector2.ZERO, 0.1)
	
	#print(input_vector)
	move_and_slide(velocity)
	#z_index = position.y


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
