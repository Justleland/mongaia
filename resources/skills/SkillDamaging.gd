extends Skill
class_name SkillDamaging

export(int) var power = 20
export(int) var hit_count = 1
export(int) var penetration = 0 #setget ,_get_penetration
export(int) var crit = 4 #setget ,_get_crit
export(bool) var recoil = false
export(bool) var hp_steal = false
export(int) var status_chance = 0

export(Array, Resource) var target_statuses = []
export(int) var target_status_count = 0
export(Array, Resource) var self_statuses = []
export(int) var self_status_count = 0

func result(acting: Unit, targets: Array) -> Array:
	var results = []
	for _target in targets:
		var is_hit = _calculate_hit(acting, _target)
		var damage = _calculate_damage(acting, _target)
		var is_crit = _calculate_crit(acting.stance)
		var mod = _calculate_modifier(acting, _target)
		results.append({
			"type": "DAMAGE",
			"acting": acting,
			"target": _target,
			"is_hit": is_hit,
			"is_crit": is_crit,
			"damage": damage,
			"modifier": mod
		})
		
		if is_hit:
			if status_chance > 0:
				results.append({
					"type": "STATUS",
					"acting": acting,
					"target": _target,
					"is_hit": true
				})
			if hp_steal:
				results.append({
					"type": "HEAL",
					"acting": acting,
					"target": acting,
					"amount": damage * 0.2 * mod
				})
			if recoil:
				results.append({
					"type": "RECOIL",
					"acting": acting,
					"target": acting,
				})
	return results
#	var instructions = []
#	for setup in setup_instructions:
#		if setup is Instruction:
#			var instruction = setup.instruct({
#				"acting": acting 
#			})
#			instruction["acting"] = acting
#			instructions.append(instruction)
#
#	instructions += _format_hit_instructions(acting, targets)
#
#	return instructions


func _calculate_hit(acting: Unit, _target: Unit) -> bool:
	var e_acc = _calculate_e_acc(acting.stance) * (float(acting.acc - _target.dodge + 5) / 100.0)
	var r = randi() % 100 + 1
	return r <= e_acc


func _calculate_damage(acting: Unit, _target: Unit) -> int:
	var true_dmg = acting.atk * (_calculate_e_power(acting.stance) / 100.0)
	var pent = max(0.0, (100.0 - _calculate_e_pent(acting.stance))) / 100.0
	var stance_mod = _calculate_stance_mod(acting.stance, _target.stance)
	var dmg_ratio = float(acting.atk) / (float(_target.def) * pent * stance_mod)
	return int(true_dmg * dmg_ratio)


func _calculate_stance_mod(a_stance, t_stance) -> float:
	match a_stance:
		BattleUtils.ST_STR:
			if t_stance == BattleUtils.ST_INT:
				return 0.66
			if t_stance == BattleUtils.ST_DEX:
				return 1.5
		BattleUtils.ST_DEX:
			if t_stance == BattleUtils.ST_STR:
				return 0.66
			if t_stance == BattleUtils.ST_INT:
				return 1.5
		BattleUtils.ST_INT:
			if t_stance == BattleUtils.ST_DEX:
				return 0.66
			if t_stance == BattleUtils.ST_STR:
				return 1.5
	return 1.0


func _calculate_e_power(stance) -> float:
	var p = float(power)
	if stance == BattleUtils.ST_STR:
		if power >= 95: 
			return p + 5.0
		if power >= 65:
			return p + 10.0
		if power >= 35:
			return p + 15.0
		return p + 20.0
		
	if stance == BattleUtils.ST_INT:
		if power > 100:
			return p - 15.0
		if power > 70:
			return p - 10.0
		return max(0.0, p - 5.0)
		 
	return p


func _calculate_e_pent(stance) -> float:
	var pent = float(penetration)
	if stance == BattleUtils.ST_DEX:
		if penetration <= 10:
			return pent * 2.0
		else: 
			return pent * 1.5
			
	if stance == BattleUtils.ST_STR:
		if penetration <= 10:
			return pent * 0.6
		else:
			return pent * 0.4
			
	return pent

func _calculate_e_acc(stance) -> float:
	var e_acc = float(acc)
	if stance == BattleUtils.ST_DEX:
		if acc >= 100:
			return e_acc + 5.0
		if acc >= 65:
			return e_acc + 10.0
		return e_acc + 15.0
		
	if stance == BattleUtils.ST_STR:
		if acc > 75:
			return e_acc - 10.0
		if acc > 50:
			return e_acc - 7.0
		return e_acc - 5.0
		
	return e_acc


func _calculate_crit(stance) -> bool:
	var r = randi() % 100 + 1
	var e_crit = float(crit)
	if penetration <= 0:
		if stance == BattleUtils.ST_DEX:
			if crit >= 100:
				return r < e_crit * 1.10
			if crit >= 50:
				return r < e_crit * 1.25
			if crit >= 25:
				return r < e_crit * 1.5
			return r < e_crit * 2.0
		
		if stance == BattleUtils.ST_STR:
			if crit >= 4:
				return r < e_crit * 0.5
			return r < e_crit * 0.0
	
	return r < crit


func _calculate_modifier(acting: Unit, _target: Unit) -> float:
	return _target.monster.get_multiplier_for(element)


func _result(acting: Unit, targets: Array) -> Dictionary:
	var result = {
		"type": "DAMAGE",
		"acting": acting,
	}
	
	var e_pent = get_e_pent(acting)
	var e_crit = get_e_crit(acting)
	var e_charge = get_e_charge(acting)
	var e_acc = get_e_acc(acting)
	var e_status_chance = get_e_status_chance(acting)
	
	var hits = []
	
	for target in targets:
		var e_dmg = get_e_dmg(acting, target)
		var true_dmg = get_true_dmg(e_dmg, e_pent)
		print("Damage: " + String(e_dmg))
		hits.append({
			"acting": acting,
			"target": target,
			"is_hit": randf() < (e_acc / 100.0),
			"is_dodge": randf() < (target.dodge / 100.0),
			"true_dmg": true_dmg,
			"dmg": int(max(0, e_dmg - true_dmg)),
			"eff_multiplier": target.monster.monsterData.get_family_multiplier_for(element),
			"stab_multiplier": get_stab(acting.stance, target.stance),
			"is_crit": randf() < (e_crit / 100.0),
			"charge": e_charge
		})
	
	result["hits"] = hits
	return result


func _format_hit_instructions(acting: Unit, targets: Array) -> Array:
	var instructions = []
	var status_targets = []
	
	var e_pent = get_e_pent(acting)
	var e_crit = get_e_crit(acting)
	var e_charge = get_e_charge(acting)
	var e_acc = get_e_acc(acting)
	var status_chance = randf() < (get_e_status_chance(acting) / 100.0)
	
	for _i in range(hit_count):
		for hit_inst in hit_instructions:
			if hit_inst is Instruction:
				var instruction = hit_inst.instruct({
					"acting": acting 
				})
				instructions.append(instruction)
			if hit_inst is PackedScene:
				var instruction = {
					"type": "ANIMATION_SCENE",
					"scene": hit_inst,
					"acting": acting,
					"targets": targets
				}
				instructions.append(instruction)
		
		var damage_inst = _format_damage_instructions(acting, targets)
		instructions.append(damage_inst)
		if status_chance:
			for dict in damage_inst["hits"]:
				if dict["is_hit"] and !dict["is_dodge"] and !status_targets.has(dict["target"]):
					status_targets.append(dict["target"])
		
	if status_chance:
		for status in target_statuses:
			instructions.append(_format_status_instructions(acting, status_targets, status, target_status_count))
				
		for status in self_statuses:
			instructions.append(_format_status_instructions(acting, [acting], status, self_status_count))
			
	return instructions


func _format_damage_instructions(acting: Unit, targets: Array) -> Dictionary:
	var result = {
		"type": "DAMAGE",
		"acting": acting,
		"hits": []
	}
	
	var e_pent = get_e_pent(acting)
	var e_crit = get_e_crit(acting)
	var e_charge = get_e_charge(acting)
	var e_acc = get_e_acc(acting)
	
	for target in targets:
		var e_dmg = get_e_dmg(acting, target)
		var true_dmg = get_true_dmg(e_dmg, e_pent)
		print("Damage: " + String(e_dmg))
		result["hits"].append({
			"acting": acting,
			"target": target,
			"is_hit": randf() < (e_acc / 100.0),
			"is_dodge": randf() < (target.dodge / 100.0),
			"true_dmg": true_dmg,
			"dmg": int(max(0, e_dmg - true_dmg)),
			"eff_multiplier": target.monster.monsterData.get_family_multiplier_for(element),
			"stab_multiplier": get_stab(acting.stance, target.stance),
			"is_crit": randf() < (e_crit / 100.0),
			"charge": e_charge
		})
	
	return result


func _format_status_instructions(acting: Unit, targets: Array, status, count) -> Dictionary:
	return {
		"type": "STATUS",
		"acting": acting,
		"targets": targets,
		"status": status,
		"count": count
	}


func damage(attacker: BattleUnit, defender: BattleUnit) -> int:
	return 0


func get_e_dmg(unit: Unit, target: Unit) -> int:
	var dmg = (2.0 * unit.level / 5.0) * get_e_power(unit) * (float(unit.get_stance_value()) / float(target.get_stance_value())) / 50.0 + 2.0
	print("UNIT STANCE: " + String(unit.get_stance_value()))
	print("TARGET STANCE: " + String(target.get_stance_value()))
	return int(dmg)

func get_e_power(unit: Unit) -> int:
	if unit.stance == BattleUtils.ST_STR:
		var stat = _level_mod(power, unit.level, unit.strength, 0.2, 0.4)
		print("POWER: " + String(stat))
		return stat
	print("POWER: " + String(power))
	return power

func get_e_pent(unit: Unit) -> int:
	if unit.stance == BattleUtils.ST_DEX:
		return _level_mod(penetration, unit.level, unit.dexterity, 0.5, 0.5)
	return penetration


func get_e_crit(unit: Unit) -> int:
	if unit.stance == BattleUtils.ST_DEX:
		return int(min(_level_mod(crit, unit.level, unit.dexterity, 0.5, 0.5), 100.0))
	return crit


func get_e_acc(unit: Unit) -> int:
	if unit.stance == BattleUtils.ST_DEX:
		return _level_mod(acc, unit.level, unit.dexterity, 0.2, 0.4)
	return acc


func get_e_status_chance(unit: Unit) -> int:
	if unit.stance == BattleUtils.ST_INT:
		return _level_mod(status_chance, unit.level, unit.intellect, 0.2, 0.4)
	return status_chance


func get_e_status_count(unit: Unit, count) -> int:
	if unit.stance == BattleUtils.ST_INT:
		return _level_mod(count, unit.level, unit.intellect, 0.5, 0.5)
	return count


func get_e_charge(unit: Unit) -> int:
	return charge


func get_true_dmg(e_dmg: int, e_pent) -> int:
	return int(e_dmg * e_pent / 100.0)


func get_stab(a_stance, b_stance) -> float:
	if a_stance == BattleUtils.ST_NEU or a_stance == b_stance:
		return 1.0
	if a_stance == BattleUtils.ST_STR:
		match b_stance:
			BattleUtils.ST_DEX:
				return 0.75
			BattleUtils.ST_INT:
				return 1.5
	if a_stance == BattleUtils.ST_DEX:
		match b_stance:
			BattleUtils.ST_INT:
				return 0.75
			BattleUtils.ST_STR:
				return 1.5
	if a_stance == BattleUtils.ST_INT:
		match b_stance:
			BattleUtils.ST_STR:
				return 0.75
			BattleUtils.ST_DEX:
				return 1.5
	return 1.0

# Need stat calculating function

#func target_stat_change() -> bool:
#	return (!target_stat_buff.empty() and target_stat_buff_amount > 0) or (!target_stat_debuff.empty() and target_stat_debuff_amount > 0)
#
#
#func self_stat_change() -> bool:
#	return (!self_stat_buff.empty() and self_stat_buff_amount > 0) or (!self_stat_debuff.empty() and self_stat_debuff_amount > 0)
#
#
#func target_status_condition() -> bool:
#	return (target_condition_type != Conditions.NONE and target_max_condition_stack > 0) or target_condition_type == Conditions.STUNNED
#
#
#func self_status_condition() -> bool:
#	return (self_condition_type != Conditions.NONE and self_max_condition_stack > 0)
