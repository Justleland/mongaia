extends HBoxContainer

var turn_order: TurnOrder = null
onready var cards = get_children()
onready var divider = $Divider

func _ready():
	cards.pop_back()
#	divider.visible = false
#	for card in cards:
#		card.visible = false


func update():
	if turn_order != null:
		var order = turn_order.get_order()
		var offset = 0
		var i = 0
		while i <= cards.size() and i < order.size():
			if order[i] is Unit:
				var card = cards[i + offset]
				var unit = order[i]
				card.set_name(unit.monster.monster_name)
				card.set_agility(unit.agi + unit.charge)
				card.visible = true
			else:
				move_child(divider, i)
				offset -= 1
				divider.visible = i != 0 and i != order.size() - 1
			i += 1
		
		while i + offset < cards.size():
			cards[i + offset].visible = false
			i += 1


func set_turn_order(turn_order: TurnOrder) -> void:
	if turn_order != null:
		self.turn_order = turn_order
		update()
