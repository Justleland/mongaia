extends Resource
class_name Status

enum Type {
	PRIORITY
	FATIGUE
	RESTORE
	REGENERATE
	QUICK
	SLOW
	POISONED
	BURNED
	FROZEN
	PARALYZED
	CONFUSED
}

export(Type) var type 
export var label = ""
export(String, MULTILINE) var description = ""
export var stackable = true

func get_priority() -> int:
	match type:
		Type.PRIORITY:
			return 1
		Type.FATIGUE:
			return -2
		_:
			return 0
