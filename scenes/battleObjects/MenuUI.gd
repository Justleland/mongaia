extends MarginContainer

class_name MenuUI

#Need to fix Children Name
enum {
	INACTIVE
	BASE
	STANCE
	SKILL
	TARGET
	ITEM
	SWITCH
	FAINT_SWITCH
}

onready var menu = $Menu
onready var description = $Node2D/Description

var state = INACTIVE

var unit: BattleUnit = null

var base_options = ["Stance", "Skills", "Items", "Switch", "Pass", "Run"]
var stance_options = ["Neutral", "Strength", "Dexterity", "Intellect"]
var skill_options = []
var button_values = []
var enemy_targets = []
var ally_targets = []
var option_values = []

signal menu_closed
signal button_response(current_state, num)
signal revert_state(current_state)
signal title_message(text)

func initiate(menu_unit: BattleUnit, enemies: Array, allies: Array) -> void:
	unit = menu_unit
	unit.highlight()
	enemy_targets = enemies
	ally_targets = allies
	_set_buttons_and_state(BASE)


func switch(menu_unit: BattleUnit, allies: Array) -> void:
	unit = menu_unit
	unit.highlight()
	ally_targets = allies
	var monsters = _viable_switch_monsters()
	if !monsters.empty():
		_set_buttons_and_state(FAINT_SWITCH, monsters)
		if description.toggled:
			description.visible = true
	else:
		unit.state = BattleUnit.INACTIVE
		_set_buttons_and_state(INACTIVE)


func _input(event):
	if event.is_action_released("ui_cancel") and state != INACTIVE:
		_revert_state()
	if event.is_action_released("ui_menu_toggle") and state != INACTIVE and state != BASE:
		description.toggle()


func _set_buttons_and_state(change_state, options: Array = []) -> void:
	match change_state:
		INACTIVE:
			state = INACTIVE
			unit.unhighlight()
			unit = null
			menu.set_buttons([])
			description.visible = false
			emit_signal("menu_closed")
		BASE:
			menu.set_buttons(base_options)
			description.visible = false
			state = BASE
		STANCE:
			_set_stance_option_values()
			menu.set_buttons(stance_options)
			state = STANCE
			_on_Menu_button_focus(0)
		SKILL:
			option_values = unit.monster.skills_known
			menu.set_buttons(_get_skill_names())
			state = SKILL
			_on_Menu_button_focus(0)
		TARGET:
			menu.set_buttons(options)
			state = TARGET
		SWITCH:
			menu.set_buttons(options)
			state = SWITCH
			_on_Menu_button_focus(0)
		FAINT_SWITCH:
			menu.set_buttons(options)
			state = FAINT_SWITCH
			_on_Menu_button_focus(0)


func set_buttons(value: Array) -> void:
	menu.set_buttons(value)


func _get_skill_names() -> Array:
	var skill_names = []
	for skill in unit.monster.skills_known:
		skill_names.append(skill.skill_name)
	return skill_names


func _set_stance_option_values() -> void:
	var neutral = "The user takes a balanced approach gaining an portion of the benefits from their STRENGTH, DEXTERITY, and INTELLECT."
	var strength = "The user assumes an aggressive position and uses all of their might with every attack. This boosts skill power."
	var dexterity = "The user displays finese by finding their opponent's weakness using percise attacks. This boosts skill accuracy, penatration and crit chance."
	var intellect = "The user gathers information about the battle to perform attacks efficiently. This boosts status condition chance and reduces damage reduction."
	option_values = [neutral, strength, dexterity, intellect]


func _on_Menu_button_response(num):
	match state:
		BASE:
			_manage_base_menu_response(num)
		STANCE:
			_manage_stance_menu_response(num)
		SKILL:
			_manage_skill_menu_response(num)
		TARGET:
			_manage_target_menu_response(num)
		SWITCH:
			_manage_switch_menu_response(num)
		FAINT_SWITCH:
			_manage_switch_menu_response(num)

func _manage_base_menu_response(num: int) -> void:
	if description.toggled:
		description.visible = true
	var value = base_options[num]
	match value:
		"Stance":
			_set_buttons_and_state(STANCE)
		"Skills":
			_set_buttons_and_state(SKILL)
		"Items":
			pass
		"Switch":
			var monsters = _viable_switch_monsters()
			if !monsters.empty():
				_set_buttons_and_state(SWITCH, monsters)
			else:
				emit_signal("title_message", "No available monsters")
		"Pass":
			unit.state = BattleUnit.WAIT
			unit._set_sprite_color(1, 1, 1)
			_set_buttons_and_state(INACTIVE)
		"Run":
			get_tree().change_scene(PlayerData.previous_area)


func _manage_stance_menu_response(num: int) -> void:
	var value = stance_options[num]
	match value:
		"Neutral":
			unit.stance = BattleUnit.Stance.NEUTRAL
		"Strength":
			unit.stance = BattleUnit.Stance.STRENGTH
		"Dexterity":
			unit.stance = BattleUnit.Stance.DEXTERITY
		"Intellect":
			unit.stance = BattleUnit.Stance.INTELLECT
	
	_revert_state()


func _manage_skill_menu_response(skill_num: int) -> void:
	var skill = unit.monster.skills_known[skill_num]
	if unit.monster.sp >= skill.sp_cost:
		unit.charge_time = skill.charge_time
#		print("Move Cast Time: " + String(skill.charge_time))
#		print("Set Cast Time: " + String(unit_in_menu.charge_time))
#		print("")
		unit.skill = skill
		
		if skill.target == Skill.Target.MULTI:
			var array = []
			for enemy in enemy_targets:
				if enemy is BattleUnit and enemy.monster is Monster:
					array.append(enemy)
			_target_and_charge(array)
		elif skill.target == Skill.Target.SELF:
			_target_and_charge([unit])
		else:
			var target_names = []
			var units = []
			var viable_targets = []
			if unit.skill.side == Skill.Side.ENEMY:
				units = enemy_targets
			else:
				units = ally_targets
			for unit in units:
				if unit.monster is Monster:
					target_names.append(unit.monster.monster_name)
					viable_targets.append(unit)
			option_values = viable_targets
			_set_buttons_and_state(TARGET, target_names)


func _manage_target_menu_response(num: int):
	_target_and_charge([option_values[num]])


func _manage_switch_menu_response(num: int):
	unit.monster = option_values[num]
	unit._set_sprite_color(1, 1, 1)
	_set_buttons_and_state(INACTIVE)
	

func _target_and_charge(targets: Array):
	unit.target = targets
	unit.monster.subtract_sp(unit.skill.sp_cost)
	unit.state = BattleUnit.CHARGE
	unit._set_sprite_color(1, 0, 1)
	_set_buttons_and_state(INACTIVE)


func _viable_switch_monsters() -> Array:
	var monsters = []
	var names = []
	for monster in PlayerData.team:
		if monster.hp > 0:
			var viable = true
			for ally in ally_targets:
				if ally.monster == monster:
					viable = false
					break;
			if viable:
				monsters.append(monster)
				names.append(monster.monster_name)
	
	option_values = monsters
	return names

func _revert_state():
	match state:
		TARGET:
			_set_buttons_and_state(SKILL)
		FAINT_SWITCH:
			pass
		_:
			_set_buttons_and_state(BASE)


func _on_Menu_button_focus(num):
	match state:
		STANCE:
			description.set_with_stance(stance_options[num], option_values[num], unit)
		SKILL:
			if option_values.size() >= num + 1:
				var value = option_values[num]
				if value is Skill:
					description.set_with_skill(value)
		SWITCH:
			_monster_description(num)
		FAINT_SWITCH:
			_monster_description(num)


func _monster_description(num) -> void:
	if option_values.size() >= num + 1:
		var value = option_values[num]
		if value is Monster:
			description.set_monster_view(value)
