extends Node

var previous_scene: String = ""
var previous_config: Dictionary = {}
var debug = true

func _ready():
	randomize()


func log_print(arg) -> void:
	if debug:
		if arg is Array:
			for a in arg:
				print(a)
		else:
			print(arg)
		print()

func safe_connect(emitter: Object, signal_name: String, listener: Object, signal_func: String):
	emitter.connect(signal_name, listener, signal_func)
	
	
func safe_disconnect(emitter: Object, signal_name: String, listener: Object, signal_func: String):
	emitter.disconnect(signal_name, listener, signal_func)
	pass

func safe_load_skill(name) -> Skill:
#	print(name)
	if name is String:
		var skill = load("res://resources/skills/impl/" + name + ".tres")
		if skill is Skill:
			return skill
	return null 


func go_to_scene(path: String, parameters: Dictionary = { "name": "Working"}, pause: bool = false):
	call_deferred("_deferred_go_to_scene", path, parameters)
	get_tree().paused = pause


func _deferred_go_to_scene(path, parameters):
	var root = get_tree().get_root()
	var current_scene = root.get_child(root.get_child_count() - 1)
	previous_scene = current_scene.get_filename()
	previous_config = current_scene.get_parameters()
	current_scene.free()
	
	var scene = ResourceLoader.load(path)
	current_scene = scene.instance()
	current_scene.init(parameters)
	root.add_child(current_scene)
	get_tree().set_current_scene(current_scene)
	get_tree().paused = false


func go_to_previous(parameters:= {}, pause: bool = false):
	if previous_scene == null or previous_scene.empty():
		previous_scene = "res://scenes/rooms/overworld/riverway/room01.tscn"
	
	for key in parameters.keys():
		previous_config[key] = parameters[key]
		
	go_to_scene(previous_scene, previous_config, pause)
