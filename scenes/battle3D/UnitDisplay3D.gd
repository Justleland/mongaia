extends AnimatedSprite3D

var unit: Unit = null
var is_flicker = false
var is_shadowed = false

func _physics_process(delta):
	if is_flicker:
		if is_shadowed and modulate.v < 1.0:
			modulate.v += 0.005
			if modulate.v >= 1.0:
				is_shadowed = false
		else:
			modulate.v -= 0.005
			if modulate.v <= 0.65:
				is_shadowed = true
		

func setup(unit: Unit) -> void:
	self.unit = unit
	frames = BattleUtils.load_monster_spriteframes(unit.monster.monsterData.spriteframes_path)


func set_flicker(toggle: bool) -> void:
	if toggle:
		is_flicker = true
	else:
		is_flicker = false
		modulate.v = 1.0
