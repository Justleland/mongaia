extends HBoxContainer

signal tab_action(action)

var unit: Unit = null
var switch_targets = []

onready var unit_info = $MenuUnitStatInfo
onready var name_level = $VBox/NameLevel
onready var species = $VBox/Species
onready var skills = [
	$VBox/Skill1,
	$VBox/Skill2,
	$VBox/Skill3,
	$VBox/Skill4
]
onready var buttons = [
	$Buttons/SwitchButton,
	$Buttons/SwitchButton2,
	$Buttons/SwitchButton3
]

func _ready():
	visible = false

func activate(_unit: Unit, _switch_targets: Array) -> void:
	visible = true
	unit = _unit
	switch_targets = _switch_targets
	unit_info.visible = false
	species.visible = false
	name_level.visible = false
	for skill in skills:
		skill.visible = false
	
	for i in range(buttons.size()):
		if switch_targets.size() > i:
			buttons[i].set_unit(switch_targets[i])
		else:
			buttons[i].set_unit(null)


func deactivate() -> void:
	unit = null
	switch_targets = []
	for button in buttons:
		button.set_unit(null)
	visible = false


func is_active() -> bool:
	return unit != null


func _on_button_mouse_entered(pos):
	var switch = buttons[pos].unit
	if switch != null:
		unit_info.set_info(switch)
		unit_info.visible = true
		name_level.text = switch.monster.monster_name + " Lvl: " + String(switch.level)
		if switch.monster.monster_name != switch.monster.monsterData.species:
			species.text = switch.monster.monsterData.species
			species.modulate.a = 1.0
		else:
			species.modulate.a = 0.0
		species.visible = true
		name_level.visible = true
		for skill in skills:
			skill.visible = true


func _on_button_pressed(pos):
	var switch = buttons[pos].unit
	if switch != null:
		emit_signal("tab_action", {
			"type": "SWITCH",
			"side": "PLAYER",
			"acting": unit,
			"switch_to": switch
		})
		pass

