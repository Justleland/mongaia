extends HBoxContainer

signal action_selected(dict)

var unit: Unit = null
#var state = INACTIVE
#var skill_charge: SkillChargeCounter = null
var skill: Skill = null
var target = null
var targets: Array = []
var team = []

onready var buttons = [
	$GridContainer/BattleTabButton,
	$GridContainer/BattleTabButton2,
	$GridContainer/BattleTabButton3,
	$GridContainer/BattleTabButton4,
	$GridContainer/BattleTabButton5,
	$GridContainer/BattleTabButton6
]

onready var info_label = $Margin/Margin/VBox/InfoLabel
onready var stance_tab = $Margin/Margin/VBox/InfoMargin/StanceInfoTab
onready var skill_tab = $Margin/Margin/HBoxContainer/LeftMargin/SkillTab
onready var switch_tab = $Margin/Margin/HBoxContainer/LeftMargin/SwitchTab
onready var active_unit_ui = $Margin/Margin/HBoxContainer/RightMargin/MenuUnitUI
onready var hover_unit_right = $Margin/Margin/HBoxContainer/RightMargin/HoverUnitUI
onready var hover_unit_left = $Margin/Margin/HBoxContainer/LeftMargin/HoverUnitUI

func _ready():
	Utils.safe_connect(stance_tab, "tab_close", self, "_on_tab_close")
	Utils.safe_connect(stance_tab, "tab_action", self, "_on_tab_action")
	Utils.safe_connect(switch_tab, "tab_action", self, "_on_tab_action")
	Utils.safe_connect(active_unit_ui, "tab_action", self, "_on_tab_action")
	Utils.safe_connect(active_unit_ui, "tab_skill", self, "_on_button_tab_skill")
	Utils.safe_connect(active_unit_ui, "tab_switch", self, "_on_button_tab_switch")
	for button in buttons:
		button.disabled = false
		_connect_button(button)
	
	hover_unit_right.visible = false
	hover_unit_left.visible = false


func activate(unit: Unit, targets: Array, team: Array) -> bool:
	if unit == null:
		return false
	
	self.unit = unit
	self.targets = targets
	self.team = team
	_set_stance_button_text(unit.stance)
	
	active_unit_ui.set_unit(unit)
	return true


func select_unit(_unit: Unit):
	if skill_tab.is_active() and _unit != null:
		var _targets = skill_tab.get_targets(_unit)
		if !_targets.empty():
			_submit_action({
				"type": "SKILL",
				"skill": skill_tab.skill,
				"acting": unit,
				"targets": _targets
			})


func display_hover_unit(_unit):
	if _unit is Unit:
		if _unit.player_owned:
			hover_unit_right.set_unit(_unit)
			hover_unit_right.visible = true
			active_unit_ui.visible = false
		else:
			hover_unit_left.set_unit(_unit)
			hover_unit_left.visible = true
			skill_tab.visible = false
			switch_tab.visible = false


func hide_hover_unit():
	hover_unit_right.visible = false
	active_unit_ui.visible = true
	hover_unit_left.visible = false
	
	if skill_tab.is_active():
		skill_tab.visible = true
	elif switch_tab.is_active():
		switch_tab.visible = true


func _connect_button(button) -> void:
	var method = "_on_dead_button"
	match button.value:
		BattleTabButton.SKILL:
			method = "_on_button_tab_skill"
		BattleTabButton.STANCE:
			method = "_on_button_tab_stance"
		BattleTabButton.ITEM:
			method = "_on_button_tab_item"
		BattleTabButton.SWITCH:
			method = "_on_button_tab_switch"
		BattleTabButton.PASS:
			method = "_on_button_tab_pass"
		BattleTabButton.RUN:
			method = "_on_button_tab_run"
	
	Utils.safe_connect(button, "button_up", self, method)


func _set_stance_button_text(text: String) -> void:
	var formatted_text = buttons[1].text
	match text:
		BattleUtils.ST_NEU:
			formatted_text = "NEU"
		BattleUtils.ST_STR:
			formatted_text = "STR"
		BattleUtils.ST_DEX:
			formatted_text = "DEX"
		BattleUtils.ST_INT:
			formatted_text = "INT"
	buttons[1].text = formatted_text


func _on_button_tab_skill(_skill):
	_deactivate_tabs()
	var targetable = _skill.filter_targets(unit, targets)
	skill_tab.activate(_skill, targetable)


func _on_button_tab_stance():
	_deactivate_tabs()
	info_label.text = "STANCE"
	stance_tab.activate(unit)


func _on_button_tab_item():
	pass


func _on_button_tab_switch():
	_deactivate_tabs()
	info_label.text = "SWITCH"
	var valid_switch_targets = []
	for t in team:
		if !targets.has(t) and t != unit:
			valid_switch_targets.append(t)
	switch_tab.activate(unit, valid_switch_targets)


func _on_button_tab_pass():
	_submit_action({
		"type": "PASS",
		"acting": unit
	})


func _on_button_tab_run():
	pass


func _on_dead_button():
	pass


func _submit_action(action: Dictionary) -> void:
	emit_signal("action_selected", action)
	_close()


func _on_tab_action(action) -> void:
	if action is Dictionary:
		_submit_action(action)


func _close() -> void:
#	state = INACTIVE
	unit = null
	targets = []
	team = []
	_deactivate_tabs()


func _on_tab_close() -> void:
	_deactivate_tabs()
	

func _deactivate_tabs() -> void:
	stance_tab.deactivate()
	skill_tab.deactivate()
	switch_tab.deactivate()
