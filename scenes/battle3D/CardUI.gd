extends MarginContainer

func set_name(name: String) -> void:
	$Margin/VBox/Margin/Name.text = name


func set_agility(agi: int) -> void:
	$Margin/VBox/Agility.text = String(agi)
