extends Reference
class_name ActionAnimator

signal animation_finished
signal no_animation
signal next
signal animation_switch(side, a_unit, b_unit)
signal display_title(text)

const NO_ANIMATION = "NO_ANIMATION"

var animations = []
var step = []

func animate() -> void:
	var animation = null
	if !step.empty():
		animation = step.pop_front()
		
	if animation is Dictionary:
		_animate_dictionary(animation)
	elif animation == null or (animation is String and animation == NO_ANIMATION):
		emit_signal("no_animation")


func _animate_dictionary(animation: Dictionary):
	match animation["type"]:
		"UNIT_DISPLAY":
			_animate_unit_display(animation["display"], animation["action"])
		"ANIMATION_SCENE":
			_animate_scene(animation["scene"], animation)
		"DAMAGE":
			for hit in animation["hits"]:
				hit["target"].damage_data(hit)
			emit_signal("animation_finished")
		"STATUS":
			for target in animation["targets"]:
				target.inflict_status(animation["status"], animation["count"])
			emit_signal("animation_finished")
		"DAMAGE_NO_ANIMATION":
			for data in animation["info"]:
				data["target"].damage_data(data)
			emit_signal("animation_finished")
		"UNIT_DISPLAY_EXIT":
			Utils.safe_connect(animation["display"], "animation_finished", self, "_on_animation_finished")
			animation["display"].animate_exit()
		"UNIT_DISPLAY_SWITCH":
			emit_signal("animation_switch", animation["side"], animation["a_unit"], animation["b_unit"])
		"UNIT_DISPLAY_ENTER":
			animation["display"].animate_enter()
		"UNIT_DISPLAY_FINISHED":
			Utils.safe_disconnect(animation["display"], "animation_finished", self, "_on_animation_finished")
			emit_signal("animation_finished")
		_:
			emit_signal("animation_finished")


func _animate_unit_display(display, action) -> void:
	Utils.safe_connect(display, "animation_finished", self, "_on_animation_finished")
	animations.append(display)
	display.animate(action)


func _animate_scene(packed_scene, data):
	if data.has("target_displays"):
		for target_display in data["target_displays"]:
			var dup_data = data.duplicate()
			dup_data["target_display"] = target_display
			var scene = packed_scene.instance()
			Utils.safe_connect(scene, "animation_finished", self, "_on_animation_finished")
			animations.append(scene)
			scene.setup(dup_data)
	else:
		var scene = packed_scene.instance()
		Utils.safe_connect(scene, "animation_finished", self, "_on_animation_finished")
		animations.append(scene)
		scene.setup(data)


func setup(action: Dictionary, players, enemies) -> void:
	match action["type"]:
		"SKILL":
			if !action.has("animation"):
				_setup_skill(action, players, enemies)
			else:
				step.append(NO_ANIMATION)
		"SWITCH":
			_setup_swtich(action, players, enemies)


func _setup_skill(action: Dictionary, players, enemies) -> void:
	for result in action["results"]:
		match result["type"]:
			"UNIT_DISPLAY":
				result["display"] = _get_display(result["acting"], players, enemies)
			"ANIMATION_SCENE":
				result["acting_display"] = _get_display(result["acting"], players, enemies)
				if result["targets"].size() == 1:
					result["target_display"] = _get_display(result["targets"][0], players, enemies)
				else:
					var target_displays = []
					for target in result["targets"]:
						target_displays.append(_get_display(target, players, enemies))
					result["target_displays"] = target_displays
		step.append(result)
	
	_display_title(action["name"])

#func _setup_skill(action: Dictionary, players, enemies) -> void:
#	if action["animation"] is BattleAnimation:
#		var instructions = action["animation"].get_instructions(action, players, enemies)
#		step += instructions
#	else:
#		step.append(NO_ANIMATION)

# Still needs code
func _format_skill(action: Dictionary, players, enemies) -> Array:
	return []


func _setup_skill_no_animation(action: Dictionary) -> void:
	
	var animations = []
	for _i in range(action["hit_count"]):
		animations.append([])
		
	for result in action["results"]:
		for i in range(result["hits"].size()):
			var hit = result["hits"][i]
			hit["target"] = result["target"]
			hit["type"] = result["type"]
			hit["acting"] = result["acting"]
			animations[i].append(hit)
	
	for animation in animations:
		step.append({
			"type": "DAMAGE_NO_ANIMATION",
			"info": animation
		})
	
	if !animations.empty():
		action["acting"].add_charge(action["charge"])
		


func _get_display(unit, players, enemies):
	var pos = players.find(unit)
	if pos > -1:
		return players.get_display(pos).get_parent()
	pos = enemies.find(unit)
	if pos > -1:
		return enemies.get_display(pos).get_parent()
	return null


func _setup_swtich(action: Dictionary, players, enemies) -> void:
	# Can refactor if to get the correct display and reuse most of this code
	if action["side"] == "PLAYER":
		var display = players.get_display(players.find(action["acting"])).get_parent()
		step.append({
			"type": "UNIT_DISPLAY_EXIT",
			"display": display
		})
		step.append({
			"type": "UNIT_DISPLAY_SWITCH",
			"side": players,
			"a_unit": action["acting"],
			"b_unit": action["switch_to"]
		})
		step.append({
			"type": "UNIT_DISPLAY_ENTER",
			"display": display
		})
		step.append({
			"type": "UNIT_DISPLAY_FINISHED",
			"display": display
		})
	elif action["side"] == "ENEMY":
		step.append(NO_ANIMATION)
	
	_display_title("Switch")


func _on_animation_finished():
	var animating = []
	for animation in animations:
		if animation is UnitDisplay:
			Utils.safe_disconnect(animation, "animation_finished", self, "_on_animation_finished")
		if animation is BattleAnimation:
			if !animation.finished:
				animating.append(animation)
			else: 
				Utils.safe_disconnect(animation, "animation_finished", self, "_on_animation_finished")
	
	animations = animating
	
	if animations.empty():
		emit_signal("animation_finished")


func _display_title(text: String):
	emit_signal("display_title", text)
