extends BattleAnimation

func _process(delta):
	if animation == "start" and frame == frames.get_frame_count("start") - 1:
		play("finish")
		$AudioStreamPlayer.play()
		finished = true
		emit_signal("animation_finished")
	elif animation == "finish" and frame == frames.get_frame_count("finish") - 1:
		complete_skill()


func setup(skill_data: Dictionary) -> void:
	.setup(skill_data)
	
	if skill_data.has("target_display"):
		skill_data["target_display"].add_child(self)


func complete_skill():
	get_parent().remove_child(self)
