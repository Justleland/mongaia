extends Reference
class_name SkillChargeCounter

var skill setget ,get_skill
var charge: int = -1 setget ,get_charge
var was_stanced: bool = false

func _init(skill):
	self.skill = skill


func is_ready() -> bool:
	if was_stanced:
		return charge > skill.charge
	return charge >= skill.charge


func increment() -> void:
	if charge < skill.charge or was_stanced and charge <= skill.charge:
		charge += 1


func reset(was_stanced: bool = false) -> void:
	self.was_stanced = was_stanced
	charge = -1


func get_skill():
	return skill


func get_charge():
	return charge
