extends Node

const WEAK = "WEAK"
const RESIST = "RESIST"
const IMMUNE = "IMMUNE"

enum Stage {
	NEG_6,
	NEG_5, 
	NEG_4,
	NEG_3,
	NEG_2,
	NEG_1,
	ZERO,
	POS_1,
	POS_2,
	POS_3,
	POS_4,
	POS_5,
	POS_6 
}

enum {
	INACTIVE,
	WAIT,
	PREP,
	CAST,
	EXECUTE
}

const ST_NEU = "NEUTRAL"
const ST_STR = "STRENGTH"
const ST_DEX = "DEXTERITY"
const ST_INT = "INTELLECT"

var stage_value = [
	0.25,
	0.28,
	0.33,
	0.4,
	0.5,
	0.66,
	1.0,
	1.5,
	2.0,
	2.5,
	3.0,
	3.5,
	4.0
]

var stage = Stage.ZERO

var encounter_monsters: Array = []

var caught_monster: Monster = null

func build_monster(data: MonsterData, level: int) -> Monster:
	if data is MonsterData and level != null:
		var monster = load("res://resources/monsters/Monster.gd").new()
		monster.level = level
		return monster.setup(data)
	
	return null

func add_encounter_monster(monsters: Array) -> void:
	for monster in monsters:
		if monster is Monster and encounter_monsters.size() < 5:
			encounter_monsters.append(monster)


func build_encounter(players_team, enemy_team, npc_battle:= false) -> Dictionary:
	return {
		"players_team": players_team,
		"enemy_team": enemy_team,
		"npc_battle": npc_battle
	}

func clear_encounter_monster() -> void:
	encounter_monsters.clear()


func load_monster_spriteframes(path: String) -> Resource:
	var frames = load(path)
	if frames is SpriteFrames:
		return frames
	return load("res://resources/spriteframes/monsters/default.tres")


func modulate_stage(current: float, change: int):
	var stage_num = _num_to_stage(current)
	var changed_stage_num = max(min(stage_num + change, 12), 0)
	return _stage_to_num(changed_stage_num)


func _stage_to_num(value: int):
	var num = stage_value[value]
	if num == null:
		return stage_value[0]
	return num


func _num_to_stage(num: float):
	match num:
		1.5:
			return Stage.POS_1
		2.0:
			return Stage.POS_2
		2.5:
			return Stage.POS_3
		3.0:
			return Stage.POS_4
		3.5:
			return Stage.POS_5
		4.0:
			return Stage.POS_6
		0.66:
			return Stage.NEG_1
		0.5:
			return Stage.NEG_2
		0.4:
			return Stage.NEG_3
		0.33:
			return Stage.NEG_4
		0.28:
			return Stage.NEG_5
		0.25:
			return Stage.NEG_6
		_:
			return Stage.ZERO


func load_counter():
	var counter = load("res://resources/skills/Counter.tscn")
	return counter.instance()
