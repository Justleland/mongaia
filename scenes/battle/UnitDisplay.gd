extends Node2D
class_name UnitDisplay

enum {
	NONE
	RETURN
	ATTACK
	ENTER
	EXIT
}

var unit: Unit

var animation_state = NONE
var start_pos 
var move_direction = 100.0
var attack_direction = -36.0

signal animation_finished

func _ready():
	$Sprite.texture = null
	start_pos = position


func _physics_process(delta):
	if animation_state == RETURN:
		_animate_return()
	elif animation_state == ATTACK:
		_animate_attack()
	elif animation_state == ENTER or animation_state == EXIT:
		_animate_switch()


func _animate_return():
	position = position.move_toward(start_pos, 1.0)
	if position.distance_to(start_pos) < 1.0:
		position = start_pos


func _animate_attack():
	position = position.move_toward(Vector2(start_pos.x + attack_direction, start_pos.y), 4.0)
	if position.distance_to(Vector2(start_pos.x + attack_direction, start_pos.y)) < 2.0:
		emit_signal("animation_finished")
		animation_state = RETURN


func _animate_switch():
	if animation_state == EXIT:
		position = position.move_toward(Vector2(start_pos.x + move_direction, start_pos.y), 4.0)
		modulate.a -= 0.04
	elif animation_state == ENTER:
		position = position.move_toward(start_pos, 4.0)
		modulate.a += 0.04
	
	if animation_state != NONE:
		if modulate.a <= 0.0:
			modulate.a = 0.0
			animation_state = NONE
			emit_signal("animation_finished")
		elif modulate.a >= 1.0:
			modulate.a = 1.0
			position = start_pos
			animation_state = NONE
			emit_signal("animation_finished")

func set_unit(unit: Unit) -> void:
	if unit != null:
		$Sprite.texture = unit.monster.texture


func animate(action) -> void:
	match action:
		"ATTACK":
			animation_state = ATTACK
		"ENTER":
			animation_state = ENTER
		"EXIT":
			animation_state = EXIT
		_:
			emit_signal("animation_finished")


func animate_exit() -> void:
	animation_state = EXIT


func animate_enter() -> void:
	animation_state = ENTER
