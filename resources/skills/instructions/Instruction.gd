extends Resource
class_name Instruction

export var type = ""
export var action = ""

func instruct(data: Dictionary) -> Dictionary:
	return {
		"type": type,
		"action": action,
		"acting": data["acting"]
	}
