extends VBoxContainer

onready var name_label = $HSplit/Name
onready var current_label = $HSplit/Current
onready var num_label = $HSplit2/Panel/Margin/VSplit/Num
onready var info_text = $HSplit2/InfoText

func set_info_for_stance(stance: String, stat, current: bool) -> void:
	name_label.text = stance
	num_label.text = String(stat)
	current_label.visible = current
	
	match stance:
		BattleUtils.ST_NEU:
			info_text.text = "average atk between strength, dexterity, and intellect"
		BattleUtils.ST_STR:
			info_text.text = "+10 skill base power"
		BattleUtils.ST_DEX:
			info_text.text = "+8% crit chance\n+15% accuracy"
		BattleUtils.ST_INT:
			info_text.text = "+15% inflict chance\n+20% affliction damage\n+20% healing"
	
 
