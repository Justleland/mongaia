extends RootScene

enum {
	BATTLE_AI,
	INTERPRET,
	STATUS,
	ANIMATE,
	SKIP_TURN,
	FAINT
}

onready var players = $Players
onready var enemies = $Enemies
onready var turn_order_ui = $TurnOrderMargin/TurnOrderUI
onready var menu_ui = $MenuMargin/MenuUI
onready var title_ui = $ActionTitleUI

var players_team = []
var enemy_team = []

var turn_order = TurnOrder.new()
var battle_ai = BattleAI.new()
var action_interpreter = ActionInterpreter.new()
var action_animator = ActionAnimator.new()

const buffer = 80
var buffer_count = 0
var buffer_actions = []

var npc_battle = false

func init(parameters: Dictionary):
	print(parameters)
	if parameters.has_all(["players_team", "enemy_team"]):
		players_team = parameters["players_team"]
		enemy_team = parameters["enemy_team"]
	
	if parameters.has("npc_battle"):
		npc_battle = parameters["npc_battle"]

func _ready():
	print("ready")
	Utils.safe_connect(battle_ai, "processed", self, "_on_battle_ai_processed")
	Utils.safe_connect(battle_ai, "action_processed", self, "_on_battle_ai_action_processed")
	Utils.safe_connect(action_interpreter, "action_switch", self, "_on_action_switch")
	Utils.safe_connect(action_interpreter, "action_animate", self, "_on_action_animate")
	Utils.safe_connect(action_interpreter, "no_animation", self, "_on_no_animation")
	Utils.safe_connect(action_interpreter, "display_title", self, "_on_display_title")
	Utils.safe_connect(action_animator, "no_animation", self, "_on_no_animation")
	Utils.safe_connect(action_animator, "animation_finished", self, "_on_animation_finished")
	Utils.safe_connect(action_animator, "animation_switch", self, "_on_animation_switch")
	Utils.safe_connect(action_animator, "display_title", self, "_on_display_title")
	
	if players_team is Array and !players_team.empty():
		var team = []
		for p_monster in players_team:
			if p_monster is Monster:
				var unit = Unit.new().init(p_monster)
				unit.player_owned = true
				team.append(unit)
		players_team = team
	
	if enemy_team is Array and !enemy_team.empty():
		var team = []
		for e_monster in enemy_team:
			if e_monster is Monster:
				var unit = Unit.new().init(e_monster)
				team.append(unit)
		enemy_team = team
	
	var iterate = 0
	var pos = 0
	while pos < 3 and iterate < players_team.size():
		if players_team[iterate].hp > 0:
			players.assign(pos, players_team[iterate])
			pos += 1
		iterate += 1
	
	if npc_battle:
		pos = 0
		while pos < 3 and pos < enemy_team.size():
			enemies.assign(pos, enemy_team[pos])
			pos += 1
	else:
		for i in range(min(enemy_team.size(), 5)):
			enemies.assign(i, enemy_team[i])
	
	for unit in players.units:
		turn_order.add(unit)
	
	for unit in enemies.units:
		turn_order.add(unit)
	
	if !players_team.empty() and !enemy_team.empty():
		_iterate_turn()
	else:
		Utils.go_to_previous()


func _process(delta):
	if buffer_count < buffer:
		buffer_count += 1
	elif !buffer_actions.empty():
		_perform_action()


func _perform_action():
	match buffer_actions.pop_front():
		BATTLE_AI:
			battle_ai.process()
		INTERPRET:
			action_interpreter.interpret()
		ANIMATE:
			action_animator.animate()
		FAINT:
			_faint()
		SKIP_TURN:
			_iterate_turn()


func _iterate_turn() -> void:
	if _is_player_defeated():
		print("PLAYER DEFEATED. YOU LOSE\n")
		Utils.go_to_previous()
	elif _is_enemy_defeated():
		print("ENEMY DEFEATED. YOU WIN\n")
		Utils.go_to_previous()
	_reset_buffer()
	
	var active_unit = turn_order.next()
	turn_order.print_order()
	turn_order_ui.set_turn_order(turn_order)
	
	var status_stacks = []
	for stack in active_unit.statuses:
		stack.decrement_action(active_unit)
		if stack.count > 0:
			status_stacks.append(stack)
	active_unit.statuses = status_stacks
	
	if active_unit.player_controlled:
		menu_ui.activate(active_unit, players.units + enemies.units, players_team)
	else:
		battle_ai.activate(active_unit)
		buffer_actions.append(BATTLE_AI)


func _reset_buffer():
	buffer_count = 0


func _faint():
	for unit in players.defeated_units:
		turn_order.remove(unit)
		players.unassign(unit)
		# Add Menu Input for switch if player has units left
	
	for unit in enemies.defeated_units:
		turn_order.remove(unit)
		enemies.unassign(unit)
	
	_iterate_turn()


func _is_player_defeated() -> bool:
	for unit in players_team:
		if unit.hp > 0:
			return false
	return true


func _is_enemy_defeated() -> bool:
	for unit in enemy_team:
		if unit.hp > 0:
			return false
	return true


func _on_battle_ai_action_processed(action):
	action_interpreter.append(action)
	buffer_actions.append(INTERPRET)


func _on_action_animate(action):
	action_animator.setup(action, players, enemies)
	buffer_actions.append(ANIMATE)


func _on_action_switch(side, a_unit, b_unit):
	a_unit.charge = 0
	turn_order.remove(a_unit)
	turn_order.add(b_unit)
	if side == "PLAYER":
		players.switch(a_unit, b_unit)
	elif side == "ENEMY":
		pass


func _on_animation_switch(side, a_unit, b_unit):
	a_unit.charge = 0
	a_unit.stance = BattleUtils.ST_NEU
	turn_order.remove(a_unit)
	turn_order.add(b_unit)
	side.switch(a_unit, b_unit)
	buffer_actions.append(ANIMATE)



func _on_animation_finished():
	buffer_actions.append(ANIMATE)


func _on_no_animation():
	if !players.defeated_units.empty() or !enemies.defeated_units.empty():
		buffer_actions.append(FAINT)
	else:
		_iterate_turn()


func _on_MenuUI_closed():
	pass


func _on_battle_ai_processed():
	pass


func _on_MenuUI_action_selected(action):
	action_interpreter.append(action)
	buffer_actions.append(INTERPRET)


func _on_display_title(title):
	if title is String:
		title_ui.display_text(title)
