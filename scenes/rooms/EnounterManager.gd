extends Node

var encounters: Array = []
export(Array, Resource) var monsters = []
export(int) var average_lvl = 1

func _ready():
	encounters = get_children()


func get_random_encounter() -> Array:
	if monsters.empty():
		var rate = randf()
		var chance = 0.0
		if !encounters.empty():
			for encounter in encounters:
				chance += encounter.weight
				if chance >= rate:
					return _build_encounter(encounter)
			
			return _build_encounter(encounters[1])
	
	var count = randi() % 5 + 1
	var lvl_min = int(max(1.0, average_lvl - count))
	var lvl_max = int(min(average_lvl + 6 - count,100.0))
	var encounter = []
	for _i in count:
		encounter.append(_build_encounter_monster(monsters[randi() % monsters.size()], lvl_min, lvl_max))
	
	return encounter

func _build_encounter(encounter) -> Array:
	var monsters = []
	if encounter.monster_1 is MonsterData:
		var monster = _build_encounter_monster(
				encounter.monster_1, 
				encounter.monster_1_lvl_min,
				encounter.monster_1_lvl_max)
		
		if monster != null:
			monsters.append(monster)
	
	if encounter.monster_2 is MonsterData:
		var monster = _build_encounter_monster(
				encounter.monster_2, 
				encounter.monster_2_lvl_min,
				encounter.monster_2_lvl_max)
		
		if monster != null:
			monsters.append(monster)
	
	if encounter.monster_3 is MonsterData:
		var monster = _build_encounter_monster(
				encounter.monster_3, 
				encounter.monster_3_lvl_min,
				encounter.monster_3_lvl_max)
		
		if monster != null:
			monsters.append(monster)
	
	if encounter.monster_1 is MonsterData:
		var monster = _build_encounter_monster(
				encounter.monster_4, 
				encounter.monster_4_lvl_min,
				encounter.monster_4_lvl_max)
		
		if monster != null:
			monsters.append(monster)
	
	if encounter.monster_1 is MonsterData:
		var monster = _build_encounter_monster(
				encounter.monster_5, 
				encounter.monster_5_lvl_min,
				encounter.monster_5_lvl_max)
		
		if monster != null:
			monsters.append(monster)
	
	return monsters


func _build_encounter_monster(data, min_lvl, max_lvl) -> Monster:
	if data is MonsterData:
		return BattleUtils.build_monster(data, min_lvl + randi() % (max_lvl - min_lvl + 1))
	
	return null

