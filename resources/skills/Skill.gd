extends Resource
class_name Skill

enum Conditions {
	NONE
	PARALYZED,
	POISONED,
	FROZEN,
	BURNED,
	STUNNED,
	CONFUSED
}

export(String) var skill_name
export(Resource) var element
export(Resource) var target
export(String, MULTILINE) var description

export(int) var charge = 0
export(int) var sp_cost = 5
export(int) var acc = 100 setget ,_get_acc

export(Array, Resource) var setup_instructions
export(Array, Resource) var hit_instructions
export(Array, Resource) var resolve_instructions

var skill_result_template = load("res://resources/skills/SkillResult.gd")
var damage_template = load("res://resources/skills/Damage.gd")
var heal_template = load("res://resources/skills/Heal.gd")

func result(acting: Unit, target: Array) -> Array:
	return []


func damage(attacker: BattleUnit, defender: BattleUnit) -> int:
	return 0


func target_stat_change() -> bool:
	return false


func self_stat_change() -> bool:
	return false


func target_status_condition() -> bool:
	return false


func self_status_condition() -> bool:
	return false


func calculate_effectiveness(family) -> float:
	match family:
		MonsterData.Family.SLIME:
			if element == Element.FIRE or element == Element.ICE or element == Element.ELEC or element == Element.WIND:
				return 2.0
			if element == Element.NEUTRAL or element == Element.MARTIAL or element == Element.METAL:
				return 0.5
			if element == Element.LIGHT or element == Element.SHADOW:
				return 0.0
		MonsterData.Family.FLYING:
			if element == Element.ICE or element == Element.ELEC:
				return 2.0
			if element == Element.WIND or element == Element.NATURE or element == Element.MARTIAL:
				return 0.5
		MonsterData.Family.AQUATIC:
			if element == Element.ELEC or element == Element.NATURE:
				return 2.0
			if element == Element.FIRE or element == Element.METAL or element == Element.WATER:
				return 0.5
		MonsterData.Family.INSECTOID:
			if element == Element.FIRE or element == Element.WIND or element == Element.POISON:
				return 2.0
			if element == Element.EARTH or element == Element.NATURE:
				return 0.5
		MonsterData.Family.BEAST:
			if element == Element.FIRE or element == Element.METAL:
				return 2.0
			if element == Element.EARTH:
				return 0.5
		MonsterData.Family.PLANT:
			if element == Element.FIRE or element == Element.POISON or element == Element.ICE:
				return 2.0
			if element == Element.NATURE or element == Element.LIGHT or element == Element.WATER:
				return 0.5
		MonsterData.Family.DRAGON:
			if element == Element.EARTH or element == Element.METAL or element == Element.LIGHT or element == Element.SHADOW:
				return 2.0
			if element == Element.FIRE or element == Element.ICE or element == Element.ELEC or element == Element.WIND:
				return 0.5
		MonsterData.Family.HUMANOID:
			if element == Element.PSI:
				return 2.0
			if element == Element.MARTIAL:
				return 0.5
		MonsterData.Family.UNDEAD:
			if element == Element.FIRE or element == Element.NATURE or element == Element.LIGHT:
				return 2.0
			if element == Element.ELEC or element == Element.EARTH or element == Element.POISON or element == Element.PSI or element == Element.SHADOW:
				return 0.5
		MonsterData.Family.MATERIAL:
			if element == Element.EARTH or element == Element.NATURE or element == Element.MARTIAL or element == Element.WATER:
				return 2.0
			if element == Element.NEUTRAL or element == Element.FIRE or element == Element.ICE or element == Element.METAL or element == Element.PSI:
				return 0.5
			if element == Element.POISON:
				return 0.0
		MonsterData.Family.ETHEREAL:
			if element == Element.PSI or element == Element.METAL:
				return 2.0
			if element == Element.EARTH:
				return 0.5
			if element == Element.NEUTRAL or element == Element.MARTIAL:
				return 0.0
		MonsterData.Family.DEMONIC:
			if element == Element.ICE or element == Element.LIGHT or element == Element.MARTIAL:
				return 2.0
			if element == Element.POISON or element == Element.SHADOW:
				return 0.5
			if element == Element.FIRE:
				return 0.0
		MonsterData.Family.ANGELIC:
			if element == Element.EARTH or element == Element.SHADOW:
				return 2.0
			if element == Element.FIRE or element == Element.LIGHT:
				return 0.5
	return 1.0


func _move_hit(dodge_chance: float, e_acc: float) -> bool:
	return randf() < ((e_acc - dodge_chance) / 100.0)

func _get_acc() -> float:
	return acc / 100


func get_acc_num() -> int:
	return acc

#func _get_penetration() -> float:
#	return penetration / 100


#func _get_crit() -> float:
#	return crit / 100


func _percentage_mod(skill_stat: int, monster_mod: int, percentage:float = 0.2) -> float:
	return (float(skill_stat) * percentage * (float(monster_mod) / 100.0))


func _flat_mod(mod: int, flat:float = 5.0) -> float:
	return flat * float(mod) / 100

func _level_mod(skill_stat: int, level: int, stance: int, percentage: float, p_range: float = 0.0) -> int:
	print(skill_stat)
	var f_level = float(level)
	var f_stance = float(stance)
	return int(skill_stat * (1.0 + ((percentage + (p_range * f_level / 100.0)) * f_stance / f_level)))


func element_type_text() -> String:
	if element is Element:
		return element.text
	return "---"


func target_type_text() -> String:
	if target is TargetType:
		return target.display_text()
	return "---"


func filter_targets(acting: Unit, targets: Array) -> Array:
	if acting != null and target is TargetType:
		var targetable = []
		for _target in targets:
			if _target is Unit and target.is_valid_target(acting, _target):
				targetable.append(_target)
		return targetable
	return []
