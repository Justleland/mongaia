extends Resource
class_name MonsterData

enum EXP_Rate {
	FAST
	MEDIUM
	MEDIUM_SLOW
	SLOW
}

export(String) var species: String = ""

export(Resource) var family1
export(Resource) var family2

export (Texture) var texture
export (String) var spriteframes_path = "res://resources/spriteframes/monsters/default.tres"
export(EXP_Rate) var exp_rate = EXP_Rate.MEDIUM

export(int) var exp_value = 50
export(int) var base_hp = 30
export(int) var base_def = 30
export(int) var base_agi = 30
export(int) var base_str = 30
export(int) var base_dex = 30
export(int) var base_int = 30

export(Array, Resource) var evolutions: Array
export(String, MULTILINE) var skill_config: String = '{"0": ["Bash"]}'
export(Array, String, MULTILINE) var evo_config: Array 
var skills: Array

func calculate_exp_from_level(level: int) -> int:
	if level <= 1:
		return 0
	
	var experience = 0
	match exp_rate:
		EXP_Rate.FAST:
			experience = int(floor((4.0 * pow(level, 3)) / 5.0))
		EXP_Rate.MEDIUM:
			experience = int(floor(pow(level, 3)))
		EXP_Rate.MEDIUM_SLOW:
			experience = int(floor(((6.0 * pow(level, 3)) / 5.0 ) - (15.0 * pow(level, 2)) + (100.0 * level) - 140.0))
		EXP_Rate.SLOW:
			experience = int(floor((5.0 * pow(level, 3)) / 4.0))
	
	return experience


func build_skills_at_level(level: int) -> Array:
	var available_skills = []
	var skill_dict = _create_skill_config_dict()
#	var builder = SkillBuilder.new()
#	print(skill_dict)
	for i in range(level, -1, -1):
		var skills = _get_skills_at_level(i, skill_dict)
		for skill in skills:
			if skill != null:
				available_skills.append(skill)
			if available_skills.size() >=4:
#				builder.free()
				return available_skills
#	builder.free()
	return available_skills

func get_skills_from_level(level: int, to: int = -1) -> Array:
	var available_skills = []
	var skill_dict = _create_skill_config_dict()
	var to_level = level + 1
	if to > level:
		to_level = to + 1
	for i in range(level, to_level):
		var skills = _get_skills_at_level(i, skill_dict)
		for skill in skills:
			if skill != null:
				available_skills.append(skill)
	return available_skills

func _get_skills_at_level(level: int, config = null) -> Array:
	if config == null:
		return []

	var skills = []
	var skill_names = config.get(String(level))
#	print(String(level))
#	print(skill_names)
	if skill_names is Array:
		for name in skill_names:
			skills.append(Utils.safe_load_skill(name))
#	print(skills)
	return skills


func _create_skill_config_dict() -> Dictionary:
	var result = JSON.parse(skill_config)
	if result.error == OK and result.result is Dictionary:
		return result.result
	print(result.error)
	print(result.error_line)
	print(result.error_string)
	print(result.result)
	return {}


func get_family_text() -> Array:
	return [family1.text, family2.text]


func get_family_multiplier_for(element: Element) -> float:
	if family1 is Family and family2 is Family:
		return family1.get_multiplier(element.text) * family2.get_multiplier(element.text)
	if family1 is Family:
		return family1.get_multiplier(element.text)
	if family2 is Family:
		return family2.get_multiplier(element.text)
	return 1.0


func set_exp_rate(rate: int):
	match rate:
		EXP_Rate.FAST:
			exp_rate = EXP_Rate.FAST
		EXP_Rate.MEDIUM:
			exp_rate = EXP_Rate.MEDIUM
		EXP_Rate.MEDIUM_SLOW:
			exp_rate = EXP_Rate.MEDIUM_SLOW
		EXP_Rate.SLOW:
			exp_rate = EXP_Rate.SLOW
