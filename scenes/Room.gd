extends RootScene2D

onready var player = $YSort/PartyYsort/Player
var player_start = null

func init(parameters: Dictionary):
	if parameters.has("player_pos"):
		player_start = parameters["player_pos"]

func get_parameters() -> Dictionary:
	var params = {
		"player_pos": player.position
	}
	return params


func _ready():
	if player_start is Vector2:
		player.position = player_start
	
	var top = $BoundTopLeft.position.y
	var left = $BoundTopLeft.position.x
	var right = $BoundBottomRight.position.x
	var bottom = $BoundBottomRight.position.y
	
	$YSort/PartyYsort/Player/Camera2D.limit_top = top
	$YSort/PartyYsort/Player/Camera2D.limit_bottom = bottom
	$YSort/PartyYsort/Player/Camera2D.limit_left = left
	$YSort/PartyYsort/Player/Camera2D.limit_right = right
