extends VBoxContainer

var unit: BattleUnit

onready var buttons = [
	$Button0,
	$Button1,
	$Button2,
	$Button3,
	$Button4,
	$Button5
]


func _ready():
	for button in buttons:
		button.visible = false
		button.disabled = true


func set_unit(value: BattleUnit) -> void:
	if unit == null and value is BattleUnit and value.monster is Monster:
		unit = value
		for i in range(unit.monster.skills_known.size()):
			_activate_button(buttons[i], unit.monster.skills_known[i].move_name)
		
		


func _activate_button(button: Button, move_name: String) -> void:
	if move_name is String:
		button.text = move_name
		button.visible = true
		button.disabled = false
	else:
		button.visible = false
		button.disabled = true


func _prep_unit_and_dispatch(move_pos: int) -> void:
	if unit != null:
		unit.state = BattleUnit.TARGET
		unit._set_sprite_color(1, 0, 1)
		var skill = unit.monster.skills_known[move_pos]
		unit.charge_time = skill.charge_time
		print("Move Cast Time: " + String(skill.charge_time))
		print("Set Cast Time: " + String(unit.charge_time))
		print("")
		unit.skill = skill
	
	unit = null
	for i in range(buttons.size()):
		buttons[i].visible = false
		buttons[i].disabled = true	


func _on_button_up(extra_arg_0):
	_prep_unit_and_dispatch(extra_arg_0) # Replace with function

