extends Reference
class_name Unit

signal no_hp
signal hp_def_changed
signal level_changed
signal stance_changed
signal targeted

var monster: Monster
var player_owned = false
var player_controlled = false

var family1 setget ,_get_family1
var family2 setget ,_get_family2

var level: int setget ,_get_level
var max_hp: int setget ,_get_max_hp
var hp: int setget ,_get_hp
var max_def: int setget ,_get_def
var def: int
var agi: int setget ,_get_agi
var strength: int setget ,_get_str
var dexterity: int setget ,_get_dex
var intellect: int setget ,_get_int
var neutral: int setget ,_get_neu
var atk: int setget ,_get_atk

var skills = []

var acc: int = 100
var dodge: int = 0
var charge: int = 0
var stance: String = BattleUtils.ST_NEU

var statuses = []

func init(monster: Monster) -> Unit:
	if monster != null:
		self.monster = monster
		def = monster.def
		for skill in monster.skills_known:
			skills.append(skill)
		return self
	return null


func damage(num: int) -> void:
	def -= num
	if def < 0:
		monster.subtract_hp(abs(def))
		def = 0
	emit_signal("hp_def_changed")


func true_damage(num: int) -> void:
	monster.subtract_hp(num)
	emit_signal("hp_def_changed")


func accept_skill_data(data: Dictionary) -> void:
	match data["type"]:
		"DAMAGE":
			damage_data(data)
		"HEAL":
			pass
		"STATUS":
			pass


func damage_data(data: Dictionary) -> void:
	if !data["is_dodge"] and data["is_hit"]:
		var multiplier = data["eff_multiplier"] * data["stab_multiplier"]
		if data["is_crit"]:
			multiplier *= 2
		# Maybe true damage should be limited to the max pentration after
		# the multiplier
		true_damage(data["true_dmg"] * multiplier)
		damage(data["dmg"] * multiplier)
		
		if self.hp <= 0:
			emit_signal("no_hp")


func inflict_status(status, count) -> void:
	var has_status = has_status(status)
	if has_status and status.stackable:
		get_status_stack(status).count += count
	elif !has_status:
		statuses.append(StatusStack.new(status, count))


func get_status_stack(status) -> StatusStack:
	for stack in statuses:
		if stack is StatusStack and stack.status.type == status.type:
			return stack
	return null


func has_status(status) -> bool:
	for stack in statuses:
		if stack.status.type == status.type:
			return true
	return false


func pass_turn() -> void:
	pass


func charge() -> void:
#	for value in skills:
#		value.increment()
	pass


func add_charge(num: int) -> void:
	charge += num
	if charge < (_get_agi() * -1):
		charge = _get_agi() * -1


func is_empty() -> bool:
	return monster == null


func get_stance_value() -> int:
	match stance:
		BattleUtils.ST_STR:
			return _get_str()
		BattleUtils.ST_DEX:
			return _get_dex()
		BattleUtils.ST_INT:
			return _get_int()
		_:
			return int((_get_str() + _get_dex() + _get_int()) / 3.0)


func get_priority() -> int:
	var priority = 0
	for status in statuses:
		priority += status.get_priority()
	return priority


func _get_family1():
	if monster != null:
		return monster.monsterData.family1
	return null


func _get_family2():
	if monster != null:
		return monster.monsterData.family2
	return null



func _get_level() -> int:
	if monster != null:
		return monster.level
	return 1


func _get_max_hp() -> int:
	if monster != null:
		return monster.max_hp
	return 1


func _get_hp() -> int:
	if monster != null:
		return monster.hp
	return 0


func _get_def() -> int:
	if monster != null:
		return monster.def
	return 1


func _get_agi() -> int:
	if monster != null:
		return monster.agi
	return 1
	

func _get_str() -> int:
	if monster != null:
		return monster.strength
	return 1


func _get_dex() -> int:
	if monster != null:
		return monster.dexterity
	return 1


func _get_int() -> int:
	if monster != null:
		return monster.intellect
	return 1


func _get_neu() -> int:
	if monster != null:
		return (monster.strength + monster.dexterity + monster.intellect) / 3
	return 1


func _get_atk() -> int:
	match stance:
		BattleUtils.ST_STR:
			return _get_str()
		BattleUtils.ST_DEX:
			return _get_dex()
		BattleUtils.ST_INT:
			return _get_int()
	return _get_neu()

func set_stance(_stance) -> void:
	if _stance == BattleUtils.ST_NEU or _stance == BattleUtils.ST_STR or _stance == BattleUtils.ST_DEX or _stance == BattleUtils.ST_INT:
		stance = _stance
		emit_signal("stance_changed")
