extends Node

var time_per_encounter = 120
var last_encounter_time = 0

signal encounter

func _process(delta):
	if last_encounter_time < time_per_encounter:
		last_encounter_time += 1
		print(String(last_encounter_time))


func _on_Area2D_body_entered(body):
	if last_encounter_time > time_per_encounter and randf() > 0.5:
		print("ENCOUNTER!")
		emit_signal("encounter")
		last_encounter_time = 0

