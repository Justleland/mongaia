extends VBoxContainer

export(Texture) var burned_texture
export(Texture) var poisoned_texture
export(Texture) var paralyzed_texture
export(Texture) var frozen_texture
export(Texture) var confused_texture
export(Texture) var stunned_texture

var stack_count = 0
var unit: BattleUnit
var condition: String
var fade_timer = 0

func _ready():
	_invert_time_visibility()
	$Time/Step1.visible = false
	$Time/Step2.visible = false
	$Time/Step3.visible = false


func _process(delta):
	if stack_count <= 0:
		if modulate.a > 0.0 and fade_timer >= 45:
			modulate.a = max(modulate.a - 0.02, 0.0)
		elif fade_timer < 45:
			fade_timer += 1
		else:
			_erase()
	pass


func setup(unit: BattleUnit, condition: String, stacks: int):
	self.unit = unit
	self.condition = condition
	var texture = $Margin/TextureRect
	stack_count = stacks
	_update_stack_count()
	Utils.safe_connect(unit, "monster_KO", self, "_on_monster_KO")
	match condition:
		"burned":
			Utils.safe_connect(unit, "add_burned_stacks", self, "_add_stacks")
			Utils.safe_connect(unit, "remove_burned_stacks", self, "_remove_stacks")
			Utils.safe_connect(unit, "burned_timer", self, "_tick_time")
			_invert_time_visibility()
			texture.texture = burned_texture
		"frozen":
			Utils.safe_connect(unit, "add_frozen_stacks", self, "_add_stacks")
			Utils.safe_connect(unit, "remove_frozen_stacks", self, "_remove_stacks")
			Utils.safe_connect(unit, "frozen_timer", self, "_tick_time")
			_invert_time_visibility()
			texture.texture = frozen_texture
		"poisoned":
			Utils.safe_connect(unit, "add_poisoned_stacks", self, "_add_stacks")
			Utils.safe_connect(unit, "remove_poisoned_stacks", self, "_remove_stacks")
			Utils.safe_connect(unit, "poisoned_timer", self, "_tick_time")
			_invert_time_visibility()
			texture.texture = poisoned_texture
		"paralyzed":
			Utils.safe_connect(unit, "add_paralyzed_stacks", self, "_add_stacks")
			Utils.safe_connect(unit, "remove_paralyzed_stacks", self, "_remove_stacks")
			texture.texture = paralyzed_texture
		"confused":
			Utils.safe_connect(unit, "add_confused_stacks", self, "_add_stacks")
			Utils.safe_connect(unit, "remove_confused_stacks", self, "_remove_stacks")
			texture.texture = confused_texture
		"stunned":
			texture.texture = stunned_texture
			$Margin/StackMargin/StackVBox/StackCount.visible = false
	
	return self


func _update_stack_count() -> void:
	$Margin/StackMargin/StackVBox/StackCount.text = String(stack_count)


func _add_stacks(count: int) -> void:
	stack_count += count
	_update_stack_count()


func _remove_stacks(count: int) -> void:
	stack_count -= count
	_update_stack_count()


func _invert_time_visibility() -> void:
	$Time.visible = not $Time.visible


func _tick_time() -> void:
	if $Time/Step1.visible:
		$Time/Step1.visible = false
	elif $Time/Step2.visible:
		$Time/Step2.visible = false
	elif $Time/Step3.visible:
		$Time/Step3.visible = false
	else:
		$Time/Step1.visible = true
		$Time/Step2.visible = true
		$Time/Step3.visible = true


func _on_monster_KO() -> void:
	_erase()


func _erase() -> void:
	Utils.safe_disconnect(unit, "monster_KO", self, "_on_monster_KO")
	match condition:
		"burned":
			Utils.safe_disconnect(unit, "add_burned_stacks", self, "_add_stacks")
			Utils.safe_disconnect(unit, "remove_burned_stacks", self, "_remove_stacks")
			Utils.safe_disconnect(unit, "burned_timer", self, "_tick_time")
		"frozen":
			Utils.safe_disconnect(unit, "add_frozen_stacks", self, "_add_stacks")
			Utils.safe_disconnect(unit, "remove_frozen_stacks", self, "_remove_stacks")
			Utils.safe_disconnect(unit, "frozen_timer", self, "_tick_time")
		"poisoned":
			Utils.safe_disconnect(unit, "add_poisoned_stacks", self, "_add_stacks")
			Utils.safe_disconnect(unit, "remove_poisoned_stacks", self, "_remove_stacks")
			Utils.safe_disconnect(unit, "poisoned_timer", self, "_tick_time")
		"paralyzed":
			Utils.safe_disconnect(unit, "add_paralyzed_stacks", self, "_add_stacks")
			Utils.safe_disconnect(unit, "remove_paralyzed_stacks", self, "_remove_stacks")
		"confused":
			Utils.safe_disconnect(unit, "add_confused_stacks", self, "_add_stacks")
			Utils.safe_disconnect(unit, "remove_confused_stacks", self, "_remove_stacks")
	
	get_parent().remove_child(self)
