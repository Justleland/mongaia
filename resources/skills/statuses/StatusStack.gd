extends Reference
class_name StatusStack

var status = null

var label = ""
var description = ""
var count = 0


func _init(status, stack_count: int):
	if status == null:
		return null
	self.status = status
	count = stack_count
	if count > 1 and !status.stackable:
		count = 1
	return self

func decrement_action(unit):
	count -= 1


func get_priority() -> int:
	return status.get_priority()


func get_label() -> String:
	return status.label


func get_description() -> String:
	return status.description
