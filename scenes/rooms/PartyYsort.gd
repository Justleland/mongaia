extends YSort

const MAX_DIST = 75
const ACCELERATION = 40
const MAX_SPEED = 200

func _physics_process(delta):
	$TravelMonster1.follow($Player.position, $Player.temper)
	$TravelMonster2.follow($TravelMonster1.position, $Player.temper)
	$TravelMonster3.follow($TravelMonster2.position, $Player.temper)
