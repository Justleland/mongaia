extends VBoxContainer

onready var name_level = $HBox/Margin/UIMargin/VBox/NameLevel
onready var hp_bar = $HBox/Margin/UIMargin/VBox/StatMargin/VBox/PBars/HP
onready var hp_label = $HBox/Margin/UIMargin/VBox/StatMargin/VBox/NumMargins/HBox/HP
onready var def_bar = $HBox/Margin/UIMargin/VBox/StatMargin/VBox/PBars/Def
onready var def_label = $HBox/Margin/UIMargin/VBox/StatMargin/VBox/NumMargins/HBox/Def
#onready var statuses = $StatusMargin/HBox

export var enemy_config: bool = false

var panel_reversed = load("res://resources/style/UnitUIPanelReversed.tres")

var unit: Unit = null

func _ready():
	modulate.a = 0.0
	if enemy_config:
		var left = "custom_constants/margin_left"
		var right = "custom_constants/margin_right"
		
		$HBox/Margin/PanelMargin/Panel.set("custom_styles/panel", panel_reversed)
		
		_reverse_margins($HBox/Margin/PanelMargin)
		_reverse_margins($HBox/Margin/UIMargin)
		_reverse_margins($HBox/Margin/UIMargin/VBox/StatMargin)
		_reverse_margins($HBox/Margin/UIMargin/VBox/StatMargin/VBox/NumMargins)
		
		$HBox/Margin/UIMargin/VBox/NameLevel.align = Label.ALIGN_LEFT
		$HBox/Margin/UIMargin/VBox/StatMargin/VBox/NumMargins/HBox.alignment = BoxContainer.ALIGN_BEGIN


func _reverse_margins(node) -> void:
	var left = "custom_constants/margin_left"
	var right = "custom_constants/margin_right"
	var l_margin = node.get(left)
	var r_margin = node.get(right)
	node.set(left, r_margin)
	node.set(right, l_margin)


func _update_ui():
	if unit != null:
		_update_hp()
		_update_def()
		name_level.text = unit.monster.monster_name + "   Lvl: " + String(unit.monster.level)


func _update_hp():
	if unit != null:
		hp_bar.value = 100 * unit.hp / unit.max_hp
		hp_label.text = String(unit.hp) + " / " + String(unit.max_hp) 


func _update_def():
	if unit != null:
		def_bar.value = 100 * unit.def / unit.max_def
		def_label.text = String(unit.def) + " / " + String(unit.max_def)
		if unit.def <= 0:
			def_label.modulate.a = 0.4
		else:
			def_label.modulate.a = 1.0


func set_unit(unit: Unit):
	if self.unit != null:
		clear()
		
	self.unit = unit
	Utils.safe_connect(unit, "hp_def_changed", self, "_on_unit_hp_def_changed")
	Utils.safe_connect(unit, "level_changed", self, "_on_unit_level_changed")
	modulate.a = 1.0
	_update_ui()


func clear() -> void:
	_disconnect()
	unit = null
	modulate.a = 0.0

func _disconnect() -> void:
	if unit != null:
		Utils.safe_disconnect(unit, "hp_def_changed", self, "_on_unit_hp_def_changed")
		Utils.safe_disconnect(unit, "level_changed", self, "_on_unit_level_changed")


func _on_unit_hp_def_changed() -> void:
	_update_hp()
	_update_def()


func _on_unit_level_changed() -> void:
	_update_ui()
