extends HBoxContainer

signal tab_action(action)
signal tab_close

var unit = null

onready var buttons = [
	$Buttons/Button,
	$Buttons/Button2,
	$Buttons/Button3,
	$Buttons/Button4
]

onready var current_stance = $CurrentStanceCard
onready var selected_stance = $SelectedStanceCard

func _ready():
	_setup_button(buttons[0], BattleUtils.ST_NEU)
	_setup_button(buttons[1], BattleUtils.ST_STR)
	_setup_button(buttons[2], BattleUtils.ST_DEX)
	_setup_button(buttons[3], BattleUtils.ST_INT)
	visible = false

func _setup_button(button, text) -> void:
	button.text = text

func activate(_unit: Unit) -> void:
	visible = true
	unit = _unit
	current_stance.set_info_for_stance(unit.stance, unit.get_stance_value(), true)
	if unit.stance == BattleUtils.ST_NEU:
		selected_stance.set_info_for_stance(BattleUtils.ST_STR, unit.strength, false)
	else:
		selected_stance.set_info_for_stance(BattleUtils.ST_NEU, unit.neutral, false)
	
	for button in buttons:
		if button.text == unit.stance:
			button.disabled = true
			break
	

func deactivate() -> void:
	unit = null
	for button in buttons:
		button.pressed = false
		button.disabled = false
	visible = false


func _on_button_hover(pos) -> void:
	print("HOVER")
	match buttons[pos].text:
		BattleUtils.ST_NEU:
			selected_stance.set_info_for_stance(BattleUtils.ST_NEU, unit.neutral, false)
		BattleUtils.ST_STR:
			selected_stance.set_info_for_stance(BattleUtils.ST_STR, unit.strength, false)
		BattleUtils.ST_DEX:
			selected_stance.set_info_for_stance(BattleUtils.ST_DEX, unit.dexterity, false)
		BattleUtils.ST_INT:
			selected_stance.set_info_for_stance(BattleUtils.ST_INT, unit.intellect, false)
	


func _on_button_pressed(pos):
	print("PRESSED")
	match buttons[pos].text:
		BattleUtils.ST_NEU:
			unit.set_stance(BattleUtils.ST_NEU)
		BattleUtils.ST_STR:
			unit.set_stance(BattleUtils.ST_STR)
		BattleUtils.ST_DEX:
			unit.set_stance(BattleUtils.ST_DEX)
		BattleUtils.ST_INT:
			unit.set_stance(BattleUtils.ST_INT)
	emit_signal("tab_action", {
		"type": "STANCE",
		"acting": unit
	})
