extends GutTest

const neutral = ["NEUTRAL", 1.0]
const weak = ["WEAK", 2.0]
const resist = ["RESIST", 0.5]
const immune = ["IMMUNE", 0.0]

func test_angelic():
	var family = load("res://resources/monsters/families/impl/Angelic.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": resist,
		"ICE": neutral,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": weak,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": resist,
		"SHADOW": weak,
	}
	
	_assert_family_effects(family, expected)


func test_aquatic():
	var family = load("res://resources/monsters/families/impl/Aquatic.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": resist,
		"ICE": neutral,
		"ELEC": weak,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": resist,
		"NATURE": weak,
		"POISON": neutral,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_arcane():
	var family = load("res://resources/monsters/families/impl/Elemental.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": neutral,
		"ICE": neutral,
		"ELEC": resist,
		"WIND": neutral,
		"EARTH": weak,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": neutral,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_beast():
	var family = load("res://resources/monsters/families/impl/Beast.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": neutral,
		"ICE": resist,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": resist,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": neutral,
		"METAL": weak,
		"PSI": neutral,
		"MARTIAL": weak,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_demonic():
	var family = load("res://resources/monsters/families/impl/Demonic.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": immune,
		"ICE": neutral,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": weak,
		"NATURE": neutral,
		"POISON": resist,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": weak,
		"LIGHT": weak,
		"SHADOW": resist,
	}
	
	_assert_family_effects(family, expected)


func test_dragon():
	var family = load("res://resources/monsters/families/impl/Dragon.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": resist,
		"ICE": neutral,
		"ELEC": resist,
		"WIND": resist,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": neutral,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": weak,
		"SHADOW": weak,
	}
	
	_assert_family_effects(family, expected)


func test_ethereal():
	var family = load("res://resources/monsters/families/impl/Ethereal.tres")
	var expected = {
		"NEUTRAL": immune,
		"FIRE": neutral,
		"ICE": neutral,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": neutral,
		"METAL": weak,
		"PSI": weak,
		"MARTIAL": immune,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_flying():
	var family = load("res://resources/monsters/families/impl/Flying.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": neutral,
		"ICE": weak,
		"ELEC": weak,
		"WIND": resist,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": resist,
		"POISON": neutral,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": resist,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_humanoid():
	var family = load("res://resources/monsters/families/impl/Humanoid.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": neutral,
		"ICE": neutral,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": neutral,
		"METAL": neutral,
		"PSI": weak,
		"MARTIAL": resist,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_insectoid():
	var family = load("res://resources/monsters/families/impl/Insectoid.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": weak,
		"ICE": neutral,
		"ELEC": neutral,
		"WIND": weak,
		"EARTH": resist,
		"WATER": neutral,
		"NATURE": resist,
		"POISON": weak,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_material():
	var family = load("res://resources/monsters/families/impl/Material.tres")
	var expected = {
		"NEUTRAL": resist,
		"FIRE": resist,
		"ICE": resist,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": weak,
		"WATER": weak,
		"NATURE": neutral,
		"POISON": immune,
		"METAL": resist,
		"PSI": resist,
		"MARTIAL": weak,
		"LIGHT": neutral,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_plant():
	var family = load("res://resources/monsters/families/impl/Plant.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": weak,
		"ICE": weak,
		"ELEC": neutral,
		"WIND": neutral,
		"EARTH": neutral,
		"WATER": resist,
		"NATURE": resist,
		"POISON": weak,
		"METAL": neutral,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": resist,
		"SHADOW": neutral,
	}
	
	_assert_family_effects(family, expected)


func test_slime():
	var family = load("res://resources/monsters/families/impl/Slime.tres")
	var expected = {
		"NEUTRAL": resist,
		"FIRE": weak,
		"ICE": weak,
		"ELEC": weak,
		"WIND": weak,
		"EARTH": neutral,
		"WATER": neutral,
		"NATURE": neutral,
		"POISON": resist,
		"METAL": resist,
		"PSI": neutral,
		"MARTIAL": neutral,
		"LIGHT": resist,
		"SHADOW": resist,
	}
	
	_assert_family_effects(family, expected)


func test_undead():
	var family = load("res://resources/monsters/families/impl/Undead.tres")
	var expected = {
		"NEUTRAL": neutral,
		"FIRE": weak,
		"ICE": resist,
		"ELEC": resist,
		"WIND": neutral,
		"EARTH": resist,
		"WATER": neutral,
		"NATURE": weak,
		"POISON": resist,
		"METAL": neutral,
		"PSI": resist,
		"MARTIAL": neutral,
		"LIGHT": weak,
		"SHADOW": resist,
	}
	
	_assert_family_effects(family, expected)


func _assert_family_effects(family: Family, expected: Dictionary) -> void:
	for el in expected:
		var effect = expected[el][0]
		var multi = expected[el][1] 
		assert_eq(family.get_effect(el), effect, _fail_effect_text(family.text, el, effect))
		assert_eq(family.get_multiplier(el), multi, el + " should have " + String(multi) + "x multiplier against " + family.text)


func _fail_effect_text(family_text, element, effect) -> String:
	if effect == "RESIST":
		return family_text + " should RESIST " + element
	return family_text + " should be " + effect + " to " + element
