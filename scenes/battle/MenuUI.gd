extends HBoxContainer

enum {
	INACTIVE,
	BASE,
	SKILLS,
	TARGET,
	ITEM,
	SWITCH
}

onready var stance_btn = $Buttons/StanceButton
onready var buttons = [
	$Buttons/Button1,
	$Buttons/Button2,
	$Buttons/Button3,
	$Buttons/Button4,
	$Buttons/Button5
]

const base_menu = ["Skills", "Items", "Switch", "Pass", "Run"]

var unit: Unit = null
var state = INACTIVE
var skill_charge: SkillChargeCounter = null
var skill: Skill = null
var target = null
var targets: Array = []
var team = []

signal closed
signal action_selected(dict)

func _ready():
	stance_btn.visible = false
	var i = 1
	for button in buttons:
		button.visible = false
		_connect_button(button, i)
		i += 1

func _input(event):
	if event.is_action_released("ui_cancel") and state != INACTIVE:
		_revert_state()


func activate(unit: Unit, targets: Array, team: Array) -> bool:
	if unit == null:
		return false
	
	self.unit = unit
	self.targets = targets
	self.team = team
	stance_btn.set_stance(unit.stance)
	_format_base_menu()
	return true


func _format_buttons(values: Array) -> void:
	var i = 0
	while i < buttons.size() and i < values.size():
		buttons[i].set_value(values[i])
		i += 1
	
	while i < buttons.size():
		buttons[i].visible = false
		i += 1


func _format_base_menu():
	_format_buttons(base_menu)
	stance_btn.visible = false
	state = BASE


func _format_skill_menu():
	stance_btn.visible = true
	_format_buttons(unit.skills)
	state = SKILLS


func _format_switch_menu(values: Array):
	_format_buttons(values)
	state = SWITCH


func _revert_state():
	match state:
		SKILLS:
			_format_base_menu()
		TARGET:
			_format_skill_menu()
		SWITCH:
			_format_base_menu()


func _close() -> void:
	state = INACTIVE
	unit = null
	targets = []
	team = []
	stance_btn.visible = false
	for button in buttons:
		button.visible = false
	emit_signal("closed")


func _button_response(value):
	match state:
		BASE:
			_base_response(value)
		SKILLS:
			_skills_response(value)
		TARGET:
			_target_response(value)
		SWITCH:
			_switch_response(value)


func _base_response(value):
	match value:
		"Skills":
			_format_skill_menu()
		"Switch":
			var switch_targets = _valid_switch_targets()
			if !switch_targets.empty():
				_format_switch_menu(switch_targets)
		"Pass":
			_submit_action({
				"type": "PASS",
				"acting": unit
			})
			_close()


func _skills_response(value):
	if value is SkillChargeCounter and value.get_skill() is Skill and value.get_skill().target is TargetType:
		skill_charge = value
		skill = value.get_skill()
		var valid_targets = []
		for target in targets:
			if skill.target.is_valid_target(target, unit):
				valid_targets.append(target)
		
		if skill.target.multi and !valid_targets.empty():
			unit.stance = stance_btn.stance
			stance_btn.visible = false
			skill_charge.reset(unit.stance != BattleUtils.ST_NEU)
			_submit_action({
				"type": "SKILL",
				"skill": skill,
				"acting": unit,
				"target": valid_targets
			})
		elif !valid_targets.empty():
			_format_buttons(valid_targets)
			stance_btn.visible = false
			state = TARGET
		else:
				print("NO VALID TARGETS\n")
	else:
		print("ERROR: NOT SKILL OR SKILL IMPLEMENTED INCORRECTLY\n")


func _target_response(value):
	if value is Unit:
		unit.stance = stance_btn.stance
		skill_charge.reset(unit.stance != BattleUtils.ST_NEU)
		var action = {
			"type": "SKILL",
			"skill": skill,
			"acting": unit,
			"target": [value]
		}
		_submit_action(action)


func _switch_response(value):
	if value is Unit:
		_submit_action({
			"type": "SWITCH",
			"side": "PLAYER",
			"acting": unit,
			"switch_to": value
		})


func _submit_action(action: Dictionary) -> void:
	emit_signal("action_selected", action)
	_close()


func _valid_switch_targets() -> Array:
	var valid_targets = []
	var player_units = []
	for u in targets:
		if u.player_owned:
			player_units.append(u)
	for t_unit in team:
		if t_unit.hp <= 0:
			continue
		if _unit_is_active(t_unit, player_units):
			continue
		valid_targets.append(t_unit)
	return valid_targets


func _unit_is_active(u: Unit, active_units: Array) -> bool:
	if active_units.empty():
		return false
	for a_unit in active_units:
		if a_unit == u:
			return true
	return false


func _connect_button(button, num: int) -> void:
	var method = "_on_dead_button"
	match num:
		1:
			method = "_on_button1_up"
		2:
			method = "_on_button2_up"
		3:
			method = "_on_button3_up"
		4:
			method = "_on_button4_up"
		5:
			method = "_on_button5_up"
	
	Utils.safe_connect(button, "button_up", self, method)


func _on_StanceButton_button_up():
	var stance = ""
	
	match stance_btn.stance:
		BattleUtils.ST_NEU:
			stance = BattleUtils.ST_STR
		BattleUtils.ST_STR:
			stance = BattleUtils.ST_DEX
		BattleUtils.ST_DEX:
			stance = BattleUtils.ST_INT
		BattleUtils.ST_INT:
			stance = BattleUtils.ST_NEU
	
	stance_btn.set_stance(stance)


func _on_button1_up():
	_button_response(buttons[0].value)


func _on_button2_up():
	_button_response(buttons[1].value)


func _on_button3_up():
	_button_response(buttons[2].value)


func _on_button4_up():
	_button_response(buttons[3].value)


func _on_button5_up():
	_button_response(buttons[4].value)


func _on_dead_button():
	pass
