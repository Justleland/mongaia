extends PanelContainer

var min_size
var timer = 0

func _ready():
	min_size = rect_size
	_set_alpha(0.0)


func _process(delta):
	if modulate.a > 0.0:
		if timer >= 60:
			_modulate_alpha(-0.05)
		else:
			timer += 1


func display_text(text: String) -> void:
	if text != null and !text.empty(): 
		rect_size = min_size
		$Label.text = text
		_set_alpha(1.0)
		timer = 0


func _set_alpha(alpha: float) -> void:
	modulate = Color(modulate.r, modulate.g, modulate.b, _safe_alpha(alpha))


func _modulate_alpha(rate: float) -> void:
	var alpha = _safe_alpha(modulate.a + rate)
	modulate = Color(modulate.r, modulate.g, modulate.b, alpha)


func _safe_alpha(value: float) -> float:
	return clamp(value, 0.0, 1.0)
