# Enemies manages the enemy units, their display, and their UI
extends Node

var units setget ,_get_units
var defeated_units setget ,_get_defeated_units

var unit0: Unit = null
var unit1: Unit = null
var unit2: Unit = null
var unit3: Unit = null
var unit4: Unit = null

onready var display0 = $UnitDisplay1/Sprite
onready var display1 = $UnitDisplay2/Sprite
onready var display2 = $UnitDisplay3/Sprite
onready var display3 = $UnitDisplay4/Sprite
onready var display4 = $UnitDisplay5/Sprite

onready var ui0 = $UI/UIHBox/UnitUI1
onready var ui1 = $UI/UIHBox/UnitUI2
onready var ui2 = $UI/UIHBox/UnitUI3
onready var ui3 = $UI/UIHBox/UnitUI4
onready var ui4 = $UI/UIHBox/UnitUI5

func assign(pos: int, unit: Unit) -> void:
	if unit != null:
		if pos == 0:
			unit0 = unit
			display0.texture = unit.monster.texture
			ui0.set_unit(unit)
		if pos == 1:
			unit1 = unit
			display1.texture = unit.monster.texture
			ui1.set_unit(unit)
		if pos == 2:
			unit2 = unit
			display2.texture = unit.monster.texture
			ui2.set_unit(unit)
		if pos == 3:
			unit3 = unit
			display3.texture = unit.monster.texture
			ui3.set_unit(unit)
		if pos == 4:
			unit4 = unit
			display4.texture = unit.monster.texture
			ui4.set_unit(unit)


func unassign(unit: Unit) -> void:
	var pos = find(unit)
	if pos == 0:
		unit0 = null
		display0.texture = null
		ui0.clear()
	if pos == 1:
		unit1 = null
		display1.texture = null
		ui1.clear()
	if pos == 2:
		unit2 = null
		display2.texture = null
		ui2.clear()
	if pos == 3:
		unit3 = null
		display3.texture = null
		ui3.clear()
	if pos == 4:
		unit4 = null
		display4.texture = null
		ui4.clear()


func switch(a_unit, b_unit) -> void:
	assign(find(a_unit), b_unit)


func find(unit: Unit) -> int:
	if unit != null:
		if unit == unit0:
			return 0
		if unit == unit1:
			return 1
		if unit == unit2:
			return 2
		if unit == unit3:
			return 3
		if unit == unit4:
			return 4
	return -1


func get_display(pos: int):
	if pos == 0:
		return display0
	if pos == 1:
		return display1
	if pos == 2:
		return display2
	if pos == 3:
		return display3
	if pos == 4:
		return display4


func _get_units() -> Array:
	var units = [];
	if unit0 != null:
		units.append(unit0)
	if unit1 != null:
		units.append(unit1)
	if unit2 != null:
		units.append(unit2)
	if unit3 != null:
		units.append(unit3)
	if unit4 != null:
		units.append(unit4)
	return units


func _get_defeated_units() -> Array:
	var units = [];
	if unit0 != null and unit0.hp <= 0:
		units.append(unit0)
	if unit1 != null and unit1.hp <= 0:
		units.append(unit1)
	if unit2 != null and unit2.hp <= 0:
		units.append(unit2)
	if unit3 != null and unit3.hp <= 0:
		units.append(unit3)
	if unit4 != null and unit4.hp <= 0:
		units.append(unit4)
	return units

