extends Panel

var unit: BattleUnit = null
var red = load("res://resources/style/TimeIndicatorRed.tres")

func _ready():
	visible = false


func _process(delta):
	_move_timerMarkers()


func add_unit(unit: BattleUnit, is_red: bool):
	if unit is BattleUnit:
		self.unit = unit
		Utils.safe_connect(unit, "state_inactive", self, "_on_unit_state_inactive")
		Utils.safe_connect(unit, "state_active", self, "_on_unit_state_active")
		if is_red:
			set("custom_styles/panel", red)


func _move_timerMarkers() -> void:
	if unit.monster != null:
		match unit.state:
			BattleUnit.WAIT:
				var wait_time = 100 * (100 / (unit.monster.agi * unit.agi_mod))
				rect_position.x = 500 * unit.wait_timer / wait_time - 300
			BattleUnit.PREP:
				rect_position.x = 500 - 300
			BattleUnit.TARGET:
				rect_position.x = 500 -300
			BattleUnit.CHARGE:
				rect_position.x = 500 + (100 * unit.charge_timer / unit.charge_time) -300
			BattleUnit.EXECUTE:
				rect_position.x = 600 - 300

func _on_unit_state_inactive() -> void:
	visible = false


func _on_unit_state_active() -> void:
	visible = true
