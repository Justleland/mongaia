extends Reference
class_name TurnOrder

var num: int = 0
var active: Unit = null
var current_turn: Array = []
var next_turn: Array = []

class UnitSorter:
	static func decend(a, b):
		if a is Unit and b is Unit:
			var a_priority = a.get_priority()
			var b_priority = b.get_priority()
			if a_priority != b_priority:
				return a_priority > b_priority
			return (a.agi + a.charge) > (b.agi + b.charge)
		return false
	
	
	static func ascend(a, b):
		if a is Unit and b is Unit:
			var a_priority = a.get_priority()
			var b_priority = b.get_priority()
			if a_priority != b_priority:
				return a_priority < b_priority
			return (a.agi + a.charge) < (b.agi + b.charge)
		return false


func _sort_turns() -> void:
	_sort(current_turn)
	_sort(next_turn)


func _sort(turn: Array) -> void:
	turn.sort_custom(UnitSorter, "decend")


func next() -> Unit:
	if active != null:
		next_turn.append(active)
		active = null
	
	_sort_turns()
	
	if current_turn.empty():
		current_turn = next_turn
		next_turn = []
		num += 1
	
	active = current_turn.pop_front()
	active.charge()
	return active


func add(unit: Unit) -> void:
	if unit != null:
		next_turn.append(unit)
		_sort(next_turn)


func remove(unit: Unit) -> void:
	if active == unit:
		active = null
	elif current_turn.has(unit):
		current_turn.erase(unit)
	elif next_turn.has(unit):
		next_turn.erase(unit)


func get_order() -> Array:
	var order = []
	
	if active != null:
		order.append(active)
	order += current_turn
	order.append("TURN SPACE")
	order += next_turn
	
	return order


# for debugging purposes
func print_order():
	print("Turn Order: \n\tCurrent Turn (" + String(num) + "):") 
	print("\t\t" + active.monster.monster_name + " " + String(active.agi))
	for unit in current_turn:
		print("\t\t" + unit.monster.monster_name + " " + String(unit.agi))
	if !next_turn.empty():
		print("\n\tNext Turn (" + String(num) + "):")
		for unit in next_turn:
			print("\t\t" + unit.monster.monster_name + " " + String(unit.agi))
	print()

