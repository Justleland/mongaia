extends BattleAnimation
class_name StrikeAnimation

func _process(delta):
	if !finished:
		finished = true
		emit_signal("animation_finished")
		
	if frame == frames.get_frame_count("finish") - 1:
		complete_skill()


func setup(skill_data: Dictionary) -> void:
	.setup(skill_data)
	
	if skill_data.has("target_display"):
		skill_data["target_display"].add_child(self)


func complete_skill():
	get_parent().remove_child(self)
