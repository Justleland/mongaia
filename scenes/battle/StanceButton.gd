extends Button

var stance: String = BattleUtils.ST_NEU

func _ready():
	text = "NEUTRAL"
	

func set_stance(stance: String) -> void:
	if stance == BattleUtils.ST_NEU:
		self.stance = stance
		text = "NEUTRAL"
	if stance == BattleUtils.ST_STR:
		self.stance = stance
		text = "STRENGTH"
	if stance == BattleUtils.ST_DEX:
		self.stance = stance
		text = "DEXTERITY"
	if stance == BattleUtils.ST_INT:
		self.stance = stance
		text = "INTELLECT"
