extends PanelContainer

onready var title_label = $VBoxContainer/TitleMargin/TitleLabel
onready var description_label = $VBoxContainer/BodyMargin/SkillDescription/DescriptionLabel
onready var skill_view = $VBoxContainer/BodyMargin/SkillDescription
onready var stance_view = $VBoxContainer/BodyMargin/StanceDescription
onready var monster_view = $VBoxContainer/BodyMargin/MonsterDescription

var toggled = false

func _ready():
	visible = false


func toggle() -> void:
	if toggled:
		visible = false
		toggled = false
	else:
		visible = true
		toggled = true


func _view_visible(view) -> void:
	skill_view.visible = false
	stance_view.visible = false
	monster_view.visible = false
	
	if view != null:
		view.visible = true


func set_title(text: String) -> void:
	title_label.text = text


func set_description(text: String) -> void:
	description_label.text = text


func set_title_description(title: String, description: String) -> void:
	set_title(title)
	set_description(description)


func set_with_skill(skill: Skill) -> void:
	_view_visible(skill_view)
	set_title(skill.skill_name)
	set_description(skill.description)
	$VBoxContainer/BodyMargin/SkillDescription/StatBox/LeftStatBox/Element/ElementDesc.text = skill.element_type_text()
	$VBoxContainer/BodyMargin/SkillDescription/StatBox/LeftStatBox/Charge/ChargeDesc.text = skill.charge_time_text()
	$VBoxContainer/BodyMargin/SkillDescription/StatBox/LeftStatBox/SPCost/SPCostDesc.text = String(skill.sp_cost)
	$VBoxContainer/BodyMargin/SkillDescription/StatBox/RightStatBox/Acc/AccDesc.text = String(skill.get_acc_num())
	
	if skill is SkillDamaging:
		$VBoxContainer/BodyMargin/SkillDescription/StatBox/RightStatBox/Power/PowerDesc.text = String(skill.power)
		$VBoxContainer/BodyMargin/SkillDescription/StatBox/RightStatBox/Pent/PentDesc.text = String(skill.penetration)
		$VBoxContainer/BodyMargin/SkillDescription/StatBox/RightStatBox/Crit/CritDesc.text = String(skill.crit)


func set_with_stance(stance_name: String, stance_desc: String, unit: BattleUnit) -> void:
	_view_visible(stance_view)
	set_title(stance_name)
	$VBoxContainer/BodyMargin/StanceDescription/HSplit/Description.text = stance_desc
	$VBoxContainer/BodyMargin/StanceDescription/HSplit/VBox/Strength/Desc.text = _stance_num_format(unit.get_str(), unit.get_str(stance_name))
	$VBoxContainer/BodyMargin/StanceDescription/HSplit/VBox/Dexterity/Desc.text = _stance_num_format(unit.get_dex(), unit.get_dex(stance_name))
	$VBoxContainer/BodyMargin/StanceDescription/HSplit/VBox/Intellect/Desc.text = _stance_num_format(unit.get_int(), unit.get_int(stance_name))


func set_monster_view(monster: Monster) -> void:
	_view_visible(monster_view)
	set_title(monster.monster_name)
	
	var family_names = monster.monsterData.get_family_text()
	$VBoxContainer/BodyMargin/MonsterDescription/Family/Family1.text = family_names[0]
	if family_names[1] == "":
		$VBoxContainer/BodyMargin/MonsterDescription/Family/Family2.visible = false
	else:
		$VBoxContainer/BodyMargin/MonsterDescription/Family/Family2.visible = true
		$VBoxContainer/BodyMargin/MonsterDescription/Family/Family2.text = family_names[1]
	
	$VBoxContainer/BodyMargin/MonsterDescription/HP/HSplit/ProgressBar.value = 100 * (monster.hp / monster.max_hp)
	$VBoxContainer/BodyMargin/MonsterDescription/HP/Margin/Num.text = _consumable_stat_format(monster.hp, monster.max_hp)
	
	$VBoxContainer/BodyMargin/MonsterDescription/SP/HSplit/ProgressBar.value = 100 * (monster.sp / monster.max_sp)
	$VBoxContainer/BodyMargin/MonsterDescription/SP/Margin/Num.text = _consumable_stat_format(monster.sp, monster.max_sp)
	
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/LeftVBox/ATK/Num.text = String(monster.atk)
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/LeftVBox/DEF/Num.text = String(monster.def)
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/LeftVBox/AGI/Num.text = String(monster.agi)
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/RightVBox/STR/Num.text = String(monster.strength)
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/RightVBox/DEX/Num.text = String(monster.dexterity)
	$VBoxContainer/BodyMargin/MonsterDescription/Stats/RightVBox/INT/Num.text = String(monster.intellect)
	
	if monster.level == 100:
		$VBoxContainer/BodyMargin/MonsterDescription/EXP/HSplit/Margin/ProgressBar.value = 100
		$VBoxContainer/BodyMargin/MonsterDescription/EXP/Margin/Num.text = "COMPLETE"
	else:
		var current_lvl_exp = monster.monsterData.calculate_exp_from_level(monster.level)
		var next_lvl_exp = monster.monsterData.calculate_exp_from_level(monster.level + 1)
		var percentage = 100 * ((monster.experience - current_lvl_exp) / (next_lvl_exp - current_lvl_exp))
		$VBoxContainer/BodyMargin/MonsterDescription/EXP/HSplit/Margin/ProgressBar.value = 100 * (monster.experience - current_lvl_exp) / (next_lvl_exp - current_lvl_exp)
		$VBoxContainer/BodyMargin/MonsterDescription/EXP/Margin/Num.text = "Next: " + String(next_lvl_exp - monster.experience)



func _stance_num_format(current: int, change: int) -> String:
	return String(current) + " -> " + String(change)


func _consumable_stat_format(value: int, max_value: int) -> String:
	return String(value) + " / " + String(max_value)
