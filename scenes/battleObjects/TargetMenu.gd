extends VBoxContainer

var unit: BattleUnit

onready var buttons = [
	$Button0,
	$Button1,
	$Button2,
	$Button3,
	$Button4,
	$Button5
]

var button_values = [0, 1, 2, 3, 4, 5]

var player_targets

var enemy_targets

func _ready():
	for button in buttons:
		button.visible = false
		button.disabled = true


func set_unit(value: BattleUnit) -> void:
	if unit == null and value is BattleUnit and value.monster is Monster:
		unit = value
		if unit.skill.target == Skill.Target.MULTI:
			unit.target = enemy_targets
			unit.state = BattleUnit.CHARGE
			unit._set_sprite_color(1, 0, 1)
			unit = null
		else:
			var pos = 0
			for i in range(enemy_targets.size()):
				var enemy = enemy_targets[i]
				if(enemy.monster is Monster):
					_activate_button(buttons[pos], enemy.monster.monster_name)
					button_values[pos] = i
					pos += 1


func _activate_button(button: Button, move_name: String) -> void:
	if move_name is String:
		button.text = move_name
		button.visible = true
		button.disabled = false
	else:
		button.visible = false
		button.disabled = true


func _prep_unit_and_dispatch(value: int) -> void:
	if unit != null:
		unit.state = BattleUnit.CHARGE
		unit._set_sprite_color(1, 0, 1)
		unit.target = [enemy_targets[button_values[value]]]
	
	unit = null
	for i in range(buttons.size()):
		buttons[i].visible = false
		buttons[i].disabled = true	


func _on_button_up(extra_arg_0):
	_prep_unit_and_dispatch(extra_arg_0) # Replace with function

