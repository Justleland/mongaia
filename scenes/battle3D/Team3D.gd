extends Spatial

var display_scene = preload("res://scenes/battle3D/UnitDisplay3D.tscn")

var units: Array = [] setget ,_get_units
var displays = []
var display_units: Array setget ,_get_display_units
var is_wild: bool = false
var is_player: bool
var length: float = 3.0

func _ready():
	var children = get_children()
	for child in children:
		remove_child(child)

func setup(units: Array, player_side: bool, wild_config := false) -> void:
	is_player = player_side
	if !is_player:
		is_wild = wild_config
	
	if units.size() <= _get_max_units():
		self.units = units
	else:
		Utils.log_print("Unit size too large: " + String(units.size()))
	
	var max_displays = _get_max_displays()
	var unit_index = 0
	var display_index = 0
	while unit_index < units.size() and display_index < max_displays:
		var unit = units[unit_index]
		if unit is Unit and unit.hp > 0:
			var display = display_scene.instance()
			display.setup(unit)
			if !is_player:
				display.flip_h = true
			add_child(display)
			displays.append(display)
			display_index += 1
		unit_index += 1
	
	if is_player:
		align(3.5, length)
	else:
		align(-3.5, length)


func align(center: float, length: float) -> void:
	translation.x = center
	Utils.log_print("Center: " + String(translation.x))
	self.length = length
	var start = length / 2.0
	var direction = -1
	var spacing
	
	
	if displays.size() > 3:
		spacing = length / 2.0
		_move_display(displays[0], start, -0.65 * 2)
		_move_display(displays[1], start - (spacing * 0.85), -0.65 * 3.0)
		_move_display(displays[2], start - spacing, -0.65)
		if displays.size() > 4:
			_move_display(displays[3], start - spacing - (spacing * 0.85), -0.65 * 1.5)
			_move_display(displays[4], start - (spacing * 2.0), 0.0)
		else:
			_move_display(displays[3], start - (spacing * 2.0), 0.0)
		
		for display in displays:
			Utils.log_print(display.translation.x)
	else:
		if !is_player:
			start = -start
			direction = 1
			
		spacing = length / (float(displays.size()) - 1.0)
		for i in range(displays.size()):
			_move_display(
				displays[displays.size() - (i + 1)], 
				start + (spacing * i * direction), 
				-0.65 * i
			)
			Utils.log_print(displays[displays.size() - (i + 1)].translation.x)


func _move_display(display, x: float, z: float) -> void:
	display.translation.x = x
	display.translation.z = z

func switch(unit_a, unit_b) -> bool:
	var display_units = _get_display_units()
	if display_units.has(unit_a) and !display_units.has(unit_b) and units.has(unit_b):
		var b_display = _create_display(unit_b)
		var pos = _get_display_position(unit_a)
		var a_display = displays[pos]
		displays[pos] = b_display
		b_display.translation = a_display.translation
		_destory_display(a_display)
		return true
	return false


func _create_display(unit):
	var display = display_scene.instance()
	display.setup(unit)
	if !is_player:
		display.flip_h = true
	add_child(display)
	return display


func _destory_display(display):
	remove_child(display)
	

func _get_units():
	return units


func _get_display_position(unit) -> int:
	for i in range(displays.size()):
		if displays[i].unit == unit:
			return i
	return -1


func _get_display_units() -> Array:
	var units = []
	for display in displays:
		if display.unit is Unit:
			units.append(display.unit)
	return units


func _get_max_units():
	if is_wild:
		return 5
	return 6


func _get_max_displays():
	if is_wild:
		return 5
	return 3
