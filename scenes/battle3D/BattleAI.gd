extends Reference
class_name BattleAI

signal processed
signal action_processed(action)

var unit: Unit = null

func activate(unit: Unit) -> bool:
	if unit == null:
		return false
	
	self.unit = unit
	return true


func process() -> void:
	if unit != null:
		_submit_action({
			"type": "PASS",
			"acting": unit
		})
		unit = null


func _submit_action(action: Dictionary) -> void:
	if action is Dictionary:
		emit_signal("action_processed", action)


## Need action for switching
