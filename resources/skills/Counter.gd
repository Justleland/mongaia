extends Node2D

class_name Counter

var space = -30

var timer = 0

func _ready():
	$CenterContainer/Crit.visible = false

func _process(delta):
	position.y -= 0.8
	if modulate.a > 0.0:
		if timer >= 60:
			_modulate_alpha(-0.04)
		else:
			timer += 1
	else:
		get_parent().remove_child(self)

func attach(parent: Node) -> Node2D:
	parent.add_child(self)
	return self

func set_location(pos: Vector2) -> Node2D:
	position = pos
	return self


func set_message(message: String) -> Node2D:
	$CenterContainer/DamageHit.text = message
	return self


func set_damage(damage: int) -> Node2D:
	$CenterContainer/DamageHit.text = String(damage)
	return self


func set_stat(stat_name: String, amount: int) -> Node2D:
	var message = stat_name
	if amount > 0:
		message = message + " up"
	elif amount < 0:
		message = message + " down"
	for i in range(abs(amount) - 1):
		message = message + "+"
	return set_message(message)

func set_miss(hit: bool) -> Node2D:
	if !hit:
		$CenterContainer/DamageHit.text = "Miss"
	return self


func set_crit(crit: bool) -> Node2D:
	if crit:
		$CenterContainer/Crit.visible = true
	return self


func set_tint(mod: float, crit: bool = false) -> Node2D:
	var mod_value = mod
	if crit:
		mod_value = mod * 2
	if mod_value > 1.0:
		_tint_labels(_safe_color_value(mod_value - 1.0), true, false)
	elif mod_value < 1.0 and mod_value >= 0.0:
		_tint_labels(_safe_color_value((1.0 - mod_value) / 0.5), false, true)
	return self


func tint_green() -> Node2D:
	modulate = Color.green
	return self


func tint_cyan() -> Node2D:
	modulate = Color.cyan
	return self


func tint(color: Color) -> Node2D:
	modulate = color
	return self


func _safe_color_value(value: float) -> float:
	return clamp(value, 0.0, 1.0)


func _tint_labels(value: float, red: bool, blue: bool) -> void:
	if red and not blue:
		modulate = Color(value, 0.0, 0.0, 1.0)
	if blue and not red:
		modulate = Color(0.0, 0.0, value, 1.0)


func _set_alpha(alpha: float) -> void:
	modulate = Color(modulate.r, modulate.g, modulate.b, _safe_alpha(alpha))


func _modulate_alpha(rate: float) -> void:
	var alpha = _safe_alpha(modulate.a + rate)
	modulate = Color(modulate.r, modulate.g, modulate.b, alpha)


func _safe_alpha(value: float) -> float:
	return clamp(value, 0.0, 1.0)
