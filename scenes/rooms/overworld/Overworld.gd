extends Node2D

export (Array, String) var go_to_rooms: Array = []
export (Array, Vector2) var go_to_pos: Array = []

func _on_Area2D_body_entered(body, room_num):
	if go_to_rooms[room_num] != null and go_to_pos[room_num] != null:
		PlayerData.load_pos = go_to_pos[room_num]
		get_tree().change_scene(go_to_rooms[room_num])
