extends Control

onready var level_tabs = [
	$Margin/VBox/LevelTab0,
	$Margin/VBox/LevelTab1,
	$Margin/VBox/LevelTab2,
	$Margin/VBox/LevelTab3,
	$Margin/VBox/LevelTab4,
	$Margin/VBox/LevelTab5
]

onready var level_up_ui = $RightUI/LevelUpUI
onready var learn_skill_ui = $RightUI2/LearnSkillUI

var exp_earned = 0
var level_up_list = []
var learned_skills = []
var all_complete = false

signal complete

func _ready():
	level_up_ui.visible = false
	Utils.safe_connect(learn_skill_ui, "inactive", self, "_on_learn_skill_inactive")
	for i in range(min(PlayerData.team.size(), level_tabs.size())):
		level_tabs[i].setup(PlayerData.team[i])
		Utils.safe_connect(level_tabs[i], "complete", self, "_on_level_tab_complete")


func _input(event):
	if all_complete and (event.is_action_released("ui_accept") or event.is_action_released("mouse_controls")):
		if learn_skill_ui.active:
				level_up_ui.visible = false
				learn_skill_ui.visible = true
		else:
			_display_next_level_up()


func _display_next_level_up() -> void:
	var tab = level_up_list.pop_front()
	if tab != null:
		display_level_up(tab)
	else:
		emit_signal("complete")


func gain_exp(amount: int) -> void:
	$Margin/VBox/HSplitContainer/NumLabel.text = String(amount)
	exp_earned = amount
	for level_tab in level_tabs:
		if level_tab.active:
			level_tab.gain_exp(amount)


func display_level_up(tab) -> void:
	var monster = tab.monster
	level_up_ui.visible = true
	learn_skill_ui.visible = false
	level_up_ui.set_name(monster.monster_name)
	level_up_ui.set_hp(tab.current_max_hp, monster.max_hp)
	level_up_ui.set_sp(tab.current_max_sp, monster.max_sp)
	level_up_ui.set_atk(tab.current_atk, monster.atk)
	level_up_ui.set_def(tab.current_def, monster.def)
	level_up_ui.set_agi(tab.current_agi, monster.agi)
	level_up_ui.set_str(tab.current_str, monster.strength)
	level_up_ui.set_dex(tab.current_dex, monster.dexterity)
	level_up_ui.set_int(tab.current_int, monster.intellect)
	learn_skill_ui.setup(monster, tab.current_lvl, monster.level)


func _on_level_tab_complete() -> void:
	all_complete = true
	for level_tab in level_tabs:
		if !level_tab.complete:
			all_complete = false
			break
	if all_complete:
		for level_tab in level_tabs:
			var monster = level_tab.monster
			if monster is Monster and monster.hp > 0:
				monster.add_exp(exp_earned)
				if monster.level > level_tab.current_lvl:
					level_up_list.append(level_tab)


func _on_learn_skill_inactive() -> void:
	_display_next_level_up()
