extends VBoxContainer

onready var name_level = $NameLevel
onready var species = $Species
onready var type1 = $HSplit/Stats/Types/Type1
onready var type2 = $HSplit/Stats/Types/Type2
onready var hp_bar = $HSplit/Stats/HP/HSplit/Margin/HpBar
onready var hp_num = $HSplit/Stats/HP/Num
onready var stat_def = $HSplit/Stats/StatBlock1/Defense/Panel/Center/Label
onready var stat_agi = $HSplit/Stats/StatBlock1/Agility/Panel/Center/Label
onready var stat_str = $HSplit/Stats/StatBlock2/Strength/Panel/Center/Label
onready var stat_dex = $HSplit/Stats/StatBlock2/Dexterity/Panel/Center/Label
onready var stat_int = $HSplit/Stats/StatBlock2/Intellect/Panel/Center/Label

func _ready():
	$HSplit/Stats/StatBlock1/Defense/Label.text = "DEF"
	$HSplit/Stats/StatBlock1/Agility/Label.text = "AGI"
	$HSplit/Stats/StatBlock2/Strength/Label.text = "STR"
	$HSplit/Stats/StatBlock2/Dexterity/Label.text = "DEX"
	$HSplit/Stats/StatBlock2/Intellect/Label.text = "INT"

func set_info(unit: Unit) -> void:
	if unit != null:
		name_level.text = unit.monster.monster_name + " Lvl: " + String(unit.level)
		
		if unit.monster.monster_name != unit.monster.monsterData.species:
			species.text = unit.monster.monsterData.species
			species.modulate.a = 1.0
		else:
			species.modulate.a = 0.0
		
		type1.text = unit.family1.text
		
		if unit.family2 is Family:
			type2.text = unit.family2.text
			type2.visible = true
		else:
			type2.visible = false
		
		hp_bar.value = 100.0 * float(unit.hp) / float(unit.max_hp)
		hp_num.text = String(unit.hp) + "/" + String(unit.max_hp)
		stat_def.text = String(unit.def)
		stat_agi.text = String(unit.agi)
		stat_str.text = String(unit.strength)
		stat_dex.text = String(unit.dexterity)
		stat_int.text = String(unit.intellect)
