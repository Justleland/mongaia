extends Button

var unit: Unit = null 

func _ready():
	disabled = true


func set_unit(_unit: Unit) -> void:
	if _unit != null:
		unit = _unit
		text = unit.monster.monster_name + " Lvl: " + String(unit.level)
		if unit.hp > 0:
			disabled = false
		else:
			disabled = true
	else:
		disabled = true
