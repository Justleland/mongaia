extends Skill
class_name SkillHealing

export(int) var hp_percent = 0
export(int) var sp_percent = 0

export(bool) var heal_all_conditions
export(Conditions) var heal_condition


export(bool) var nullify_stats = false


func get_healing_result(target: BattleUnit) -> Heal:
	var heal = heal_template.new()
	if target is BattleUnit and target.monster is Monster:
		heal.hp_heal = target.monster.max_hp * (float(hp_percent) / 100.0)
		heal.sp_heal = target.monster.max_sp * (float(sp_percent) / 100.0)
	return heal


func is_removing_conditions() -> bool:
	return heal_all_conditions or heal_condition != Conditions.NONE


func get_healed_conditions() -> Array:
	if heal_all_conditions:
		return [
			Conditions.BURNED,
			Conditions.CONFUSED,
			Conditions.FROZEN,
			Conditions.PARALYZED,
			Conditions.POISONED
		]
	return [heal_condition]
