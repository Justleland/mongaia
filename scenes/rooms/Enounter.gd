extends Node

class_name Encounter

export(float) var weight: float = 0.0

export(Resource) var monster_1
export(int) var monster_1_lvl_min = 1
export(int) var monster_1_lvl_max = 1 

export(Resource) var monster_2
export(int) var monster_2_lvl_min = 1
export(int) var monster_2_lvl_max = 1

export(Resource) var monster_3
export(int) var monster_3_lvl_min = 1
export(int) var monster_3_lvl_max = 1

export(Resource) var monster_4
export(int) var monster_4_lvl_min = 1
export(int) var monster_4_lvl_max = 1

export(Resource) var monster_5
export(int) var monster_5_lvl_min = 1
export(int) var monster_5_lvl_max = 1
