extends RootScene3D

enum {
	BATTLE_AI,
	INTERPRET,
	STATUS,
	ANIMATE,
	SKIP_TURN,
	FAINT,
	NEXT,
}

const buffer = 80

var turn_order = TurnOrder.new()
var battle_ai = BattleAI.new()
var action_interpreter = ActionInterpreter.new()
var action_animator = ActionAnimator.new()


var buffer_count = 0
var buffer_actions = []
var player_monsters = []
var enemy_monsters = []
var npc_battle = false
var from_scene = true

onready var player_team = $PlayerTeam
onready var enemy_team = $EnemyTeam
onready var player_unit_ui = $PlayerUnitUI
onready var enemy_unit_ui = $EnemyUnitUI
onready var turn_order_ui = $TurnOrderMargin/TurnOrderUI
onready var menu_ui = $MenuMargin/MenuUI3D
onready var title_ui = $ActionTitleUI

func init(parameters: Dictionary):
	Utils.log_print(parameters)
	
	from_scene = false
	
	if parameters.has_all(["players_team", "enemy_team"]):
		player_monsters = parameters["players_team"]
		enemy_monsters = parameters["enemy_team"]
	
	if parameters.has("npc_battle"):
		npc_battle = parameters["npc_battle"]


func _ready():
#	Utils.safe_connect(battle_ai, "processed", self, "_on_battle_ai_processed")
	Utils.safe_connect(battle_ai, "action_processed", self, "_on_action_selected")
	Utils.safe_connect(menu_ui, "action_selected", self, "_on_action_selected")
	Utils.safe_connect(action_interpreter, "action_hit_results", self, "_on_action_hit_results")
	Utils.safe_connect(action_interpreter, "action_switch", self, "_on_action_switch")
	Utils.safe_connect(action_interpreter, "action_animate", self, "_on_action_animate")
	Utils.safe_connect(action_interpreter, "no_animation", self, "_on_no_animation")
	Utils.safe_connect(action_interpreter, "display_title", self, "_on_display_title")
	Utils.safe_connect(action_interpreter, "buffer_interpret", self, "_on_buffer_interpret")
	Utils.safe_connect(action_interpreter, "next", self, "_on_next")
	Utils.safe_connect(player_unit_ui, "unit_hovered", self, "_on_unit_hovered")
	Utils.safe_connect(player_unit_ui, "unit_hover_exited", self, "_on_unit_hover_exited")
	Utils.safe_connect(player_unit_ui, "unit_selected", self, "_on_unit_selected")
	Utils.safe_connect(enemy_unit_ui, "unit_hovered", self, "_on_unit_hovered")
	Utils.safe_connect(enemy_unit_ui, "unit_hover_exited", self, "_on_unit_hover_exited")
	Utils.safe_connect(enemy_unit_ui, "unit_selected", self, "_on_unit_selected")
	
	if from_scene:
		_setup_team(PlayerData.team, player_team, true)
		_setup_team([
			BattleUtils.build_monster(load("res://resources/monsters/impl/Wyrmdra.tres"), 5),
			BattleUtils.build_monster(load("res://resources/monsters/impl/Wyrmdra.tres"), 5),
			BattleUtils.build_monster(load("res://resources/monsters/impl/Wyrmdra.tres"), 5),
			BattleUtils.build_monster(load("res://resources/monsters/impl/Wyrmdra.tres"), 5),
			BattleUtils.build_monster(load("res://resources/monsters/impl/Wyrmdra.tres"), 5)
		], enemy_team, false)
	else:
		_setup_team(player_monsters, player_team, true)
		_setup_team(enemy_monsters, enemy_team, false)
	
	_align_teams()
	
	for unit in player_team.display_units:
		turn_order.add(unit)
	
	for unit in enemy_team.display_units:
		turn_order.add(unit)
	
	if !player_team.displays.empty() and !enemy_team.displays.empty():
		_iterate_turn()
	else:
		Utils.go_to_previous()


func _process(delta):
	if buffer_count < buffer:
		buffer_count += 1
	elif !buffer_actions.empty():
		_perform_action()


func _setup_team(monsters, team, player_owned: bool) -> void:
	if monsters is Array and !monsters.empty():
		var units = []
		for monster in monsters:
			if monster is Monster:
				var unit = Unit.new().init(monster)
				unit.player_owned = player_owned
				unit.player_controlled = player_owned
				units.append(unit)
		team.setup(units, player_owned, !npc_battle)


func _align_teams() -> void:
	if enemy_team.displays.size() >= 4:
		player_team.align(4.25, 1.5)
		player_unit_ui.align(4.25, 1.5)
		enemy_team.align(-3.0, 4.0)
		enemy_unit_ui.align(-3.0, 4.0)
	else:
		player_unit_ui.align(3.5, player_team.length)
		enemy_unit_ui.align(-3.5, enemy_team.length)
	
	player_unit_ui.format(player_team.display_units)
	enemy_unit_ui.format(enemy_team.display_units)


func _perform_action():
	match buffer_actions.pop_front():
		BATTLE_AI:
			battle_ai.process()
		INTERPRET:
			action_interpreter.interpret()
		ANIMATE:
			action_animator.animate()
#		FAINT:
#			_faint()
		SKIP_TURN:
			_iterate_turn()
		NEXT:
			buffer_actions.clear()
			_iterate_turn()


func _iterate_turn() -> void:
#	if _is_player_defeated():
#		print("PLAYER DEFEATED. YOU LOSE\n")
#		Utils.go_to_previous()
#	elif _is_enemy_defeated():
#		print("ENEMY DEFEATED. YOU WIN\n")
#		Utils.go_to_previous()

#	_reset_buffer()
	
	var active_unit = turn_order.next()
	turn_order.print_order()
	turn_order_ui.set_turn_order(turn_order)
	
	var status_stacks = []
	for stack in active_unit.statuses:
		stack.decrement_action(active_unit)
		if stack.count > 0:
			status_stacks.append(stack)
	active_unit.statuses = status_stacks
	
	if active_unit.player_owned:
		menu_ui.activate(active_unit, player_team.display_units + enemy_team.display_units, player_team.units)
	else:
		battle_ai.activate(active_unit)
		buffer_actions.append(BATTLE_AI)


func _on_action_selected(action):
	action_interpreter.append(action)
	buffer_actions.append(INTERPRET)


func _on_action_hit_results(results):
	if results is Array:
		for result in results:
			print(result)
			match result:
				"DAMAGE":
					pass
				"STATUS":
					pass
				"HEAL":
					pass
				"RECOIL":
					pass


func _on_action_switch(side, a_unit, b_unit):
	var team = player_team
	var team_ui = player_unit_ui
	if side == "ENEMY":
		team = enemy_team
		team_ui = enemy_unit_ui
	var switched = team.switch(a_unit, b_unit)
	if switched:
		team_ui.switch(a_unit, b_unit)
		turn_order.remove(a_unit)
		turn_order.add(b_unit)
	#	a_unit.charge = 0
		a_unit.stance = BattleUtils.ST_NEU


func _on_action_animate(action):
#	action_animator.setup(action, players, enemies)
#	buffer_actions.append(ANIMATE)
	pass

func _on_next():
	buffer_actions.append(NEXT)


func _on_no_animation():
	_iterate_turn()
#	if !players.defeated_units.empty() or !enemies.defeated_units.empty():
#		buffer_actions.append(FAINT)
#	else:
#		_iterate_turn()


func _on_buffer_interpret() -> void:
	buffer_actions.append(INTERPRET)


func _on_display_title(title):
	if title is String:
		title_ui.display_text(title)


func _on_unit_selected(unit):
	menu_ui.select_unit(unit)


func _on_unit_hovered(unit):
	menu_ui.display_hover_unit(unit)
	for display in player_team.displays:
		if display.unit == unit:
			display.set_flicker(true)
	
	for display in enemy_team.displays:
		if display.unit == unit:
			display.set_flicker(true)


func _on_unit_hover_exited():
	menu_ui.hide_hover_unit()
	for display in player_team.displays:
		display.set_flicker(false)
	
	for display in enemy_team.displays:
		display.set_flicker(false)
