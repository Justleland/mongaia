extends Node
class_name Monster

# Signals for affecting Monster
signal level_changed
signal hp_changed
signal max_hp_changed
signal no_hp

export(Resource) var monsterData

export(String) var monster_name = ""
export(String) var species: String = ""
export (Texture) var texture: Texture setget ,_get_texture
export(int) var level = 5
export(Array, Resource) var skills_known: Array

var experience: int = 0

var ev_hp = 0
var ev_def = 0
var ev_agi = 0
var ev_str = 0
var ev_dex = 0
var ev_int = 0

var fv_hp = 0
var fv_def = 0
var fv_agi = 0
var fv_str = 0
var fv_dex = 0
var fv_int = 0

var max_hp setget ,_get_max_hp
var hp = 1
var def setget ,_get_def
var agi setget ,_get_agi
var strength setget ,_get_str
var dexterity setget ,_get_dex
var intellect setget ,_get_int

var burned_stacks = 0
var frozen_stacks = 0
var poisoned_stacks = 0
var paralyzed_stacks = 0
var confused_stacks = 0

func setup(value: MonsterData) -> Monster:
	if level > 100:
		level = 100
	if value != null:
		monsterData = value
		monster_name = monsterData.species
		hp = self.max_hp
		skills_known = monsterData.build_skills_at_level(level)
		experience = monsterData.calculate_exp_from_level(level)
	return self


# Getters and Setters
func _get_texture() -> Texture:
	if monsterData is MonsterData:
		return monsterData.texture
	return null


func _get_max_hp() -> int:
	if monsterData is MonsterData:
		return _effective_stat(monsterData.base_hp, ev_hp, fv_hp) * 2 + 10
	return 10


func _get_def() -> int:
	if monsterData is MonsterData:
		return _effective_stat(monsterData.base_def, ev_def, fv_def) * 2 + 5
	return 5


func _get_agi() -> int:
	if monsterData is MonsterData:
		var stat = _effective_stat(monsterData.base_agi, ev_agi, fv_agi) + 2
		if stat > 1:
			return stat
	return 1


func _get_str() -> int:
	if monsterData is MonsterData:
		var stat = _effective_stat(monsterData.base_str, ev_str, fv_str) + 2
		if stat > 1:
			return stat
	return 1


func _get_dex() -> int:
	if monsterData is MonsterData:
		var stat = _effective_stat(monsterData.base_dex, ev_dex, fv_dex) + 2
		if stat > 1:
			return stat
	return 1


func _get_int() -> int:
	if monsterData is MonsterData:
		var stat = _effective_stat(monsterData.base_int, ev_int, fv_int) + 2
		if stat > 1:
			return stat
	return 1


func _effective_stat(base: int, ev: int, fv: int) -> int:
	return int((base + min(ev, 4 * fv)) * level / 50)


func subtract_hp(amount: int):
	hp = max(hp - amount, 0)
	emit_signal("hp_changed")
	if hp <= 0:
		burned_stacks = 0
		frozen_stacks = 0
		poisoned_stacks = 0
		paralyzed_stacks = 0
		confused_stacks = 0
		emit_signal("no_hp")


func add_hp(amount: int):
	hp = min(hp + amount, self.max_hp)
	emit_signal("hp_changed")


func add_exp(amount: int):
	experience += amount
	print("exp gained: " + String(amount))
	while monsterData is MonsterData and level < 100 and experience >= monsterData.calculate_exp_from_level(level + 1):
		var previous_hp = _get_max_hp()
		level += 1
		print(monster_name + " gained a level!")
		if level > 100:
			level = 100
			experience = monsterData.calculate_exp_from_level(100)
			break
		else:
			var current_hp = _get_max_hp()
			hp = min(hp + current_hp - previous_hp, current_hp)


func get_multiplier_for(element: Element) -> float:
	if monsterData != null:
		return monsterData.get_family_multiplier_for(element)
	return 1.0
