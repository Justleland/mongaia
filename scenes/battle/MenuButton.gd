extends Button

var value = null

func set_value(value) -> void:
	if value is String:
		text = value
	elif value is SkillChargeCounter:
		text = value.get_skill().skill_name
	elif value is Skill:
		text = value.skill_name
	elif value is Unit:
		text = value.monster.monster_name
	elif value is Monster:
		text = value.monster_name
	
	self.value = value
	visible = _value_is_visible()
	disabled = _value_is_disabled()


func _value_is_visible() -> bool:
	return value is String or value is Skill or value is Unit or \
		value is Monster or value is SkillChargeCounter


func _value_is_disabled() -> bool:
	return value is SkillChargeCounter and !value.is_ready()
