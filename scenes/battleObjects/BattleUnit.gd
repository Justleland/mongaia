extends Node2D

class_name BattleUnit

enum {
	INACTIVE,
	WAIT,
	PREP,
	TARGET,
	CHARGE,
	EXECUTE,
	SWITCH
}

enum Stance {
	NEUTRAL,
	STRENGTH,
	DEXTERITY,
	INTELLECT
}

var monster: Monster setget set_monster
var origin: Vector2

var skill
var target: Array

export var player_control = false
export var catchable = false
var catch_rate = 0.5

var state = INACTIVE
var stance = Stance.NEUTRAL

var wait_timer = 0
var charge_timer = 0
var charge_time = 120

var atk_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var def_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var agi_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var str_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var dex_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var int_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
var dodge_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]

var atk setget ,_get_atk
var def setget ,_get_def
var dodge = 5 setget ,_get_dodge

var strength setget ,_get_str
var dexterity setget ,_get_dex
var intellect setget ,_get_int

var stunned = false

var dot_rate = 120;
var burned_timer = 0;
var poisoned_timer = 0;
var frozen_timer = 0;

signal state_changed
signal state_active
signal state_inactive
signal highlighted
signal unhighlighted
signal monster_KO_EXP(experience)
signal monster_KO
signal faint_switch

signal new_status_condition(condition, stacks)
signal add_burned_stacks(stacks)
signal remove_burned_stacks(stacks)
signal burned_timer
signal add_poisoned_stacks(stacks)
signal remove_poisoned_stacks(stacks)
signal poisoned_timer
signal add_frozen_stacks(stacks)
signal remove_frozen_stacks(stacks)
signal frozen_timer
signal add_paralyzed_stacks(stacks)
signal remove_paralyzed_stacks(stacks)
signal add_confused_stacks(stacks)
signal remove_confused_stacks(stacks)

func _ready():
	$Sprite.texture = null
	origin = position


func increment_timer():
	if monster is Monster:
		_increment_condition_stacks()
		if catchable:
			catch_rate = max(0, catch_rate - 0.0001)
	
	if state == WAIT:
		if stunned:
			wait_timer /= 2
			stunned = false
		wait_timer += 1
		var wait_time = 100 * (100 / (monster.agi * agi_mod))
		if wait_timer >= wait_time:
			wait_timer = 0
			charge_time = monster.skills_known[0].charge_time
			var prep = true
			if _check_paralyzed():
				prep = false
			if _check_confused():
				prep = false
			if prep:
				state = PREP
				emit_signal("state_changed")
				$Sprite.modulate = Color(0, 0, 1)
			
	elif state == CHARGE:
		if stunned:
			charge_timer /= 2
			stunned = false
		charge_timer += 1
		if charge_timer >= charge_time:
			state = EXECUTE
			emit_signal("state_changed")
			charge_timer = 0
			$Sprite.modulate = Color(1, 0, 0)


func _check_paralyzed() -> bool:
	if monster.paralyzed_stacks > 0:
		monster.paralyzed_stacks -= 1
		emit_signal("remove_paralyzed_stacks", 1)
		return randf() > 0.5
	return false


func _check_confused() -> bool:
	if monster.confused_stacks > 0:
		monster.confused_stacks -= 1
		emit_signal("remove_confused_stacks", 1)
		var confused = randf() > 0.5
		if confused:
			# replace with confused attack damage
# warning-ignore:narrowing_conversion
			monster.subtract_hp(max(1, monster.max_hp * (1.0 / 32.0)))
			return true
	return false


func _increment_condition_stacks():
	var dmg = 0.0
	var spacing = 0
	
	if monster.burned_stacks > 0:
		burned_timer += 1
		if burned_timer % 30 == 0:
			emit_signal("burned_timer")
		if burned_timer >= dot_rate:
			monster.burned_stacks -= 1
			emit_signal("remove_burned_stacks", 1)
			burned_timer = 0
			var burned_dmg = max(1, monster.max_hp * (1.0 / 16.0))
			dmg += burned_dmg
			BattleUtils.load_counter() \
						.attach(self) \
						.set_damage(burned_dmg) \
						.tint(Color.red)
			spacing -= 30.0
	
	if monster.frozen_stacks > 0:
		frozen_timer += 1
		if frozen_timer % 30 == 0:
			emit_signal("frozen_timer")
		if frozen_timer >= dot_rate:
			monster.frozen_stacks -= 1
			emit_signal("remove_frozen_stacks", 1)
			frozen_timer = 0
			var frozen_dmg = max(1, monster.max_hp * (1.0 / 16.0))
			dmg += frozen_dmg
			BattleUtils.load_counter() \
						.attach(self) \
						.set_location(Vector2(0.0, spacing)) \
						.set_damage(frozen_dmg) \
						.tint(Color.cyan)
			spacing -= 30.0
	
	if monster.poisoned_stacks > 0:
		poisoned_timer += 1
		if poisoned_timer % 30 == 0:
			emit_signal("poisoned_timer")
		if poisoned_timer >= dot_rate:
			monster.poisoned_stacks -= 1
			emit_signal("remove_poisoned_stacks", 1)
			poisoned_timer = 0
			var poisoned_dmg = max(1, monster.max_hp * (1.0 / 12.0))
			dmg += poisoned_dmg
			BattleUtils.load_counter() \
						.attach(self) \
						.set_location(Vector2(0.0, spacing)) \
						.set_damage(poisoned_dmg) \
						.tint(Color.purple)
	
	if dmg > 0.0:
		monster.subtract_hp(dmg)


func add_condition_stacks(condition, stacks: int) -> void:
	if monster is Monster:
		# Bug doesn't allow two classes to reference each other
		# Skill will have to stay as ints until a 4.0 release
		# https://github.com/godotengine/godot/issues/27136
		match condition:
			4:
				
				if monster.burned_stacks <= 0:
					emit_signal("new_status_condition", "burned", stacks)
				else:
					emit_signal("add_burned_stacks", stacks)
				monster.burned_stacks += stacks
			3:
				if monster.frozen_stacks <= 0:
					emit_signal("new_status_condition", "frozen", stacks)
				else:
					emit_signal("add_frozen_stacks", stacks)
				monster.frozen_stacks += stacks
			2:
				if monster.poisoned_stacks <= 0:
					emit_signal("new_status_condition", "poisoned", stacks)
				else:
					emit_signal("add_poisoned_stacks", stacks)
				monster.poisoned_stacks += stacks
			1:
				if monster.paralyzed_stacks <= 0:
					emit_signal("new_status_condition", "paralyzed", stacks)
				else:
					emit_signal("add_paralyzed_stacks", stacks)
				monster.paralyzed_stacks += stacks
			6:
				if monster.confused_stacks <= 0:
					emit_signal("new_status_condition", "confused", stacks)
				else:
					emit_signal("add_confused_stacks", stacks)
				monster.confused_stacks += stacks
			5:
				stunned = true
				emit_signal("new_status_condition", "stunned", 0)
				print(monster.monster_name + " was stunned!\n")


func _set_sprite_color(red: int, green: int, blue: int):
	$Sprite.modulate = Color(red, green, blue)


func _is_state_prep():
	return state == PREP


func mod_stat(stat: String, change: int):
	match stat:
		"atk":
			atk_mod = BattleUtils.modulate_stage(atk_mod, change)
		"def":
			def_mod = BattleUtils.modulate_stage(def_mod, change)
		"agi":
			agi_mod = BattleUtils.modulate_stage(agi_mod, change)
		"str":
			str_mod = BattleUtils.modulate_stage(str_mod, change)
		"dex":
			dex_mod = BattleUtils.modulate_stage(dex_mod, change)
		"int":
			int_mod = BattleUtils.modulate_stage(int_mod, change)


func set_monster(value: Monster) -> void:
	if monster != null:
		Utils.safe_disconnect(monster, "no_hp", self, "_on_monster_no_hp")
		monster = null
		$Sprite.texture = null
		state = INACTIVE
	
	if value != null:
		monster = value
		$Sprite.texture = monster.texture
		wait_timer = 0
		charge_timer = 0
		Utils.safe_connect(monster, "no_hp", self, "_on_monster_no_hp")
		state = WAIT
		stance = Stance.NEUTRAL
		atk_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		def_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		agi_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		str_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		dex_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		int_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
		dodge_mod = BattleUtils.stage_value[BattleUtils.Stage.ZERO]
	
	if monster is Monster:
		emit_signal("state_active")
	else:
		emit_signal("state_inactive")


func _get_atk() -> int:
	return monster.atk * atk_mod * (1.0 - (min(0.25, (1.0 / 16.0) * monster.burned_stacks)))


func _get_def() -> int:
	return monster.def * def_mod * (1.0 - (min(0.25, (1.0 / 16.0) * monster.frozen_stacks)))


func _get_dodge() -> float:
	return (float(dodge) / 100.0) * dodge_mod


func _get_str() -> int:
	return monster.strength * str_mod * _get_stance_mod(Stance.STRENGTH, stance)


func get_str(stance_type: String = "current") -> int:
	return monster.strength * _get_projected_stance_stat_mod(Stance.STRENGTH, stance_type)


func _get_dex() -> int:
	return monster.dexterity * dex_mod * _get_stance_mod(Stance.DEXTERITY, stance)


func get_dex(stance_type: String = "current") -> int:
	return monster.dexterity * _get_projected_stance_stat_mod(Stance.DEXTERITY, stance_type)


func _get_int() -> int:
	return monster.intellect * int_mod * _get_stance_mod(Stance.INTELLECT, stance)


func get_int(stance_type: String = "current") -> int:
	return monster.intellect * _get_projected_stance_stat_mod(Stance.INTELLECT, stance_type)


func get_stance_stat() -> int:
	match stance:
		Stance.STRENGTH:
			return _get_str()
		Stance.DEXTERITY:
			return _get_dex()
		Stance.INTELLECT:
			return _get_int()
		Stance.NEUTRAL:
			return (_get_str() + _get_dex() + _get_int()) / 3
		_:
			return 5


func _get_projected_stance_stat_mod(expected, projected) -> float:
	if projected == "current":
		return _get_stance_mod(expected, stance)
	elif projected == "Neutral":
		return _get_stance_mod(expected, Stance.NEUTRAL)
	elif projected == "Strength":
		return _get_stance_mod(expected, Stance.STRENGTH)
	elif projected == "Dexterity":
		return _get_stance_mod(expected, Stance.DEXTERITY)
	elif projected == "Intellect":
		return _get_stance_mod(expected, Stance.INTELLECT)
	return 0.0

func _get_stance_mod(expected, actual) -> float:
	if actual == expected:
		return 1.0
	if actual == Stance.NEUTRAL:
		return 0.5
	return 0.33


func _on_monster_no_hp():
	if not player_control:
		emit_signal("monster_KO_EXP", (monster.monsterData.exp_value * monster.level) / 7)
		if catchable and BattleUtils.caught_monster == null and randf() < 0.25:
			BattleUtils.caught_monster = monster
			print("caught")
		state = INACTIVE
	self.monster = null
	
	if player_control:
		state = SWITCH
		emit_signal("faint_switch")
	
	emit_signal("monster_KO")


func highlight():
	emit_signal("highlighted")


func unhighlight():
	emit_signal("unhighlighted")
