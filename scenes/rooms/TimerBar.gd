extends Control

onready var timerMarkers = [
	$Indicator0,
	$Indicator1,
	$Indicator2,
	$Indicator3,
	$Indicator4,
	$Indicator5,
	$Indicator6,
	$Indicator7
]

func add_battle_unit(unit: BattleUnit, red: bool):
	if unit is BattleUnit:
		for indicator in timerMarkers:
			if indicator.unit == null:
				indicator.add_unit(unit, red)
				break
