extends Reference
class_name ActionInterpreter

signal action_hit_results(results)
signal action_switch(side, a_unit, b_unit)
signal action_animate(action)
signal no_animation
signal display_title(text)
signal buffer_interpret
signal next

var actions = []

func append(action: Dictionary) -> void:
	if action != null:
		actions.append(action)

func interpret():
	if !actions.empty():
		_interpret(actions.pop_front())


func _interpret(action: Dictionary) -> void:
	match action["type"]:
		"SKILL":
			_skill(action["skill"], action["acting"], action["targets"])
		"HIT_RESULTS":
			emit_signal("action_hit_results", action["results"])
		"STANCE":
			_stance(action["acting"])
		"SWITCH":
			_switch(action)
		"SWITCH_ACTION":
			emit_signal("action_switch", action["side"], action["acting"], action["switch_to"])
		"PASS":
			_pass_turn(action["acting"])

func _pass_turn(_unit: Unit) -> void:
#	if unit is Unit:
#		unit.pass_turn()
	emit_signal("display_title", "Pass")
	emit_signal("no_animation")


func _skill(skill: Skill, acting: Unit, targets: Array) -> void:
	emit_signal("display_title", skill.skill_name)
	
	# before all
	
	# before each
	
	# during hit
	_buffer_action({
		"type": "HIT_RESULTS",
		"results": skill.result(acting, targets)
	})
	
	# after each
	
	# after all
	
	emit_signal("next")
#	if skill is SkillDamaging and !target.empty():
#		var format = {
#			"type": "SKILL",
#			"name": skill.skill_name,
#			"hit_count": skill.hit_count,
#			"charge": skill.get_e_charge(acting),
#			"acting": acting
#		}
#		format["results"] = skill.result(acting, target)
#		emit_signal("action_animate", format)
#	else:
#		pass


func _stance(acting: Unit) -> void:
	emit_signal("display_title", "Change Stance: " + acting.stance)
	emit_signal("no_animation")


func _switch(action) -> void:
	emit_signal("action_animate", {
		"type": "SWITCH_OUT",
		"unit": action["acting"],
		"display": null
	})
	
	var switch_action = action.duplicate()
	switch_action["type"] = "SWITCH_ACTION"
	append(switch_action)
	emit_signal("buffer_interpret")
	
	emit_signal("action_animate", {
		"type": "SWITCH_IN",
		"unit": action["switch_to"],
		"display": null
	})
	
	emit_signal("next")


func _buffer_action(action: Dictionary) -> void:
	if action != null:
		append(action)
		emit_signal("buffer_interpret")
