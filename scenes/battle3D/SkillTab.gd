extends HBoxContainer

signal tab_action(dict)

var skill: Skill = null
var targets = []

onready var power = $Stats/Grid/Power
onready var crit = $Stats/Grid/Crit

func _ready():
	visible = false


func activate(_skill: Skill, _targets: Array) -> void:
	visible = true
	skill = _skill
	
	if skill != null:
		for target in targets:
			target.emit_signal("targeted", false)
		for target in _targets:
			target.emit_signal("targeted", true)
		targets = _targets
		
		$Stats/ElementType.text = skill.element_type_text()
		$Stats/Grid/TargetType.text = skill.target_type_text()
		$Stats/Grid/Acc.text = String(skill.get_acc_num())
		$SkillInfo/SkillName.text = skill.skill_name
		$SkillInfo/Description.text = skill.description
		
		if skill is SkillDamaging:
			power.text = String(skill.power)
			crit.text = String(skill.crit)
		else:
			power.text = "---"
			crit.text = "---"


func deactivate() -> void:
	skill = null
	for target in targets:
		target.emit_signal("targeted", false)
	targets = []
	visible = false


func get_targets(unit: Unit) -> Array:
	if is_active() and unit != null and targets.has(unit):
		if skill.target.multi:
			return targets
		return [unit]
	return []


func is_active() -> bool:
	return skill != null
