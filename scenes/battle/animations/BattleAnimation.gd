extends AnimatedSprite
class_name BattleAnimation

var acting: Unit = null
var target: Unit = null
var skill_data: Dictionary = {}
var finished = false




func setup(skill_data: Dictionary) -> void:
	self.skill_data = skill_data
	play()


func complete_skill() -> void:
	finished = true
	pass
