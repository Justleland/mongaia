extends Object

class_name Damage

var dmg: int = 0
var recoil: int = 0
var hp_steal: int = 0
var hit: bool = false
var crit: bool = false
var effectiveness: float
var status_effect = false

func print_values():
	if !hit:
		print("miss")
	else:
		print("Effectiveness: " + String(effectiveness))
		print("Damage: " + String(dmg))
		if recoil > 0:
			print("Recoil: " + String(recoil))
		if hp_steal > 0: 
			print("HP Steal: " + String(hp_steal))
		if crit:
			print("Critical Hit")
		if status_effect:
			print("Added Status Effect")
	print()

