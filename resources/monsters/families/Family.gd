extends Resource
class_name Family

enum Effect {
	NEUTRAL
	WEAK
	RESIST
	IMMUNE
}

export(String) var text = ""

export(Effect) var neutral
export(Effect) var fire
export(Effect) var ice
export(Effect) var elec
export(Effect) var wind
export(Effect) var earth
export(Effect) var water
export(Effect) var nature
export(Effect) var poison
export(Effect) var metal
export(Effect) var psi
export(Effect) var martial
export(Effect) var light
export(Effect) var shadow

func _get_effect(element: String):
	match element:
		"NEUTRAL":
			return neutral
		"FIRE":
			return fire
		"ICE":
			return ice
		"ELEC":
			return elec
		"WIND":
			return wind
		"EARTH":
			return earth
		"WATER":
			return water
		"NATURE":
			return nature
		"POISON":
			return poison
		"METAL":
			return metal
		"PSI":
			return psi
		"MARTIAL":
			return martial
		"LIGHT":
			return light
		"SHADOW":
			return shadow
	return Effect.NEUTRAL


func get_effect(element: String) -> String:
	var effect = _get_effect(element)
	match effect:
		Effect.NEUTRAL:
			return "NEUTRAL"
		Effect.WEAK:
			return "WEAK"
		Effect.RESIST:
			return "RESIST"
		Effect.IMMUNE:
			return "IMMUNE"
	return "ERROR"


func get_multiplier(element: String) -> float:
	var effect = _get_effect(element)
	match effect:
		Effect.NEUTRAL:
			return 1.0
		Effect.WEAK:
			return 2.0
		Effect.RESIST:
			return 0.5
		Effect.IMMUNE:
			return 0.0
	return 1.0
