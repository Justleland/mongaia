extends Button
class_name BattleTabButton

const SKILL = "SKILL"
const STANCE = "STANCE"
const ITEM = "ITEM"
const SWITCH = "SWITCH"
const PASS = "PASS"
const RUN = "RUN"

export(String) var value = ""

func _ready():
	match value:
		SKILL:
			return
		STANCE:
			return
		ITEM:
			return
		SWITCH:
			return
		PASS:
			return
		RUN:
			return
		_:
			Utils.log_print("Button Set Up Incorrectly. Value: " + value)
			disabled = true

