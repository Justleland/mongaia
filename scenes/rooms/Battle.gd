extends Node


onready var players = [
	$Players/PlayerUnit0,
	$Players/PlayerUnit1,
	$Players/PlayerUnit2
]

onready var playerUIs = [
	$UI/PlayerBufferBox/PlayerUI/InfoListUI/PlayerUnitInfoUI0,
	$UI/PlayerBufferBox/PlayerUI/InfoListUI/PlayerUnitInfoUI1,
	$UI/PlayerBufferBox/PlayerUI/InfoListUI/PlayerUnitInfoUI2
]

onready var enemies = [
	$Enemies/EnemyUnit0,
	$Enemies/EnemyUnit1,
	$Enemies/EnemyUnit2,
	$Enemies/EnemyUnit3,
	$Enemies/EnemyUnit4
]

onready var enemyUIs = [
	$UI/EnemyBufferBox/EnemyUI/InfoListUI/EnemyUnitInfoUI0,
	$UI/EnemyBufferBox/EnemyUI/InfoListUI/EnemyUnitInfoUI1,
	$UI/EnemyBufferBox/EnemyUI/InfoListUI/EnemyUnitInfoUI2,
	$UI/EnemyBufferBox/EnemyUI/InfoListUI/EnemyUnitInfoUI3,
	$UI/EnemyBufferBox/EnemyUI/InfoListUI/EnemyUnitInfoUI4
]

onready var menu_ui = $UI/MenuUI

enum {
	TIMER,
	PREP,
	MENU,
	TARGET,
	SKILL_ANIMATE,
	EXECUTE,
	FAINT_SWITCH
	VICTORY,
	POLL,
	PAUSE
}

var state = TIMER
var unit_in_menu: BattleUnit = null
var unit_in_animation: BattleUnit = null

var exp_earned = 0

onready var skill_animator = $SkillAnimator

# Called when the node enters the scene tree for the first time.
# Builds the battle from the Global Player Data
func _ready():
	if PlayerData.team.empty() or BattleUtils.encounter_monsters.empty():
		get_tree().change_scene(PlayerData.previous_area)
	
	for i in range(players.size()):
		var player = players[i]
		$UI/TimerBar.add_battle_unit(player, false)
		playerUIs[i].set_unit(player)
	
	var placement = 0
	for monster in PlayerData.team:
		if placement < 3 and monster is Monster and monster.hp > 0:
			players[placement].monster = monster
			playerUIs[placement].start_visible_alpha()
			placement += 1
	
	for i in range(enemies.size()):
		var enemy = enemies[i]
		$UI/TimerBar.add_battle_unit(enemy, true)
		enemyUIs[i].set_unit(enemy)
	
	for i in range(min(BattleUtils.encounter_monsters.size(), enemies.size())):
		if BattleUtils.encounter_monsters[i] is Monster:
			enemies[i].monster = BattleUtils.encounter_monsters[i]
			enemyUIs[i].start_visible_alpha()
	
	BattleUtils.clear_encounter_monster()


# Handles the current state of the battle
func _process(delta):
	match state:
		TIMER:
			increment_timer()
		PREP:
			set_up_prep()
		MENU:
			pass
		EXECUTE:
			execute_skill()
		POLL:
			poll_unit_state()
		SKILL_ANIMATE:
			_check_animation()
		FAINT_SWITCH:
			pass
#		VICTORY:
#			_exit_battle()


func poll_unit_state():
	state = TIMER
	for player in players:
#		print("Player state: " + String(player.state))
		if player.state == BattleUnit.SWITCH:
			state = FAINT_SWITCH
			menu_ui.switch(player, players)
			return
		if state != EXECUTE:
			if state != TARGET and player.state == player.PREP:
				state = PREP
			elif player.state == BattleUnit.TARGET:
				state = TARGET
			elif player.state == player.EXECUTE:
				state = EXECUTE
#	print("after players: " + String(state))
	
	for enemy in enemies:
#		print("Player state: " + String(enemy.state))
		if state != EXECUTE and enemy.state == enemy.PREP:
			state = PREP
		elif enemy.state == enemy.EXECUTE:
			state = EXECUTE
#	print("after enemies: " + String(state))


func _check_animation():
	if !skill_animator.is_animating():
		state = POLL


func increment_timer():
	for player in players:
		player.increment_timer()
	
	for enemy in enemies:
		enemy.increment_timer()


func set_up_prep():
	var menu_ready_unit = null
	for player in players:
		if player.state == player.PREP and menu_ready_unit == null:
			# $MenuUI/MoveMenu.set_unit(player)
			menu_ready_unit = player
	
	for enemy in enemies:
		if enemy.state == enemy.PREP:
			enemy.monster.add_sp(3)
			if enemy.monster.skills_known[0].sp_cost <= enemy.monster.sp:
				enemy.monster.subtract_sp(enemy.monster.skills_known[0].sp_cost) 
				enemy.state = enemy.CHARGE
				enemy._set_sprite_color(1, 0, 1)
			else:
				enemy.state = enemy.WAIT
			
	if menu_ready_unit is BattleUnit:
		menu_ready_unit.monster.add_sp(3)
		var skills = menu_ready_unit.monster.skills_known
		var can_attack = false
		for skill in skills:
			if skill.sp_cost <= menu_ready_unit.monster.sp:
				can_attack = true
		if can_attack:
			state = MENU
			menu_ui.initiate(menu_ready_unit, enemies, players)
		else:
			menu_ready_unit.state = BattleUnit.WAIT
			_on_set_action_title("Not Enough SP")
			poll_unit_state()
	else: 
		poll_unit_state()


func execute_skill():
	var current_executer = null
	for player in players:
		if player.state == player.EXECUTE and current_executer == null:
			# Changed target to an array. 
			# Need to call _manage_move
			var valid_targets = false
			for enemy in player.target:
				if enemy.monster is Monster:
					valid_targets = true
					break
			
			if not valid_targets:
				var enemy = enemies[randi() % enemies.size()]
				while enemy.monster == null:
					enemy = enemies[randi() % enemies.size()]
				player.target.append(enemy)
			
			for enemy in player.target:
				if enemy.monster != null:
					skill_animator.setup_animation(player, enemy, player.skill)
					current_executer = player
					state = SKILL_ANIMATE
					player._set_sprite_color(1, 1, 1)
			
	for enemy in enemies:
		if enemy.state == enemy.EXECUTE and current_executer == null:
			var player = players[randi() % players.size()]
			if player.monster != null:
				enemy.skill = enemy.monster.skills_known[0]
				skill_animator.setup_animation(enemy, player, enemy.skill)
				enemy._set_sprite_color(1, 1, 1)
				state = SKILL_ANIMATE
				current_executer = enemy
	
	_check_animation()


func _check_for_team_KO(team: Array) -> bool:
	var count_KO = 0
	for unit in team:
		if unit.monster == null:
			count_KO += 1
	
	return count_KO == team.size()


func _on_battle_unit_state_changed():
	state = POLL


func _on_battle_unit_monster_KO() -> void:
	# Can implement some sort of monster switching on KO
	if PlayerData.is_team_fainted():
		BattleUtils.caught_monster = null
		get_tree().change_scene("res://scenes/rooms/LoseRoom.tscn")
	
	if _check_for_team_KO(enemies):
#		if BattleUtils.caught_monster is Monster and PlayerData.team.size() < 6:
#			PlayerData.team.append(BattleUtils.caught_monster)
#		BattleUtils.caught_monster = null
		var victory_UI = load("res://scenes/battleObjects/VictoryUI.tscn").instance()
		add_child(victory_UI)
		Utils.safe_connect(victory_UI, "complete", self, "_on_VictoryUI_complete")
		victory_UI.gain_exp(exp_earned)
		state = PAUSE
		$UI.visible = false
		
#		for monster in PlayerData.team:
#			if monster is Monster:
#				if monster.hp > 0:
#					monster.add_exp(exp_earned)
#				else:
#					monster.hp = monster.max_hp
#				monster.sp = monster.max_sp
#		get_tree().change_scene(PlayerData.previous_area)


func _on_enemy_unit_monster_KO_EXP(experience) -> void:
	if experience != null:
		exp_earned += experience


func _on_player_unit_faint_switch() -> void:
	for player in players:
		if player.state == BattleUnit.SWITCH:
			state = FAINT_SWITCH
			menu_ui.switch(player, players)


func _on_set_action_title(text):
	$UI/ActionTitleUI.display_text(text)


func _on_MenuUI_menu_closed():
	poll_unit_state()


func _on_VictoryUI_complete():
	for monster in PlayerData.team:
		if monster is Monster:
			if monster.hp > 0:
				pass
			else:
				monster.hp = monster.max_hp
			monster.sp = monster.max_sp
	if BattleUtils.caught_monster is Monster and PlayerData.team.size() < 6:
		PlayerData.team.append(BattleUtils.caught_monster)
	BattleUtils.caught_monster = null
	get_tree().change_scene(PlayerData.previous_area)


#func _exit_battle():
#	if Input.is_action_just_released("ui_accept"):
#		for monster in PlayerData.team:
#			if monster is Monster:
#				if monster.hp > 0:
#					pass
##					monster.add_exp(exp_earned)
#				else:
#					monster.hp = monster.max_hp
#				monster.sp = monster.max_sp
#		if BattleUtils.caught_monster is Monster and PlayerData.team.size() < 6:
#			PlayerData.team.append(BattleUtils.caught_monster)
#		BattleUtils.caught_monster = null
#		get_tree().change_scene(PlayerData.previous_area)

