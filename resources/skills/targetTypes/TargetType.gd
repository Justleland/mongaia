extends Resource
class_name TargetType

export var ally = false
export var multi = false
export var self_target = true

func display_text() -> String:
	if ally:
		if multi:
			if self_target:
				return "All Allies and Self"
			return "All Allies"
		return "Ally"
	if multi:
		return "All Enemies"
	if self_target:
		return "Self"
	return "Enemy"


func is_valid_target(target: Unit, acting: Unit) -> bool:
	if target == null or acting == null:
		return false
	
	var is_self = target == acting
	var is_ally = false
	
	if multi:
		is_ally = target.player_owned == acting.player_owned
	else:
		is_ally = target.player_controlled == acting.player_controlled or \
			target.player_owned == acting.player_owned
	
	return _targetable(is_self, is_ally)


func _targetable(is_self: bool, is_ally:bool):
	if ally and is_self:
		return self_target
	if ally:
		return is_ally
	return !is_ally

