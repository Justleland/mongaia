extends Node

var team: Array = []

var load_pos: Vector2 = Vector2(1000,1000)
var previous_area: String = ""

func _ready():
	var monster = load("res://resources/monsters/Monster.gd")
	var monsterData = [
		load("res://resources/monsters/impl/Wyrmdra.tres"),
		load("res://resources/monsters/impl/Cherum.tres"),
		load("res://resources/monsters/impl/Impo.tres"),
		load("res://resources/monsters/impl/Spribug.tres"),
		load("res://resources/monsters/impl/Cherum.tres"),
		load("res://resources/monsters/impl/Stoneward.tres")
	]
	for data in monsterData:
		var monster_instance = monster.new()
		monster_instance.level = 15
		team.append(monster_instance.setup(data))

func is_team_fainted() -> bool:
	var fainted = true
	for monster in team:
		if monster.hp > 0:
			fainted = false
			break
	return fainted
