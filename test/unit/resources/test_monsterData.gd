extends GutTest

var monster_data = load("res://resources/monsters/MonsterData.gd")

func test_fast_exp():
	var exp_rate = MonsterData.EXP_Rate.FAST
	var expected = {
		1: 0,
		12: 1382,
		25: 12500,
		50: 100000,
		100: 800000
	}
	
	_assert_rate(exp_rate, expected)


func test_medium_exp():
	var exp_rate = MonsterData.EXP_Rate.MEDIUM
	var expected = {
		1: 0,
		12: 1728,
		25: 15625,
		50: 125000,
		100: 1000000
	}
	
	_assert_rate(exp_rate, expected)


func test_medium_slow_exp():
	var exp_rate = MonsterData.EXP_Rate.MEDIUM_SLOW
	var expected = {
		1: 0,
		12: 973,
		25: 11735,
		50: 117360,
		100: 1059860
	}
	
	_assert_rate(exp_rate, expected)


func test_slow_exp():
	var exp_rate = MonsterData.EXP_Rate.SLOW
	var expected = {
		1: 0,
		12: 2160,
		25: 19531,
		50: 156250,
		100: 1250000
	}
	
	_assert_rate(exp_rate, expected)


func test_build_skills_at_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4"] }'
	
	var expected = [
		load("res://resources/skills/impl/Test1.tres"),
		load("res://resources/skills/impl/Test2.tres"),
		load("res://resources/skills/impl/Test3.tres"),
		load("res://resources/skills/impl/Test4.tres"),
	]
	
	var actual = data.build_skills_at_level(10)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 4)
	for skill in actual:
		assert_true(expected.has(skill))


func test_build_only_2_skills_at_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4"] }'
	
	var expected = [
		load("res://resources/skills/impl/Test1.tres"),
		load("res://resources/skills/impl/Test2.tres"),
	]
	
	var actual = data.build_skills_at_level(3)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 2)
	for skill in actual:
		assert_true(expected.has(skill))


func test_build_max_skills_at_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4", "Test5"], "12": ["Test6"],  }'
	
	var expected = [
		load("res://resources/skills/impl/Test3.tres"),
		load("res://resources/skills/impl/Test4.tres"),
		load("res://resources/skills/impl/Test5.tres"),
		load("res://resources/skills/impl/Test6.tres"),
	]
	
	var actual = data.build_skills_at_level(12)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 4)
	for skill in actual:
		assert_true(expected.has(skill))


func test_monster_data_has_minimun_skills():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4"] }'
	
	var expected = [
		load("res://resources/skills/impl/Test1.tres"),
	]
	
	var actual = data.build_skills_at_level(0)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 1)
	for skill in actual:
		assert_true(expected.has(skill))


func test_impls_have_base_skills():
	var dir = Directory.new()
	if dir.open("res://resources/monsters/impl/") == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if !dir.current_is_dir():
				print(dir.get_current_dir() + "/" + file_name)
				var data = load(dir.get_current_dir() + "/" +file_name)
				var skills = data.build_skills_at_level(0)
				
				assert_true(skills is Array)
				assert_true(!skills.empty())
			file_name = dir.get_next()
		dir.list_dir_end()
	else:
		fail_test("Unable to access Directory")


func test_get_skills_from_single_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4", "Test5"], "12": ["Test6"],  }'
	
	var expected = [
		load("res://resources/skills/impl/Test3.tres"),
	]
	
	var actual = data.get_skills_from_level(5)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 1)
	for skill in actual:
		assert_true(expected.has(skill))


func test_get_skills_from_multiple_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4", "Test5"], "12": ["Test6"],  }'
	
	var expected = [
		load("res://resources/skills/impl/Test3.tres"),
		load("res://resources/skills/impl/Test4.tres"),
		load("res://resources/skills/impl/Test5.tres"),
	]
	
	var actual = data.get_skills_from_level(5, 10)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 3)
	for skill in actual:
		assert_true(expected.has(skill))


func test_get_skills_from_level():
	var data = monster_data.new()
	data.skill_config = '{ "0": ["Test1"], "2": ["Test2"], "5": ["Test3"], "10": ["Test4", "Test5"], "12": ["Test6"],  }'
	
	var expected = [
		load("res://resources/skills/impl/Test1.tres"),
		load("res://resources/skills/impl/Test2.tres"),
		load("res://resources/skills/impl/Test3.tres"),
		load("res://resources/skills/impl/Test4.tres"),
		load("res://resources/skills/impl/Test5.tres"),
		load("res://resources/skills/impl/Test6.tres"),
	]
	
	var actual = data.get_skills_from_level(0, 12)
	
	assert_true(actual is Array)
	assert_true(actual.size() == 6)
	for skill in actual:
		assert_true(expected.has(skill))


func _assert_rate(exp_rate, expected: Dictionary) -> void:
	var data = monster_data.new()
	data.set_exp_rate(exp_rate)
	
	for i in expected:
		assert_eq(data.calculate_exp_from_level(i), expected[i])
