extends Object

class_name SkillResult

var is_damaging: bool = false
var is_healing
var hit: bool
var crit: bool
var effectiveness: float
var dmg: int = 0
var recoil: int = 0
var hp_steal: int = 0
var healing: int = 0
# Status object placeholer
var target_buff_stats: Array
var target_buff_amount: int
var target_debuff_stats: Array
var target_debuff_amount: int
var self_buff_stats: Array
var self_buff_amount: int
var self_debuff_stats: Array
var self_debuff_amount: int

