extends VBoxContainer

var monster = null
var active = false
var complete = true

var display_lvl
var current_lvl
var current_max_hp
var current_max_sp
var current_atk
var current_def
var current_agi
var current_str
var current_dex
var current_int

var current_lvl_exp
var next_lvl_exp
var current_exp
var exp_gained = 0

signal complete


func _ready():
	$Margin/LevelUp.modulate = Color(1.0, 1.0, 1.0, 0.0)
	visible = false


func _process(delta):
	_add_exp_gained()

func setup(value: Monster) -> void:
	if value is Monster:
		monster = value
		visible = true
		if monster.hp > 0:
			active = true
			complete = false
		current_lvl = monster.level
		display_lvl = monster.level
		current_max_hp = monster.max_hp
		current_max_sp = monster.max_sp
		current_atk = monster.atk
		current_def = monster.def
		current_agi = monster.agi
		current_str = monster.strength
		current_dex = monster.dexterity
		current_int = monster.intellect
		current_lvl_exp = monster.monsterData.calculate_exp_from_level(monster.level)
		next_lvl_exp = monster.monsterData.calculate_exp_from_level(monster.level + 1)
		current_exp = monster.experience
		
		$Panel/Margin/VBox/HBox/Name/Label.text = monster.monster_name
		$Panel/Margin/VBox/HBox/LevelNum.text = String(current_lvl)
		_update_bar()


func gain_exp(amount: int) -> void:
	exp_gained = amount


func _add_exp_gained() -> void:
	if active and exp_gained > 0:
		exp_gained -= 1
		current_exp += 1
		if current_exp >= next_lvl_exp:
			$Margin/LevelUp.modulate = Color(1.0, 1.0, 1.0, 1.0)
			display_lvl += 1
			current_lvl_exp = next_lvl_exp
			next_lvl_exp = monster.monsterData.calculate_exp_from_level(display_lvl + 1)
			$Panel/Margin/VBox/HBox/LevelNum.text = String(display_lvl)
		
		_update_bar()
		
		if exp_gained <= 0:
			complete = true
			emit_signal("complete")


func _update_bar() -> void:
	$Panel/Margin/VBox/Exp/ProgressBar.value = 100 * (current_exp - current_lvl_exp) / (next_lvl_exp - current_lvl_exp)
