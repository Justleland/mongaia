extends Node
class_name SkillAnimator

var source = load("res://scenes/battleObjects/skillAnimationImpl/SourceAnimation.gd")
var target = load("res://scenes/battleObjects/skillAnimationImpl/TargetAnimation.gd")
var source_to_target = load("res://scenes/battleObjects/skillAnimationImpl/SourceToTargetAnimation.gd")
var target_to_source = load("res://scenes/battleObjects/skillAnimationImpl/TargetToSourceAnimation.gd")

var animations: Array = []

signal set_action_title(text)

func setup_animation(source: BattleUnit, target: BattleUnit, skill: Skill) -> void:
	_add_skill_animation(_make_animation(skill).setup(source, target, skill))
	if skill is Skill:
		emit_signal("set_action_title", skill.skill_name)


func _make_animation(skill) -> SkillAnimation:
	if skill.frames == null:
		return source.new()
	match skill.animation_type:
		Skill.AnimationType.SOURCE:
			return source.new()
		Skill.AnimationType.TARGET:
			return target.new()
		Skill.AnimationType.SOURCE_TO_TARGET:
			return source_to_target.new()
		Skill.AnimationType.TARGET_TO_SOURCE:
			return target_to_source.new()
		_:
			return source.new()

func _add_skill_animation(animation: SkillAnimation) -> void:
	if animation is SkillAnimation:
		self.add_child(animation)
		animations.append(animation)
		Utils.safe_connect(animation, "finished_animating", self, "_on_finished_animating")
		Utils.safe_connect(animation, "process_skill", self, "_on_process_skill")


func _on_finished_animating() -> void:
	var unfinished = []
	
	for animation in animations:
		if !animation.is_finished():
			unfinished.append(animation)
		else:
			self.remove_child(animation)
			Utils.safe_disconnect(animation, "finished_animating", self, "_on_finished_animating")
			Utils.safe_disconnect(animation, "process_skill", self, "_on_process_skill")
	
	animations = unfinished


func is_animating() -> bool:
	return !animations.empty()


func _on_process_skill(unit: BattleUnit, target: BattleUnit):
	var skill = unit.skill
	
	if skill is SkillDamaging:
		var count = 0
		var status_effect = false
		while target.monster is Monster and unit.monster is Monster and count < skill.hit_count:
			count += 1
			var damage = skill.get_damage_result(unit, target)
			BattleUtils.load_counter() \
					.attach(self) \
					.set_location(target.position) \
					.set_damage(damage.dmg) \
					.set_crit(damage.crit) \
					.set_miss(damage.hit) \
					.set_tint(damage.effectiveness)
			target.monster.subtract_hp(damage.dmg)
			if damage.recoil > 0:
				unit.monster.subtract_hp(damage.recoil)
				BattleUtils.load_counter() \
						.attach(self) \
						.set_location(unit.position) \
						.set_damage(damage.recoil)
			if damage.hp_steal > 0 and unit.monster is Monster:
				BattleUtils.load_counter() \
						.attach(self) \
						.set_location(unit.position) \
						.set_damage(damage.hp_steal) \
						.tint_green()
				unit.monster.add_hp(damage.hp_steal)
			if !status_effect:
				status_effect = damage.status_effect
			damage.free()
		
		if status_effect:
			if target.monster is Monster:
				if skill.target_stat_change():
					for stat in skill.target_stat_buff:
						target.mod_stat(stat, skill.target_stat_buff_amount)
					for stat in skill.target_stat_debuff:
						target.mod_stat(stat, skill.target_stat_debuff_amount)
				if skill.target_status_condition():
					_process_status_condition(target, 
							skill.target_condition_type, 
							randi() % (skill.target_max_condition_stack - skill.target_min_condition_stack + 1) + skill.target_min_condition_stack)
			
			if unit.monster is Monster:
				if skill.self_stat_change():
					for stat in skill.target_stat_buff:
						unit.mod_stat(stat, skill.target_stat_buff_amount)
					for stat in skill.target_stat_debuff:
						unit.mod_stat(stat, skill.target_stat_debuff_amount)
				if skill.self_status_condition():
					_process_status_condition(unit, 
							skill.self_condition_type, 
							randi() % (skill.self_max_condition_stack - skill.self_min_condition_stack + 1) + skill.self_min_condition_stack)
	
	if target.monster is Monster and skill is SkillHealing:
		var heal = skill.get_healing_result(target)
		var space = 0.0
		if heal.sp_heal > 0:
			target.monster.add_sp(heal.sp_heal)
			BattleUtils.load_counter() \
					.attach(self) \
					.set_location(target.position) \
					.set_damage(heal.sp_heal) \
					.tint_cyan()
			space = -30.0
		
		if heal.hp_heal > 0:
			target.monster.add_hp(heal.hp_heal)
			BattleUtils.load_counter() \
					.attach(self) \
					.set_location(target.position + Vector2(0.0, space)) \
					.set_damage(heal.hp_heal) \
					.tint_green()
		
		if skill.is_removing_conditions():
			var conditions = skill.get_healed_conditions()
		
		if skill.nullify_stats:
			pass
		
		heal.free()
	
	if target.monster is Monster and unit.monster is Monster and skill is SkillStatus:
		if skill.is_hit(unit, target):
			if skill.target_stat_change():
				var spacing = -30.0
				var space = 0.0
				for stat in skill.target_stat_buff:
					target.mod_stat(stat, skill.target_stat_buff_amount)
					BattleUtils.load_counter() \
							.attach(self) \
							.set_location(target.position + Vector2(0.0, space)) \
							.set_stat(stat, skill.target_stat_buff_amount)
					space += spacing
				space = 0
				for stat in skill.target_stat_debuff:
					target.mod_stat(stat, skill.target_stat_debuff_amount)
					BattleUtils.load_counter() \
							.attach(self) \
							.set_location(target.position + Vector2(0.0, space)) \
							.set_stat(stat, skill.target_stat_debuff_amount)
					space += spacing
			
			if skill.target_status_condition():
				_process_status_condition(target, 
						skill.target_condition_type, 
						randi() % (skill.target_max_condition_stack - skill.target_min_condition_stack + 1) + skill.target_min_condition_stack)
			
			if skill.self_stat_change():
				var spacing = -30.0
				var space = 0.0
				for stat in skill.self_stat_buff:
					unit.mod_stat(stat, skill.self_stat_buff_amount)
					BattleUtils.load_counter() \
							.attach(self) \
							.set_location(unit.position + Vector2(0.0, space)) \
							.set_stat(stat, skill.self_stat_buff_amount)
					space += spacing
				for stat in skill.target_stat_debuff:
					unit.mod_stat(stat, skill.self_stat_debuff_amount)
					BattleUtils.load_counter() \
							.attach(self) \
							.set_location(unit.position + Vector2(0.0, space)) \
							.set_stat(stat, skill.self_stat_buff_amount)
					space += spacing
			
			if skill.self_status_condition():
				_process_status_condition(unit, 
						skill.self_condition_type, 
						randi() % (skill.self_max_condition_stack - skill.self_min_condition_stack + 1) + skill.self_min_condition_stack)
		else:
			BattleUtils.load_counter() \
					.attach(self) \
					.set_location(target.position) \
					.set_miss(false) 
	
	if unit.monster is Monster:
		unit.state = BattleUnit.WAIT


func _process_status_condition(target: BattleUnit, condition, stacks: int):
	target.add_condition_stacks(condition, stacks)
