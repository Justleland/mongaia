extends MarginContainer

func set_name(name: String) -> void:
	$Margin/VBox/Name.text = name


func set_hp(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/HP/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/HP/NewNum.text = String(current)


func set_sp(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/SP/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/SP/NewNum.text = String(current)


func set_atk(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/ATK/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/ATK/NewNum.text = String(current)


func set_def(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/DEF/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/DEF/NewNum.text = String(current)


func set_agi(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/AGI/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/AGI/NewNum.text = String(current)


func set_str(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/STR/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/STR/NewNum.text = String(current)


func set_dex(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/DEX/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/DEX/NewNum.text = String(current)


func set_int(old: int, current: int) -> void:
	$Margin/VBox/Margin/StatBox/INT/OldNum.text = String(old)
	$Margin/VBox/Margin/StatBox/INT/NewNum.text = String(current)
