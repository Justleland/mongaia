extends YSort

onready var tiles = $SortedTiles
var area

var body
var timer = 0

# setting this up will change with tiles made at resolution
func _ready():
	area = Area2D.new()
	var size = tiles.cell_size.x * tiles.scale.x
	var half_size = size / 2
	var extent_size = size / 3
	var tileset = tiles.tile_set
	var cells = tiles.get_used_cells()
	# print(cells)
	for cell in cells:
		var shape = RectangleShape2D.new()
		var collision = CollisionShape2D.new()
		collision.set_position(Vector2(cell.x * size + half_size, cell.y * size + half_size))
		shape.set_extents(Vector2(extent_size, extent_size))
		collision.set_shape(shape)
		area.add_child(collision)
#		print("assigned collision")
	add_child(area)
	area.connect("body_entered", self, "_on_Area2D_body_entered")
	area.connect("body_exited", self , "_on_Area2D_body_exited")
	pass

func _process(delta):
	if body != null and body.velocity != Vector2.ZERO and timer >= 120:
		if randf() > 0.75:
			var monsters = $EnounterManager.get_random_encounter()
			if !monsters.empty():
				PlayerData.load_pos = body.position
				PlayerData.previous_area = get_tree().get_current_scene().filename
#				BattleUtils.add_encounter_monster(monsters)
				var parameters = BattleUtils.build_encounter(PlayerData.team, monsters)
				Utils.go_to_scene("res://scenes/battle/Battle.tscn", parameters, true)
#				get_tree().change_scene("res://scenes/rooms/Battle.tscn")
		else:
			timer = 0
	
	if timer < 120:
		timer += 1

func _on_Area2D_body_entered(body):
	self.body = body
	print("Body Entered")
	pass # Replace with function body.

func _on_Area2D_body_exited(_body):
	self.body = null
	print("Body Exited")
	pass # Replace with function body.
